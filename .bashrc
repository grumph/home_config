# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

################################################################################
# Initialization - part 1
################################################################################

# If not running interactively, don't do anything
case $- in
	*i*) ;;
	*  ) return;;
esac

# Set XDG environment variables
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"

# Import config file
if [ -f ~/.bashrc_config ]; then
	. "$HOME/.bashrc_config"
fi

################################################################################
# Functions
################################################################################

function inside_a_chroot
{
	[ -n "$SCHROOT_SESSION_ID" ] || [ -f /run/systemd/container ] || [ "$container" == "systemd-nspawn" ] || [ "$(uname -n)" != "$(cat /etc/hostname)" ]
}

################################################################################
# Initialization - part 2
################################################################################

# Set TMUX_BIN to force the start of tmux
#if [ -n "$TMUX_BIN" ]; then
if command -v tmux >/dev/null 2>&1 && [[ "$TERM" != "dumb" ]]; then  # no tmux for dumb shells (emacs tramp)
	if [ -z "$TMUX" ]; then
	# we're not in a tmux session
		if [ -n "$SSH_TTY" ] || inside_a_chroot ; then
		# We logged in via SSH
			TERM=screen-256color # force 256 colors
			( (tmux attach) || tmux new-session) && exit 0
		fi
	fi
fi


################################################################################
# Bash options
################################################################################

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
# same as ignoredups:ignorespace
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# Save multi-line commands as one command
shopt -s cmdhist

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=50000

# Use standard ISO 8601 timestamp
# %F equivalent to %Y-%m-%d
# %T equivalent to %H:%M:%S (24-hours format)
HISTTIMEFORMAT='%F %T '

# Ignore some commands for saving in the history
HISTIGNORE="ls:pwd:cp *:youtube-dl *:ydl *:yt-dlp *"

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar

# Case-insensitive globbing (used in pathname expansion)
shopt -s nocaseglob

# auto correct minor errors in path for cd commands
shopt -s cdspell
# auto correct minor errors in dir completion
shopt -s dirspell

# Check there are no running jobs before exit
shopt -s checkjobs

# Include dotfiles with glob expansion
shopt -s dotglob

# Prevent file overwrite on stdout redirection
# Use `>|` to force redirection to an existing file
set -o noclobber

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# Do not suspend console on C-s (thus do not disable bash i-search with C-s)
[[ $- == *i* ]] && stty -ixon

################################################################################
# Various options
################################################################################

# activate 256 colors for xterm-like terminals
case $TERM in
    xterm) TERM=xterm-256color;;
    screen) TERM=screen-256color;;
esac


if [ -n "$(which emacs)" ]; then
    export EDITOR="emacs -nw"
elif [ -n "$(which vim)" ]; then
    export EDITOR="vim"
fi

################################################################################
# Source other configurations
################################################################################

if [ -d "$HOME/.bashrc.d/" ]; then
	for i in "$HOME/.bashrc.d"/*; do
		. "$i"
	done
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
	if [ -f /usr/share/bash-completion/bash_completion ]; then
		. /usr/share/bash-completion/bash_completion
	elif [ -f /etc/bash_completion ]; then
		. /etc/bash_completion
	fi
fi

