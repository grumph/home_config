#!/bin/env bash

# https://www.davidpashley.com/articles/writing-robust-shell-scripts/
set -euo pipefail


_arg=${1:-hide}  # hide / unhide


_name_search='(navigation privée|private browsing)'

# Use xdotool for x11
if [ "$XDG_SESSION_TYPE" == "x11" ]; then
    if [ "$_arg" == "unhide" ]; then
        xdotool search -name "$_name_search" windowmap %@
        exit
    fi
    xdotool search -name "$_name_search" windowunmap %@
    exit
fi

# Use kdotool for plasma wayland
if [ "$XDG_SESSION_TYPE" == "wayland" ]; then
    ACTION=--add
    if [ "$_arg" == "unhide" ]; then
        ACTION=--remove
    fi

    PRIVATE_WINDOWS=$(comm -12 \
                           <(kdotool search --name "$_name_search" | sort) \
                           <(kdotool search --class '(librewolf|LibreWolf|firefox|Firefox)' | sort) \
                   )

    for WIN in $PRIVATE_WINDOWS; do
        kdotool windowstate $ACTION SKIP_TASKBAR "$WIN"
        kdotool windowstate $ACTION SKIP_PAGER "$WIN"
        kdotool windowstate $ACTION SHADED "$WIN"
        if [ "$ACTION" == "--add" ]; then
            kdotool windowminimize "$WIN"
        else
            kdotool windowactivate "$WIN"
        fi
    done
    exit
fi
