[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=Linux
Font=MonacoB2 Nerd Font Mono,9,-1,5,400,0,0,0,0,0,0,0,0,0,0,1
TabColor=255,255,255,0
UseFontLineChararacters=false

[Cursor Options]
CursorShape=0

[General]
Name=grumph
Parent=FALLBACK/
TerminalCenter=true
TerminalMargin=0

[Interaction Options]
TrimTrailingSpacesInSelectedText=true
UnderlineFilesEnabled=false

[Keyboard]
KeyBindings=default

[Scrolling]
HighlightScrolledLines=true
HistoryMode=2
HistorySize=10000
ScrollBarPosition=1

[Terminal Features]
BlinkingCursorEnabled=false
FlowControlEnabled=false
UrlHintsModifiers=0
VerticalLine=false
