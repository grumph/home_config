///////////////////////////////////////////////////////////////////////////////
// Sources:
//  - https://ffprofile.com/
//  - https://github.com/pyllyukko/user.js/blob/master/user.js
//  - https://support.mozilla.org/en-US/kb/how-stop-firefox-making-automatic-connections
//  - https://gist.github.com/0XDE57/fbd302cef7693e62c769
///////////////////////////////////////////////////////////////////////////////
user_pref("app.normandy.api_url", "");
user_pref("app.normandy.enabled", false);
user_pref("app.shield.optoutstudies.enabled", false);
user_pref("app.update.auto", false);
user_pref("beacon.enabled", false);
user_pref("breakpad.reportURL", "");
user_pref("browser.crashReports.unsubmittedCheck.autoSubmit", false);
user_pref("browser.crashReports.unsubmittedCheck.autoSubmit2", false);
user_pref("browser.crashReports.unsubmittedCheck.enabled", false);
user_pref("browser.disableResetPrompt", true);
user_pref("browser.fixup.alternate.enabled", false);
user_pref("browser.newtab.preload", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includePocket", false);
user_pref("browser.newtabpage.enabled", false);
user_pref("browser.newtabpage.enhanced", false);
user_pref("browser.newtabpage.introShown", true);
user_pref("browser.safebrowsing.appRepURL", "");
user_pref("browser.safebrowsing.blockedURIs.enabled", false);
user_pref("browser.safebrowsing.downloads.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.url", "");
user_pref("browser.safebrowsing.enabled", false);
user_pref("browser.safebrowsing.malware.enabled", false);
user_pref("browser.safebrowsing.phishing.enabled", false);
user_pref("browser.search.suggest.enabled", false);
user_pref("browser.selfsupport.url", "");
user_pref("browser.send_pings", false);
user_pref("browser.shell.checkDefaultBrowser", false);
user_pref("browser.startup.homepage_override.mstone", "ignore");
user_pref("browser.tabs.crashReporting.sendReport", false);
user_pref("browser.urlbar.speculativeConnect.enabled", false);
user_pref("browser.urlbar.trimURLs", false);
user_pref("datareporting.healthreport.service.enabled", false);
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.policy.dataSubmissionEnabled", false);
user_pref("dom.battery.enabled", false);
user_pref("dom.event.clipboardevents.enabled", false);
// Disabling indexedDB causes multiple crashes
// user_pref("dom.indexedDB.enabled", false);
user_pref("experiments.activeExperiment", false);
user_pref("experiments.enabled", false);
user_pref("experiments.manifest.uri", "");
user_pref("experiments.supported", false);
user_pref("extensions.getAddons.cache.enabled", false);
user_pref("extensions.getAddons.showPane", false);
user_pref("extensions.greasemonkey.stats.optedin", false);
user_pref("extensions.greasemonkey.stats.url", "");
user_pref("extensions.pocket.enabled", false);
user_pref("extensions.shield-recipe-client.api_url", "");
user_pref("extensions.shield-recipe-client.enabled", false);
user_pref("extensions.webservice.discoverURL", "");
user_pref("media.autoplay.enabled", false);
user_pref("media.navigator.enabled", false);
user_pref("media.peerconnection.enabled", false);
user_pref("media.video_stats.enabled", false);
user_pref("network.IDN_show_punycode", true);
user_pref("network.allow-experiments", false);
user_pref("network.captive-portal-service.enabled", false);
user_pref("network.cookie.cookieBehavior", 1);
user_pref("network.dns.disablePrefetch", true);
user_pref("network.http.referer.spoofSource", true);
user_pref("network.http.speculative-parallel-limit", 0);
user_pref("network.prefetch-next", false);
user_pref("network.trr.mode", 5);
user_pref("privacy.donottrackheader.enabled", true);
user_pref("privacy.donottrackheader.value", 1);
user_pref("privacy.resistFingerprinting", true);
user_pref("privacy.trackingprotection.enabled", true);
user_pref("privacy.trackingprotection.pbmode.enabled", true);
user_pref("signon.autofillForms", false);
user_pref("toolkit.telemetry.archive.enabled", false);
user_pref("toolkit.telemetry.bhrPing.enabled", false);
user_pref("toolkit.telemetry.cachedClientID", "");
user_pref("toolkit.telemetry.enabled", false);
user_pref("toolkit.telemetry.firstShutdownPing.enabled", false);
user_pref("toolkit.telemetry.hybridContent.enabled", false);
user_pref("toolkit.telemetry.newProfilePing.enabled", false);
user_pref("toolkit.telemetry.prompted", 2);
user_pref("toolkit.telemetry.rejected", true);
user_pref("toolkit.telemetry.server", "");
user_pref("toolkit.telemetry.shutdownPingSender.enabled", false);
user_pref("toolkit.telemetry.unified", false);
user_pref("toolkit.telemetry.unifiedIsOptIn", false);
user_pref("toolkit.telemetry.updatePing.enabled", false);
user_pref("webgl.disabled", true);

///////////////////////////////////////////////////////////////////////////////
// Others
///////////////////////////////////////////////////////////////////////////////
// no need to keep the filename path every time, better for privacy
user_pref("print.print_to_filename", "");

// Disable geolocation
user_pref("geo.enabled", false);
user_pref("geo.wifi.logging.enabled", false);
// If geolocation is enabled, use Mozilla geolocation service instead of Google
user_pref("geo.wifi.uri", "https://location.services.mozilla.com/v1/geolocate?key=%MOZILLA_API_KEY%");

// Disable Timing API
user_pref("dom.enable_user_timing", false);
user_pref("dom.enable_performance", false);

// No internal IP address when WebRTC is enabled
user_pref("media.peerconnection.ice.default_address_only", true);
user_pref("media.peerconnection.ice.no_host", true);

// Disable various APIs
user_pref("dom.telephony.enabled", false);
user_pref("device.sensors.enabled", false);
user_pref("dom.gamepad.enabled", false);
user_pref("dom.vr.enabled", false);
user_pref("dom.vibrator.enabled", false);
user_pref("dom.enable_resource_timing", false);
user_pref("javascript.options.wasm", false);
user_pref("camera.control.face_detection.enabled", false);
user_pref("javascript.options.asmjs", false);
user_pref("dom.flyweb.enabled", false);

// When browser pings are enabled, only allow pinging the same host as the origin page
user_pref("browser.send_pings.require_same_host", true);

// WebGL information exposure
user_pref("webgl.disable-fail-if-major-performance-caveat", true);
user_pref("webgl.enable-debug-renderer-info", false);

// Don't use Mozilla-provided location-specific search engines
user_pref("browser.search.geoSpecificDefaults", false);

// When browser.fixup.alternate.enabled is enabled, strip password from 'user:password@...' URLs
user_pref("browser.fixup.hide_user_pass", true);

// If SOCKS proxy is enabled, use it for DNS requests
user_pref("network.proxy.socks_remote_dns", true);

// Don't monitor OS online/offline connection state
user_pref("network.manage-offline-status", false);

// Block mixed content
user_pref("security.mixed_content.block_active_content", true);
user_pref("security.mixed_content.block_display_content", true);

// Local files can't use distant ressources
user_pref("security.fileuri.strict_origin_policy", true);

// Disable Displaying Javascript in History URLs
user_pref("browser.urlbar.filter.javascript", true);

// Disable SVG in OpenType fonts - makes many sites ugly
//user_pref("gfx.font_rendering.opentype_svg.enabled", false);

// Don't reveal build ID - Value taken from Tor Browser
user_pref("general.buildID.override", "20100101");
user_pref("browser.startup.homepage_override.buildID", "20100101");

// Prevent font fingerprint - makes many sites ugly (displaying text instead of icons)
//user_pref("browser.display.use_document_fonts", 0);

// Ensure you have a security delay when installing add-ons (milliseconds)
user_pref("security.dialog_enable_delay", 1000);

// Flash Player crash reports privacy
user_pref("dom.ipc.plugins.flash.subprocess.crashreporter.enabled", false);
user_pref("dom.ipc.plugins.reportCrashURL", false);

// Enable Click to play
user_pref("plugins.click_to_play", true);

// Enable add-on and certificate blocklists (OneCRL) from Mozilla
user_pref("extensions.blocklist.enabled", true);
user_pref("services.blocklist.update_enabled", true);

// Disable WebIDE
user_pref("devtools.webide.enabled", false);
user_pref("devtools.webide.autoinstallADBHelper", false);
user_pref("devtools.webide.autoinstallFxdtAdapters", false);

// Disable remote debugging
user_pref("devtools.debugger.remote-enabled", false);
user_pref("devtools.chrome.enabled", false);
user_pref("devtools.debugger.force-local", true);

// Enable contextual identity Containers feature (Firefox >= 52)
user_pref("privacy.userContext.enabled", true);

// Disable "Recommended by Pocket"
user_pref("browser.newtabpage.activity-stream.feeds.section.topstories", false);

// Disable network predictor
user_pref("network.predictor.enabled", false);

// Disable SSDP
user_pref("browser.casting.enabled", false);

// Disable automatic downloading of OpenH264 codec
user_pref("media.gmp-gmpopenh264.enabled", false);
user_pref("media.gmp-manager.url", "");

// Disable some automatic connections
user_pref("browser.aboutHomeSnippets.updateUrl", "");
user_pref("browser.search.update", false);

// When username/password autofill is enabled, still disable it on non-HTTPS sites
user_pref("signon.autofillForms.http", false);

// Delete temporary files on exit
user_pref("browser.helperApps.deleteTempFileOnExit", true);

// Disable right-click menu manipulation via JavaScript
//user_pref("dom.event.contextmenu.enabled", false);

// Disable Downloading on Desktop
user_pref("browser.download.folderList", 2);

// Disable inline autocomplete in URL bar
// http://kb.mozillazine.org/Inline_autocomplete
user_pref("browser.urlbar.autoFill", false);

// mouse scroll with middle click
user_pref("general.autoScroll", true);

// Display a notification bar when websites offer data for offline use
user_pref("browser.offline-apps.notify", true);

// Enable %profile%/chrome/userChrome.css and %profile%/chrome/userContent.css
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);
