local gstring = require("gears.string")
local awful = require("awful")



-- toggle property 0/1
function toggle_client_xprop(property, c, prop_type)
    prop_type = prop_type or "8c"
    local cl = c or client.focus
    awful.spawn.easy_async("xprop -id " .. cl.window .. " ".. prop_type .. " " .. property,
                           function(stdout, stderr, _, exit_code)
                               if exit_code == 0 then
                                   local value = "1"
                                   trimmed_stdout = stdout:gsub("^%s*(.-)%s*$", "%1")
                                   if gstring.endswith(trimmed_stdout, " 1") then
                                       value = "0"
                                   end
                                   awful.spawn("xprop -id " .. cl.window ..
                                               " -f ".. property .." ".. prop_type ..
                                               " -set ".. property .." " .. value, false)
                               else
                                   naughty.notify({ preset = naughty.config.presets.critical,
                                                    title = "Error with xprop",
                                                    text = stderr })
                               end
                           end
    )
end


function toggle_client_prop(property, c)
    local cl = c or client.focus
    if cl then
        cl[property] = not cl[property]
    end
end

function toggle_client_prop_then_raise(property, c)
    local cl = c or client.focus
    if cl then
        cl[property] = not cl[property]
        cl:raise()
    end
end


function minimize_all_but_focused_on_current_tag ()
    local tags = awful.screen.focused().selected_tags
    for _, t in ipairs(tags) do
        local clients = t:clients()
        for _, c in ipairs(clients) do
            if c ~= client.focus then
                c.minimized = true
            end
        end
    end
end


function move_focused_to_tag_relative (i)
    local t = client.focus and client.focus.first_tag or nil
    if t == nil then return end
    local alltags = client.focus.screen.tags
    local t2 = alltags[(t.index + i - 1) % #alltags + 1]
    client.focus:move_to_tag(t2)
    awful.tag.viewidx(i)
end


function hide_secret_clients(hide)
    hide = (hide ~= false)  -- defaults to true
    for  _, c in ipairs(client.get()) do

        if c.name and
            (gstring.endswith(c.name, "(Navigation privée)")
             or gstring.endswith(c.name, "(Private Browsing)")
             or gstring.startswith(c.name, "[private")
             or gstring.startswith(c.name, "⛔)")
            )
        then
            c.hidden = hide
        end
    end
end


local fake_client = {}
function fake_client:struts () return nil end  -- return {}
function fake_client:buttons () return nil end
function fake_client:instances () return 0 end
function fake_client:get () return nil end  -- return {}
function fake_client:isvisible () return false end
function fake_client:kill () end
function fake_client:swap () end
function fake_client:tags () return nil end  -- return {}
function fake_client:raise () end
function fake_client:lower () end
function fake_client:unmanage () end
function fake_client:geometry () return nil end  -- return {}
function fake_client:apply_size_hints () return 0,0 end
function fake_client:keys () return nil end  -- return {}
function fake_client:get_icon () return nil end
function fake_client:disconnect_signal () end
function fake_client:emit_signal () end
function fake_client:connect_signal () end
function fake_client:jump_to () end
function fake_client:relative_move () end
function fake_client:move_to_tag () end
function fake_client:toggle_tag () end
function fake_client:move_to_screen () end
function fake_client:to_selected_tags () end
function fake_client:get_transient_for_matching () return nil end
function fake_client:is_transient_for () return nil end

function client_focus_or_not()
    local c = client.focus
    if c then
        return c
    else
        return fake_client
    end
end
