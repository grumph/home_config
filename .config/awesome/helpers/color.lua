local function hex2rgb(color)
    color, _ = string.gsub(color, "#", "")
    local result = {
        r = tonumber("0x" .. color:sub(2,3)),
        g = tonumber("0x" .. color:sub(4,5)),
        b = tonumber("0x" .. color:sub(6,7)),
     }
    if string.len(color) == 8 then
        result.a = tonumber("0x" .. color:sub(8,9))
    end
    return result
end

local function rgb2hex(rgb)
    if rgb.a then
        return string.format('#%02x%02x%02x%02x', rgb.r, rgb.g, rgb.b, rgb.a)
    end
    return string.format('#%02x%02x%02x', rgb.r, rgb.g, rgb.b)
end

local function is_dark(color)
    -- Try to determine if the color is dark or light
    local numeric_value = 0;
    for s in color:gmatch("[a-fA-F0-9][a-fA-F0-9]") do
        numeric_value = numeric_value + tonumber("0x"..s);
    end
    return (numeric_value < 383)
end

local function lighten(color, amount)
    amount = amount or 26
    local c = hex2rgb(color)

    c.r = c.r + amount
    c.r = c.r < 0 and 0 or c.r
    c.r = c.r > 255 and 255 or c.r
    c.g = c.g + amount
    c.g = c.g < 0 and 0 or c.g
    c.g = c.g > 255 and 255 or c.g
    c.b = c.b + amount
    c.b = c.b < 0 and 0 or c.b
    c.b = c.b > 255 and 255 or c.b

    return rgb2hex(c.r)
end

local function darken(color, amount)
    amount = amount or 26
    return lighten(color, -amount)
end

return {
    is_dark = is_dark,
    lighten = lighten,
    darken = darken,
    hex2rgb = hex2rgb,
    rgb2hex = rgb2hex,
}
