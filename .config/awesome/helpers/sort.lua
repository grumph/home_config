
local function by_string(as, bs, chained_test)
    as = as or ""
    bs = bs or ""
    return (
        (string.lower(as) == string.lower(bs))
        and (chained_test or false)
        or (string.lower(bs) > string.lower(as))
    )
end

local function by_bool(a, b, chained_test)
    return (
        (a == b)
        and (chained_test or false)
        or (a ~= b
            and a)
    )
end

local function by_num(a, b, chained_test)
    return (
        (a == b)
        and (chained_test or false)
        or (b > a)
    )
end


local function sort_clients(clients, sort_function)
    sort_function = sort_function or function(a, b)
        return (
            -- by_bool(a.minimized, b.minimized,
            by_bool(a.maximized, b.maximized,
                    by_string(a.class, b.class,
                              by_string(a.name, b.name
        ))))
    end

    table.sort(clients, sort_function)
    return clients
end

return {
    by_string = by_string,
    by_bool = by_bool,
    by_num = by_num,
    clients = sort_clients
}
