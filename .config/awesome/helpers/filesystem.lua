local Gio = require("lgi").Gio
local gears = require("gears")



--- Get a list of files from a directory path
-- filter for only exts extensions
-- can be recursive
function list_directory_files(path, exts, recursive)
    recursive = recursive or false
    local files, valid_exts = {}, {}

    -- Transforms { "jpg", ... } into { [jpg] = #, ... }
    if exts then for i, j in ipairs(exts) do valid_exts[j:lower()] = i end end

    -- Build a table of files from the path with the required extensions
    local file_list = Gio.File.new_for_path(path):enumerate_children("standard::*", 0)
    if file_list then
        for file in function() return file_list:next_file() end do
            local file_type = file:get_file_type()
            if file_type == "REGULAR" then
                local file_name = file:get_display_name()
                if not exts or valid_exts[file_name:lower():match(".+%.(.*)$") or ""] then
                    table.insert(files, file_name)
                end
            elseif recursive and file_type == "DIRECTORY" then
                local file_name = file:get_display_name()
                files = gears.table.join(files, list_directory_files(file_name, exts, recursive))
            end
        end
    end

    return files
end


return {
    list_directory_files = list_directory_files,
}
