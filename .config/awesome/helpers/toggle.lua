


local function toggle_titlebars_and_wiboxes(force_visible, prefer_visible)
    -- Toggle both titlebars and wiboxes
    -- prefer_visible is for manage mismatches:
    --   if prefer_visible, show all if any is hidden
    --   if not prever_visible (default), hide all if any is visible
    -- if force_visible is set, visibility is forced instead of toggle
    prefer_visible = prefer_visible or false
    if force_visible ~= nil then
        global_titlebars_visible = force_visible
        global_wibox_visible = force_visible
    elseif (not prefer_visible) and (global_titlebars_visible or global_wibox_visible)
        or prefer_visible and (global_titlebars_visible and global_wibox_visible)
    then
        global_titlebars_visible = false
        global_wibox_visible = false
    else
        global_titlebars_visible = true
        global_wibox_visible = true
    end
    apply_titlebar()
    apply_wibox()
end

local function toggle_titlebars_then_wiboxes(reverse)
    -- Cycle wibox and titlebars -> wiboxes only -> no wibox or titlebar -> wibox and titlebars
    -- if reverse, cycle the other way around
    reverse = reverse or false
    if not reverse then
        if global_titlebars_visible then
            global_titlebars_visible = false
        else
            if global_wibox_visible then
                global_wibox_visible = false
            else
                global_titlebars_visible = true
                global_wibox_visible = true
            end
        end
    else
        if not global_wibox_visible then
            global_wibox_visible = true
        else
            if not global_titlebars_visible then
                global_titlebars_visible = true
            else
                global_titlebars_visible = false
                global_wibox_visible = false
            end
        end
    end

    apply_titlebars()
    apply_wibox()
end
