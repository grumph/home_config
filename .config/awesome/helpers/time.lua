



-- Parse HH:MM:SS to seconds
local hhmmss_to_seconds = function(time)
    hour_sec = tonumber(string.sub(time, 1, 2)) * 3600
    min_sec = tonumber(string.sub(time, 4, 5)) * 60
    get_sec = tonumber(string.sub(time, 7, 8))
    return (hour_sec + min_sec + get_sec)
end


-- Get time difference
local time_diff = function(base, compare)
    local diff = hhmmss_to_seconds(base) - hhmmss_to_seconds(compare)
    return diff
end



return {
    time_diff = time_diff,
    hhmmss_to_seconds = hhmmss_to_seconds,
}
