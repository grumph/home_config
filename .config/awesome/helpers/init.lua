


local function get_script_path()
   local str = debug.getinfo(2, "S").source:sub(2)
   return str:match("(.*/)")
end


return {
    click_to_hide = require("helpers.click_to_hide"),
    sort = require("helpers.sort"),
    widget_popup = require("helpers.widget_popup"),
    color = require("helpers.color"),
    filesystem = require("helpers.filesystem"),
    time = require("helpers.time"),
    get_script_path = get_script_path,
}
