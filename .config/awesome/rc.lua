-- Notification library
local naughty = require "naughty"

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error",
                           function (err)
                               -- Make sure we don't go into an endless error loop
                               if in_error then return end
                               in_error = true

                               naughty.notify({ preset = naughty.config.presets.critical,
                                                title = "Oops, an error happened!",
                                                text = tostring(err) })
                               in_error = false
                           end
    )
end
-- }}}


-- io.stderr:write("\n▒▄▀▄░█░░▒█▒██▀░▄▀▀░▄▀▄░█▄▒▄█▒██▀░█░░▒█░█▄▒▄█\n"
--                 .."░█▀█░▀▄▀▄▀░█▄▄▒▄██░▀▄▀░█▒▀▒█░█▄▄░▀▄▀▄▀░█▒▀▒█\n")
-- io.stderr:write("\n▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄\n"
--                 .."█░▄▄▀█░███░█░▄▄█░▄▄█▀▄▄▀█░▄▀▄░█░▄▄██░███░██░▄▀▄░\n"
--                 .."█░▀▀░█▄▀░▀▄█░▄▄█▄▄▀█░██░█░█▄█░█░▄▄██░█░█░██░█░█░\n"
--                 .."█░██░██▄█▄██▄▄▄█▄▄▄██▄▄██▄███▄█▄▄▄██▄▀▄▀▄██░███░\n"
--                 .."▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀\n")
io.stderr:write("\n _____                             _ _ _ _____ \n"
                .."|  _  |_ _ _ ___ ___ ___ _____ ___| | | |     |\n"
                .."|     | | | | -_|_ -| . |     | -_| | | | | | |\n"
                .."|__|__|_____|___|___|___|_|_|_|___|_____|_|_|_|\n")


-- Allow to find luarocks packages
-- pcall(require, "luarocks.loader")


-- Standard awesome library
local awful = require "awful"
local hotkeys_popup = require "awful.hotkeys_popup.widget"
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require "awful.hotkeys_popup.keys"
-- Theme handling library
local beautiful = require "beautiful"
beautiful.init(awful.util.get_configuration_dir() .. "mytheme/theme.lua")


naughty.config.defaults.position = beautiful.notification_position
naughty.config.defaults.border_width = beautiful.notification_border_width
naughty.config.defaults.margin = beautiful.notification_margin
naughty.config.defaults.icon_size = beautiful.notification_icon_size
naughty.config.defaults.max_width = (screen.primary.geometry.width / 3) or 400


require "configuration.defaults"

local bling = require "external.bling"

bling.module.wallpaper.setup {
    set_function = bling.module.wallpaper.setters.random,
    wallpaper = os.getenv("HOME").."/Pictures/wallpapers",
    position = "maximized",
    change_timer = 631,
}

local exit_screen = require "widgets.exit_screen"


-- local leaved = require "awesome-leaved"
-- beautiful.layout_leavedright = "~/.config/awesome/awesome-leaved/icons/leavedright.png"
-- beautiful.layout_leavedleft = "~/.config/awesome/awesome-leaved/icons/leavedleft.png"
-- beautiful.layout_leavedbottom = "~/.config/awesome/awesome-leaved/icons/leavedbottom.png"
-- beautiful.layout_leavedtop = "~/.config/awesome/awesome-leaved/icons/leavedtop.png"



local bars = require "configuration.bars"
local titlebars = require "configuration.titlebars"
titlebars.toggle(false)
awful.keyboard.append_global_keybindings({
    awful.key(
        { modkey }, "comma",
        bars.toggle,
        { description = "toggle top and bottom bars", group = "tools"}),
    awful.key(
        { modkey, "Control" }, "comma",
        titlebars.toggle,
        { description = "hide titlebars", group = "tools"}),
})


-- Append layouts to awful.layout.layouts, order matters.
awful.layout.append_default_layout(bling.layout.equalarea)
awful.layout.append_default_layout(awful.layout.suit.tile)
awful.layout.append_default_layout(awful.layout.suit.fair)
awful.layout.append_default_layout(awful.layout.suit.tile.bottom)
awful.layout.append_default_layout(awful.layout.suit.spiral.dwindle)
awful.layout.append_default_layout(awful.layout.suit.max)
awful.layout.append_default_layout(awful.layout.suit.magnifier)
awful.layout.append_default_layout(awful.layout.suit.corner.nw)
awful.layout.append_default_layout(awful.layout.suit.floating)



local mainmenu = require "widgets.mainmenu"
local helpers = require "helpers"
helpers.click_to_hide.menu(mainmenu)


-- {{{ Mouse bindings
root.buttons = {
    awful.button({ }, 3, function () mainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewprev),
    awful.button({ }, 5, awful.tag.viewnext)
}
-- }}}


local hard = require "hard"

local clientinfo = require "widgets.clientinfo"

local gears = require "gears"
local supertimer = gears.timer {
    timeout = 1.5, -- seconds
    single_shot = true,
    callback =
        function ()
            clientinfo.show()
        end,
}

-- {{{ Key bindings
awful.keyboard.append_global_keybindings({
        -- hack for shortcut with only Super_L
        -- https://github.com/awesomeWM/awesome/issues/169#issuecomment-294317264
        awful.key(
            {},
            'Super_L',
            function ()
                if not clientinfo.visible then
                    if supertimer.started then
                        supertimer:again()
                    else
                        supertimer:start()
                    end
                end
            end,
            nil, -- Warning: Releasing this key have no effect ever !
            { description = "Show clientinfo popups (2s press)", group = "debug"}),
        awful.key(
            { modkey }, "Super_L",
            nil, -- Warning: Pressing this key have no effect ever !
            function ()
                supertimer:stop()
                clientinfo.hide()
            end,
            { description = "hide clientinfo popups", group = "debug"}),
    awful.key(
        { modkey }, "g",
        function () no_fullscreen = not no_fullscreen end,
        { description = "half fullscreen", group = "test"}),
    awful.key(
        { modkey }, "a",
        function ()
            -- xrandr.xrandr()

            -- io.stderr:write("\nprimary screen is " .. tostring(screen.primary))
            -- io.stderr:write("\nfocused screen is " .. tostring(awful.screen.focused()))
            -- io.stderr:write("\n")
            -- screen.primary:swap(awful.screen.focused())

            -- if supertest.wibar.screen == 2 then
            --    supertest.wibar.screen = screen.primary
            -- else
            --    supertest.wibar.screen = 2
            -- end
        end,
        { description = "swap primary screen", group = "test"}),
    -- Special keys
    awful.key(
        { }, "XF86MonBrightnessDown",
        function() hard.display.brightness:lower() end,
        {description = "decrease screen brightness", group = "system"}),
    awful.key(
        { }, "XF86MonBrightnessUp",
        function() hard.display.brightness:raise() end,
        {description = "increase screen brightness", group = "system"}),
    awful.key(
        { "Control" }, "XF86MonBrightnessDown",
        function() hard.display.brightness:set(5) end,
        {description = "minimum screen brightness", group = "system"}),
    awful.key(
        { "Control" }, "XF86MonBrightnessUp",
        function () hard.display.brightness:set(100) end,
        {description = "maximum screen brightness", group = "system"}),
    awful.key(
        { }, "XF86AudioMute",
        function() hard.audio.volume:toggle() end,
        {description = "mute audio", group = "system"}),
    awful.key(
        { }, "XF86AudioLowerVolume",
        function() hard.audio.volume:lower() end,
        {description = "lower volume", group = "system"}),
    awful.key(
        { }, "XF86AudioRaiseVolume",
        function() hard.audio.volume:raise() end,
        {description = "increase volume", group = "system"}),
   awful.key(
        { "Control" }, "XF86AudioLowerVolume",
        function() hard.audio.volume:set(10) end,
        {description = "minimum volume", group = "system"}),
    awful.key(
        { "Control"}, "XF86AudioRaiseVolume",
        function() hard.audio.volume:set(100) end,
        {description = "maximum volume", group = "system"}),
    awful.key(
        { }, "Print",
        function() hard.display.print_screen() end,
        {description = "take screenshot", group = "system"}),
    -- Media Keys
    -- awful.key({}, "XF86AudioPlay", function()
    --   awful.util.spawn("playerctl play-pause", false)
    -- end),
    -- awful.key({}, "XF86AudioNext", function()
    --   awful.util.spawn("playerctl next", false)
    -- end),
    -- awful.key({}, "XF86AudioPrev", function()
    --   awful.util.spawn("playerctl previous", false)
    -- end)
    awful.key(
        { }, "XF86Display",
        function () awful.spawn("arandr") end,
        {description = "Configure screens", group = "system"}),

    awful.key(
        { modkey }, "h",
        function () hotkeys_popup:show_help() end,
        {description="show help", group="awesome"}),
    awful.key(
        { modkey }, "twosuperior",
        awful.tag.history.restore,
        {description = "go back", group = "tag"}),
    awful.key(
        { modkey }, "equal",
        awful.tag.history.restore,
        {description = "go back", group = "tag"}),
    awful.key(
        { modkey }, "Left",
        awful.tag.viewprev,
        {description = "view previous tag", group = "tag"}),
    awful.key(
        { modkey }, "Right",
        awful.tag.viewnext,
        {description = "view next tag", group = "tag"}),
    awful.key(
        { modkey, "Shift" }, "Left",
        function () move_focused_to_tag_relative(-1) end,
        {description = "move to previous tag", group = "tag"}),
    awful.key(
        { modkey, "Shift" }, "Right",
        function () move_focused_to_tag_relative(1) end,
        {description = "move to next tag", group = "tag"}),
    awful.key(
        { modkey }, "Down",
        function () awful.client.focus.byidx( 1) end,
        {description = "focus next by index", group = "client"}),
    awful.key(
        { modkey }, "Up",
        function () awful.client.focus.byidx(-1) end,
        {description = "focus previous by index", group = "client"}),
    awful.key(
        { modkey }, "w",
        function () mainmenu:show() end,
        {description = "show main menu", group = "awesome"}),

    -- Layout manipulation
    awful.key(
        { modkey }, "Tab",
        function ()
            awful.spawn.with_shell("rofi -theme dropdown -show window -modi window ")
        end,
        {description = "swap with next client by index", group = "client"}),
    awful.key(
        { modkey, "Shift" }, "Tab",
        function ()
            awful.spawn.with_shell("rofi -theme dropdown -modi window -show window ")
        end,
        {description = "swap with next client by index", group = "client"}),

    awful.key(
        { modkey }, "F11",
        function ()
            awful.spawn.with_shell("rofi -theme common -show calc -modi calc -no-show-match -no-sort")
        end,
        {description = "swap with next client by index", group = "client"}),


    awful.key(
        { modkey, "Shift"   }, "Down",
        function ()
            awful.client.swap.byidx( 1)
        end,
        {description = "swap with next client by index", group = "client"}),
    awful.key(
        { modkey, "Shift"   }, "Up",
        function ()
            awful.client.swap.byidx(-1)
        end,
        {description = "swap with previous client by index", group = "client"}),
    awful.key(
        { modkey, "Control" }, "Down",
        function () awful.screen.focus_relative( 1) end,
        {description = "focus the next screen", group = "screen"}),
    awful.key(
        { modkey, "Control" }, "Up",
        function () awful.screen.focus_relative(-1) end,
        {description = "focus the previous screen", group = "screen"}),
    awful.key(
        { modkey }, "space",
        awful.client.urgent.jumpto,
        {description = "jump to urgent client", group = "client"}),
    awful.key(
        { modkey, "Control" }, "Tab",
        function ()
            -- awful.client.focus.history.previous()
            local c = awful.client.next(1)
            if c then
                client.focus = c
                client.focus:raise()
            end
        end,
        {description = "next raised client", group = "client"}),
    awful.key(
        { modkey, "Control", "Shift" }, "Tab",
        function ()
            -- awful.client.focus.history.previous()
            local c = awful.client.next(-1)
            if c then
                client.focus = c
                client.focus:raise()
            end
        end,
        {description = "previous raised client", group = "client"}),





    -- Standard program
    awful.key(
        { modkey }, "Return",
        function () awful.spawn(terminal) end,
        {description = "open a terminal", group = "launcher"}),
    awful.key(
        { modkey, "Control" }, "r",
        awesome.restart,
        {description = "reload awesome", group = "awesome"}),
    awful.key(
        { modkey }, "XF86PowerOff",
        exit_screen,
        {description = "logout screen", group = "awesome"}),
    awful.key(
        { modkey }, "l",
        exit_screen,
        {description = "logout screen", group = "awesome"}),
    awful.key(
        { modkey }, "e",
        function () awful.spawn("dolphin") end,
        {description = "file manager", group = "tools"}),
    awful.key(
        { modkey }, "i",
        function () awful.spawn("firefox --no-remote --ProfileManager") end,
        {description = "Firefox", group = "tools"}),
    awful.key(
        { modkey }, "p",
        function () awful.spawn("firejail --private falkon") end,
        {description = "Firejailed Falkon", group = "tools"}),






    awful.key(
        { modkey, "Control" }, "Right",
        function () awful.tag.incmwfact( 0.05)          end,
        {description = "increase master width factor", group = "layout"}),
    awful.key(
        { modkey, "Control" }, "Left",
        function () awful.tag.incmwfact(-0.05)          end,
        {description = "decrease master width factor", group = "layout"}),
    awful.key(
        { modkey, "Shift"   }, "h",
        function () awful.tag.incnmaster( 1, nil, true) end,
        {description = "increase the number of master clients", group = "layout"}),
    awful.key(
        { modkey, "Shift"   }, "l",
        function () awful.tag.incnmaster(-1, nil, true) end,
        {description = "decrease the number of master clients", group = "layout"}),
    awful.key(
        { modkey, "Control" }, "h",
        function () awful.tag.incncol( 1, nil, true)    end,
        {description = "increase the number of columns", group = "layout"}),
    awful.key(
        { modkey, "Control" }, "l",
        function () awful.tag.incncol(-1, nil, true)    end,
        {description = "decrease the number of columns", group = "layout"}),
    awful.key(
        { modkey }, "space",
        function () awful.layout.inc( 1)                end,
        {description = "select next", group = "layout"}),
    awful.key(
        { modkey, "Shift"   }, "space",
        function () awful.layout.inc(-1)                end,
        {description = "select previous", group = "layout"}),
    awful.key(
        { modkey }, "!",
        hide_secret_clients,
        {description = "hide private clients", group = "client"}),
    awful.key(
        { modkey, "Control" }, "!",
        function ()
            hide_secret_clients(false)
        end,
        {description = "unhide private clients", group = "client"}),
    awful.key(
        { modkey }, "<",
        hide_secret_clients,
        {description = "hide private clients", group = "client"}),
    awful.key(
        { modkey, "Control" }, "v",
        function ()
            local c = awful.client.restore()
            -- Focus restored client
            if c then
                client.focus = c
                c:raise()
            end
        end,
        {description = "restore minimized", group = "client"}),

    -- Prompt
    awful.key(
        { modkey, "Shift" }, "x",
        function () primary_promptbox:run() end,
        {description = "run prompt", group = "launcher"}),

    awful.key(
        { modkey, "Control" }, "x",
        function ()
            awful.prompt.run {
                prompt       = "Run Lua code: ",
                textbox      = primary_promptbox.widget,
                exe_callback = awful.util.eval,
                history_path = awful.util.get_cache_dir() .. "/history_eval"
            }
        end,
        {description = "lua execute prompt", group = "awesome"}),

    awful.key(
        { modkey }, "x",
        function()
            awful.spawn.with_shell("rofi -theme modeline -combi-modi drun,run -show combi -modi combi")
        end,
        {description = "show the launcher", group = "launcher"}),

})



-- Focused client keys

require "helpers.client_functions"
local wish = require "widgets.wish_key"

awful.keyboard.append_global_keybindings({
        awful.key (
            { modkey }, 'c',
            function () wish.client(client.focus) end),

        awful.key(
            { modkey }, "q",
            function () client_focus_or_not():kill() end,
            {description = "close", group = "client"}),
        awful.key(
            { modkey }, "o",
            function () awful.screen.focus_relative(1) end,
            {description = "navigate to screen", group = "client"}),
        awful.key(
            { modkey, "Shift"   }, "o",
            function () client_focus_or_not():move_to_screen() end,
            {description = "move to screen", group = "client"}),
        awful.key(
            { modkey }, "v",
            function () toggle_client_prop("minimized") end,
            {description = "minimize", group = "client"}),
        awful.key(
            { modkey }, "f",
            function () toggle_client_prop_then_raise("maximized") end,
            {description = "(un)maximize", group = "client"}),
        awful.key(
            { modkey }, "u",
            minimize_all_but_focused_on_current_tag,
            {description = "Make current client unique (minimize all others)", group = "client"}),
})

-- }}}


-- {{{ Signals

require "configuration.tags_and_rules"
require "configuration.client_insertion"
require "configuration.center_in_parent"
require "configuration.force_same_tag"

require "configuration.focus_follow_mouse"
require "configuration.mouse_follow_focus"

require "configuration.decorate_focused"
require "configuration.no_border_on_unique"







awful.spawn("bash " .. awful.util.get_configuration_dir() .. "autorun.sh")

-- }}}
