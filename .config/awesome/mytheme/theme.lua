---------------------------------------------
-- Awesome theme which follows xrdb config --
--   by Yauhen Kirylau                    --
---------------------------------------------

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local xrdb = xresources.get_current_theme()
local gfs = require("gears.filesystem")
local themes_path = gfs.get_themes_dir()
local config_path = gfs.get_configuration_dir()
local gears = require("gears")

-- inherit default theme
local theme = dofile(themes_path.."default/theme.lua")
-- load vector assets' generators for this theme


-- Load ~/.Xresources colors and set fallback colors
theme.xbackground = xrdb.background or "#000000"
theme.xforeground = xrdb.foreground or "#b2b2b2"
theme.xcolor0 = xrdb.color0 or "#000000"
theme.xcolor1 = xrdb.color1 or "#b21818"
theme.xcolor2 = xrdb.color2 or "#18b218"
theme.xcolor3 = xrdb.color3 or "#cbcb2f"
theme.xcolor4 = xrdb.color4 or "#4848b2"
theme.xcolor5 = xrdb.color5 or "#b218b2"
theme.xcolor6 = xrdb.color6 or "#18b2b2"
theme.xcolor7 = xrdb.color7 or "#b2b2b2"
theme.xcolor8 = xrdb.color8 or "#686868"
theme.xcolor9 = xrdb.color9 or "#ff5454"
theme.xcolor10 = xrdb.color10 or "#54ff54"
theme.xcolor11 = xrdb.color11 or "#ffff54"
theme.xcolor12 = xrdb.color12 or "#9a9aff"
theme.xcolor13 = xrdb.color13 or "#ff54ff"
theme.xcolor14 = xrdb.color14 or "#54ffff"
theme.xcolor15 = xrdb.color15 or "#ffffff"


-- theme.font          = "Terminus 9"
-- theme.font          = "Bellota Bold 11"
theme.font          = "Diavlo Book 11"
-- theme.font          = "Days 9"
-- theme.font          = "SF Archery Black 9"
-- theme.font          = "MADE Evolve Sans Thin 10"
-- theme.font          = "Vanlose BookType 12"
-- theme.font          = "Comfortaa 10"
-- theme.font          = "Sofadi One 10"


theme.icon_font = "MonacoB2 Nerd Font Mono "


theme.bg_primary = "#0f770f"
theme.fg_primary = "#97ff97"

theme.bg_secondary = "#2121aa"
theme.fg_secondary = "#ddddff"


theme.bg_normal     = theme.xbackground
theme.fg_normal     = theme.xforeground

theme.bg_focus      = theme.bg_primary
theme.fg_focus      = theme.fg_primary

theme.bg_urgent     = theme.xcolor9
theme.fg_urgent     = theme.bg_normal


theme.wibar_height = dpi(20)

theme.tasklist_bg_minimize = theme.xbackground
theme.tasklist_fg_minimize = theme.xforeground .. "90"
theme.tasklist_bg_normal = theme.bg_focus .. "77"
theme.tasklist_fg_normal = theme.fg_focus .. "90"
theme.tasklist_shape = function(cr, w, h) gears.shape.partially_rounded_rect(cr, w, h, false, false, true, true, dpi(7)) end

theme.bg_systray    = theme.bg_normal

theme.taglist_fg_focus = theme.fg_secondary
theme.taglist_bg_focus = theme.bg_secondary
theme.taglist_font = theme.icon_font .. "18"

theme.useless_gap   = dpi(0)
theme.border_width  = dpi(0)
theme.border_normal = theme.bg_normal
theme.border_focus  = theme.bg_primary
theme.border_marked = theme.bg_secondary

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

theme.tooltip_fg = theme.fg_secondary
theme.tooltip_bg = theme.bg_normal
theme.tooltip_border_width = dpi(3)
theme.tooltip_border_color = theme.border_focus
theme.tooltip_font = "MonacoB2 Nerd Font Mono 11"
-- theme.tooltip_font = "Fantasque Sans Mono 11"

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = themes_path.."default/submenu.png"
theme.menu_height = dpi(23)
theme.menu_width  = dpi(200)

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"


current_path = config_path.."mytheme/"
icons_path = config_path .."icons/"

theme.titlebar_close_button_normal = icons_path.."close-octagon.svg"
theme.titlebar_minimize_button_normal = icons_path.."window-minimize.svg"
theme.titlebar_ontop_button_normal_inactive = icons_path.."arrow-up-bold-box-outline.svg"
theme.titlebar_ontop_button_normal_active = icons_path.."arrow-up-bold-box.svg"
theme.titlebar_sticky_button_normal_inactive = icons_path.."lock-open-outline.svg"
theme.titlebar_sticky_button_normal_active = icons_path.."lock.svg"
theme.titlebar_floating_button_normal_inactive = icons_path.."view-dashboard.svg"
theme.titlebar_floating_button_normal_active = icons_path.."vector-arrange-above2.svg"
theme.titlebar_maximized_button_normal_inactive = icons_path.."fit-to-page-outline.svg"
theme.titlebar_maximized_button_normal_active = icons_path.."fit-to-page.svg"

theme.titlebar_close_button_focus  = theme.titlebar_close_button_normal
theme.titlebar_minimize_button_focus  = theme.titlebar_minimize_button_normal
theme.titlebar_ontop_button_focus_inactive = theme.titlebar_ontop_button_normal_inactive
theme.titlebar_ontop_button_focus_active  = theme.titlebar_ontop_button_normal_active
theme.titlebar_sticky_button_focus_inactive  = theme.titlebar_sticky_button_normal_inactive
theme.titlebar_sticky_button_focus_active  = theme.titlebar_sticky_button_normal_active
theme.titlebar_floating_button_focus_inactive  = theme.titlebar_floating_button_normal_inactive
theme.titlebar_floating_button_focus_active  = theme.titlebar_floating_button_normal_active
theme.titlebar_maximized_button_focus_inactive  = theme.titlebar_maximized_button_normal_inactive
theme.titlebar_maximized_button_focus_active  = theme.titlebar_maximized_button_normal_active


-- Recolor Layout icons:
theme = theme_assets.recolor_layout(theme, theme.fg_normal)

-- Recolor titlebar icons:
--
local function darker(color_value, darker_n)
    local result = "#"
    for s in color_value:gmatch("[a-fA-F0-9][a-fA-F0-9]") do
        local bg_numeric_value = tonumber("0x"..s) - darker_n
        if bg_numeric_value < 0 then bg_numeric_value = 0 end
        if bg_numeric_value > 255 then bg_numeric_value = 255 end
        result = result .. string.format("%2.2x", bg_numeric_value)
    end
    return result
end
theme = theme_assets.recolor_titlebar(
    theme, theme.bg_focus, "normal"
)
theme = theme_assets.recolor_titlebar(
    theme, darker(theme.bg_focus, -60), "normal", "hover"
)
theme = theme_assets.recolor_titlebar(
    theme, theme.fg_focus, "normal", "press"
)
theme = theme_assets.recolor_titlebar(
    theme, theme.bg_normal, "focus"
)
theme = theme_assets.recolor_titlebar(
    theme, darker(theme.bg_normal, -70), "focus", "hover"
)
theme = theme_assets.recolor_titlebar(
    theme, theme.fg_normal, "focus", "press"
)

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = "breeze-dark"

-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(
    theme.menu_height, theme.bg_focus, theme.fg_focus
)

-- Generate taglist squares:
local taglist_square_size = dpi(6)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

theme.notification_position = 'top_middle'
theme.notification_shape = function(c, w, h) gears.shape.rounded_rect(c, w, h, 7) end
theme.notification_border_width = 0
theme.notification_margin = dpi(18)
theme.notification_icon_size = dpi(64)


return theme
