-- Widget and layout library
local wibox = require "wibox"
local awful = require "awful"
local gears = require "gears"
local beautiful = require "beautiful"

local helpers = require("helpers")

local mytooltip = require "widgets.tooltip"

-- widgets
-- local cpu_widget = require "external.awesome-wm-widgets.cpu-widget.cpu-widget"
local cpu_widget = require "widgets.cpu"
local batteryarc_widget = require "widgets.batteryarc_dbus"
local memory_widget = require "widgets.membar"
local disks_widget = require "widgets.disks"
local volumebar_widget = require "external.awesome-wm-widgets.volumebar-widget.volumebar"
local brightness_widget = require "external.awesome-wm-widgets.brightness-widget.brightness"
local calendar_widget = require "external.awesome-wm-widgets.calendar-widget.calendar"

local volume_widget = require "widgets.volume"

local textclock = wibox.widget.textclock(" %F <big><b>%R</b></big> <small><i>%a w%V</i></small> ")
-- Add a calendar widget to it
local cw = calendar_widget({
        theme = 'dark',
        placement = 'top_right',
        week_numbers = true
})
textclock:connect_signal("button::press",
                         function(_, _, _, button)
                             if button == 1 then cw.toggle() end
                         end
)

local pipette_widget = require "widgets.pipette"
pipette_widget:buttons(awful.button({}, 1, function()
                               awful.spawn.raise_or_spawn('kcolorchooser',
                                                          { class = "kcolorchooser"},
                                                          function(c)
                                                              if c.class == "kcolorchooser" then
                                                                  client.focus = c
                                                                  return true
                                                              end
                                                              return false
                                                          end,
                                                          "pipette")
end))

local clientsmenu_widget = require "widgets.clientsmenu"

-- local todo_widget = require "todo"

local mainmenu = require "widgets.mainmenu"


local function client_list_menu_toggle_fn()
    local instance = nil

    return function ()
        if instance and instance.wibox.visible then
            instance:hide()
            instance = nil
        else
            local menu_items = {}
            for s in screen do
                if screen.count() > 1 then
                    menu_items[#menu_items + 1] =
                        {"__[ SCREEN " .. tostring(s.index) .. ' ]__',
                         nil,
                         nil}
                end
                for _, t in ipairs(s.tags) do
                    menu_items[#menu_items + 1] =
                        {"....................................[ " .. t.name .. ' ]....................................',
                         function ()
                             t:view_only()
                         end,
                         nil}
                    for _, c in ipairs(helpers.sort.clients(t:clients())) do
                        menu_items[#menu_items + 1] =
                            {c.name or "",
                             function()
                                 if not c.valid then return end
                                 if not c:isvisible() then
                                     awful.tag.viewmore(c:tags(), c.screen)
                                 end
                                 c:emit_signal("request::activate", "menu.clients", {raise=true})
                             end,
                             c.icon}
                    end
                end
            end

            instance = awful.menu({ items = menu_items,
                                    theme = { width = 400, border_width = 3 }
            })
            helpers.click_to_hide.menu(instance, function() instance:hide(); instance = nil end)

            instance:show()
        end
    end
end

local function client_menu_toggle_fn()
    local instance = nil
    return function(c)
        if instance then
            instance.visible = not instance.visible
        else
            instance = awful.popup({
                    widget = {
                        {
                            awful.titlebar.widget.iconwidget(c),
                            awful.titlebar.widget.titlewidget(c),
                            layout  = wibox.layout.fixed.horizontal
                        },
                        awful.titlebar.widget.closebutton    (c),
                        awful.titlebar.widget.maximizedbutton(c),
                        awful.titlebar.widget.minimizebutton (c),
                        awful.titlebar.widget.stickybutton   (c),
                        awful.titlebar.widget.ontopbutton    (c),
                        awful.titlebar.widget.floatingbutton (c),
                        spacing = 1,
                        layout = wibox.layout.fixed.vertical
                    },
                    hide_on_right_click = true,
                    visible = true,
                    ontop = true,
                    bg = beautiful.bg_focus,
                    width = 50,
                    placement = awful.placement.top_left,
                    shape = gears.shape.rounded_rect,
            })
        end
    end
end


-- {{{ Wibar

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
    awful.button({ }, 1, function(t) t:view_only() end),
    awful.button({ modkey }, 1,
        function(t)
            if client.focus then
                client.focus:move_to_tag(t)
            end
        end
    ),
    awful.button({ }, 3, awful.tag.viewtoggle),
    awful.button({ modkey }, 3, function(t)
            if client.focus then
                client.focus:toggle_tag(t)
            end
    end),
    awful.button({ }, 4, function(t) awful.tag.viewprev(t.screen) end),
    awful.button({ }, 5, function(t) awful.tag.viewnext(t.screen) end)
)

local tasklist_buttons = gears.table.join(
    awful.button({ }, 1,
        function (c)
            if c == client.focus then
                c.minimized = true
            else
                -- Without this, the following
                -- :isvisible() makes no sense
                c.minimized = false
                if (not c:isvisible()) and c.first_tag then
                    c.first_tag:view_only()
                end
                -- This will also un-minimize
                -- the client, if needed
                client.focus = c
                c:raise()
            end
        end
    ),
    awful.button({ }, 3,
        -- client_menu_toggle_fn()),
        client_list_menu_toggle_fn()),
    awful.button({ }, 4, function ()
            awful.client.focus.byidx(-1)
    end),
    awful.button({ }, 5, function ()
            awful.client.focus.byidx(1)
end))


-- p = {screen = screen.primary}


------------------------------------------------------

-- FIXME: create in update_function of primary_tasklist
-- local tasklist_tooltip = awful.tooltip {
--    objects = { primary_taskist },
--    timer_function = function()
--       return c.name
--    end,
-- }
-- tasklist_tooltip.align = "bottom"
-- tasklist_tooltip.mode = "outside"
-- tasklist_tooltip.preferred_positions = {"bottom", "top"}
-- tasklist_tooltip.preferred_alignments = {"middle"}

-- Add widgets to the wibox
local function setup_navigation_bar(bar)
    local taglist = awful.widget.taglist({
            screen = bar.screen,
            filter = awful.widget.taglist.filter.all,
            buttons = taglist_buttons,
            widget_template = {
                {
                    {
                        {
                            id     = 'icon_role',
                            widget = wibox.widget.imagebox,
                        },
                        margins = 2,
                        widget  = wibox.container.margin,
                    },
                    id     = 'background_role',
                    widget = wibox.container.background,
                },
                shape  = gears.shape.circle,
                widget = wibox.container.background,
                    create_callback = function(self, c3, index, objects) --luacheck: no unused args
                        local background = self:get_children_by_id('background_role')[1]
                        self:connect_signal('mouse::enter', function()
                                                if not self.has_backup then
                                                    self.backup     = background.bg
                                                    self.has_backup = true
                                                end
                                                background.bg = '#073707'
                        end)
                        self:connect_signal('button::release', function()
                                                if background.bg ~= self.backup then
                                                    self.backup     = background.bg
                                                    self.has_backup = true
                                                end
                        end)
                        self:connect_signal('mouse::leave', function()
                                                if self.has_backup then
                                                    background.bg = self.backup
                                                    self.has_backup = false
                                                end
                        end)
                    end,

            },
    })
    local tasklist = awful.widget.tasklist({
            screen = bar.screen,
            filter = awful.widget.tasklist.filter.currenttags,
            buttons = tasklist_buttons,
            source = function()
                -- client.get() is sorted by position.
                -- sorting a table in Lua does not guarantee that data will keep
                -- the original order if the sorting criteria is the same.
                -- To avoid this problem, we add an index to the list of the clients,
                -- then sort using this index
                local indexed_clients = client.get()
                for i = 1, #indexed_clients do
                    indexed_clients[i]._sorting_id = i
                end
                return helpers.sort.clients(indexed_clients,
                                            function(a, b)
                                                return (
                                                    helpers.sort.by_bool(
                                                        a.maximized, b.maximized,
                                                        helpers.sort.by_bool(
                                                            a.floating, b.floating,
                                                            helpers.sort.by_num(
                                                                a._sorting_id, b._sorting_id
                                                ))))
                                            end
                )
            end,
            widget_template = {
                {
                    {
                        {
                            {
                                id     = 'icon_role',
                                widget = wibox.widget.imagebox,
                            },
                            margins = 2,
                            widget  = wibox.container.margin,
                        },
                        {
                            id     = "text_role",
                            widget = wibox.widget.textbox,
                        },
                        -- fill_space = true,
                        layout     = wibox.layout.fixed.horizontal
                    },
                    -- id     = "text_margin_role",
                    left   = 6,
                    right  = 6,
                    bottom = 2,
                    widget = wibox.container.margin
                },
                id     = "background_role",
                widget = wibox.container.background,
                create_callback = function(self, c, index, clients)
                    local function resize()
                        if c.maximized then
                            self.forced_width = beautiful.wibar_height * 2
                        elseif c.minimized then
                            self.forced_width = beautiful.wibar_height * 3 / 4
                        else
                            self.forced_width = c.width * 0.2
                        end
                    end
                    resize()
                    c:connect_signal("property::width", resize)
                    c:connect_signal("property::maximized", resize)
                    c:connect_signal("property::minimized", resize)
                    c:connect_signal("property::floating", resize)
                    self._tooltip = mytooltip {
                        margin_top = 13,
                        margin_bottom = 5,
                        margin_leftright = 10,
                        -- height = 130,
                        mode = "outside",
                        preferred_alignments = { "middle", "front", "back" },
                        objects = { self },
                        timer_function = function()
                            return gears.string.xml_escape(c.name)  or ""
                        end,
                        shape = gears.shape.infobubble,
                    }
                end
                -- create_callback = function(s, c, index, objects)
                --     s:connect_signal("mouse::enter",
                --                      function()
                --                          c.saved_ontop = c.ontop
                --                          c.ontop = true
                --                          c.saved_focus = client.focus
                --                          if not c.minimized then
                --                              c:activate()
                --                          end
                --                      end
                --     )
                --     s:connect_signal("mouse::leave",
                --                      function()
                --                          c.ontop = c.saved_ontop
                --                          c.saved_focus:activate()
                --                      end
                --     )
                -- end,
            },
    })
    local launcher = awful.widget.launcher({
            image = beautiful.awesome_icon,
            menu = mainmenu
    })
    launcher.clip_shape = function(cr, w, h) gears.shape.partially_rounded_rect(cr, w, h, false, false, true, false, 7) end
    local layoutbox = awful.widget.layoutbox(bar.screen)
    layoutbox:buttons(gears.table.join(
                          awful.button({ }, 1, function () awful.layout.inc( 1) end),
                          awful.button({ }, 2, function () awful.layout.set(awful.layout.suit.tile) end),
                          awful.button({ }, 3, function () awful.layout.inc(-1) end),
                          awful.button({ }, 4, function () awful.layout.inc( 1) end),
                          awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    local promptbox = awful.widget.prompt()

    bar:setup {
        { -- Left widgets
            launcher,
            {
                {
                    taglist,
                    promptbox,
                    layout = wibox.layout.fixed.horizontal,
                },
                left = 4,
                right = 4,
                widget = wibox.container.margin
            },
            layout = wibox.layout.fixed.horizontal,
        },
        tasklist, -- Middle widget
        { -- Right widgets
            -- todo_widget(),
            -- awful.widget.keyboardlayout(),
            -- wibox.widget.systray(),
            clientsmenu_widget,
            textclock,
            layoutbox,
            layout = wibox.layout.fixed.horizontal,
        },
        layout = wibox.layout.align.horizontal,
    }
end

local function setup_diagnostic_bar(bar)
    if not bar then return end
    bar:setup {
        wibox.widget.systray(true),
        {
            memory_widget,
            cpu_widget,
            disks_widget,
            pipette_widget,
            spacing = 20,
            layout = wibox.layout.fixed.horizontal,
        },
        {
            {
                brightness_widget({
                        get_brightness_cmd = 'xbacklight -get',
                        inc_brightness_cmd = 'xbacklight -inc 5',
                        dec_brightness_cmd = 'xbacklight -dec 5',
                        path_to_icon = awful.util.get_configuration_dir() .. "icons/brightness-6.svg"
                }),
                volumebar_widget({
                        main_color = beautiful.bg_secondary,
                        mute_color = beautiful.xcolor1,
                        width = 100,
                        shape = 'rounded_bar',
                        margins = 8
                }),
                volume_widget,
                batteryarc_widget({
                        show_current_level = true,
                        batteries = {
                            {name = "BAT1", color = beautiful.bg_primary},
                            {name = "BAT2", color = beautiful.bg_secondary}
                        },
                }),
                spacing = 15,
                layout = wibox.layout.fixed.horizontal,
            },
            halign = 'right',
            widget = wibox.container.place
        },
        layout = wibox.layout.align.horizontal,
        expand = "outside",
    }
end

local function setup_minimal_bar(bar)
    if not bar then return end
    local layoutbox = awful.widget.layoutbox(bar.screen)
    layoutbox:buttons(gears.table.join(
                          awful.button({ }, 1, function () awful.layout.inc( 1) end),
                          awful.button({ }, 2, function () awful.layout.set(awful.layout.suit.tile) end),
                          awful.button({ }, 3, function () awful.layout.inc(-1) end),
                          awful.button({ }, 4, function () awful.layout.inc( 1) end),
                          awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    bar:setup {
        awful.widget.taglist(bar.screen, awful.widget.taglist.filter.all, taglist_buttons),
        textclock,
        layoutbox,
        layout = wibox.layout.align.horizontal,
        expand = "none",
    }
end


local primary_bars = {top = awful.wibar({position = "top", screen = screen.primary-- , bg = "#00000000"
}),
                      bottom = awful.wibar({position = "bottom", screen = screen.primary})}
setup_navigation_bar(primary_bars.top)
setup_diagnostic_bar(primary_bars.bottom)

local output_bars = {}

local function setup_screens()
    local ob_index = 0
    -- Let's assign bars to screens
    for s in screen do
        if (s == screen.primary) then
            s.bars = primary_bars
            for _, b in ipairs(primary_bars) do
                local changed = b.screen == s
                b.screen = s
                if changed then b:emit_signal("widget::layout_changed") end
            end
        else
            ob_index = ob_index + 1
            -- If needed, we create a new output bar
            if not output_bars[ob_index] then
                output_bars[ob_index] = {top = awful.wibar({ position = "top", screen = s})}
                setup_minimal_bar(output_bars[ob_index].top)
            else
                for _, b in ipairs(output_bars[ob_index]) do
                    local changed = b.screen == s
                    b.screen = s
                    if changed then b:emit_signal("widget::layout_changed") end
                end
            end
        end
    end
    -- Now, let's delete all the useless bars
    ob_index = ob_index + 1
    while output_bars[ob_index] do
        output_bars[ob_index] = nil
        ob_index = ob_index + 1
    end
end

screen.connect_signal("request::desktop_decoration", setup_screens)
screen.connect_signal("primary_changed", setup_screens)



local function toggle(force_visible)
    local _, firstbar = next(mouse.screen.bars)
    local visibility = (force_visible ~= nil) and force_visible or (not firstbar.visible)
    for _, b in pairs(mouse.screen.bars) do
        -- all the bars should have the same visibility
        b.visible = visibility
    end
end


return {toggle = toggle}
