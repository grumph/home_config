-- Expected behaviour:
--
-- I want mouse follow focus when:
--  * changing focus
--  * changing tag
--  * swapping clients
--  * add/remove a client (new, minimizing, raising, ...)
-- And I don't want to move the mouse when:
--  * doing actions with the mouse
--    * moving/resizing
--    * focus changed when focus follow mouse
--    * clicking the tasklist
--  * a client is raising on another tag


local gtimer = require "gears.timer"
local awful = require "awful"

function is_client_on_current_tag(c)
    for _, mouse_tag in ipairs(mouse.screen.selected_tags) do
        for _, client_tag in ipairs(c:tags()) do
            if client_tag == mouse_tag then
                return true
            end
        end
    end
    return false
end

local next_client_for_move_mouse = nil
local mouse_coords = mouse.coords()

local function move_mouse_to_client()
    local is_valid = pcall(function() return next_client_for_move_mouse.valid end) and next_client_for_move_mouse.valid  -- as seen in awesome documentation
    if is_valid then
        awful.placement.centered(mouse, {parent=next_client_for_move_mouse})
    end
end

local function move_mouse_to_client_if_mouse_static()
    if mouse_coords.x == mouse.coords().x and mouse_coords.y == mouse.coords().y then
        move_mouse_to_client()
    end
end

local move_mouse_timer = gtimer {
        timeout = 0.1,
        autostart = true,
        callback = move_mouse_to_client_if_mouse_static,
        single_shot = true,
}

function ask_move_mouse_to_client(c, skip_mouse_check)
    c = c or client.focus
    local mcc = mouse.current_client

    if mcc ~= nil                                                    -- disable for toolbars clicks
        and (skip_mouse_check
                 or (c ~= mcc                                        -- disable when mouse is already inside client
                     and not mouse.is_left_mouse_button_pressed      -- disable if mouse is used
                         and not mouse.is_right_mouse_button_pressed
                         and not mouse.is_middle_mouse_button_pressed
        ))
    and is_client_on_current_tag(c)                                  -- only if client is on a selected tag
    and c.type ~= "utility"
    -- and c.type == "normal"                                           -- only for normal clients
    then
        next_client_for_move_mouse = c
        mouse_coords = mouse.coords()
        -- io.stderr:write(" **** mouse coords saved\n")
        move_mouse_timer:again()
    end
end

----------------------
-- Manage focus change
-- Doing this with the focus signal interferes with "focus follow mouse"

client.connect_signal("raised", ask_move_mouse_to_client)


-------------------------
-- Manage swapped clients
-- This also handles add/remove client

client.connect_signal("swapped",
                      function(c1, c2)
                          if client.focus ~= c1 then
                              ask_move_mouse_to_client(c2, true)
                          end
                      end
)


--------------------
-- Manage tag switch

local next_focused_client_gets_mouse = false

screen.connect_signal("tag::history::update",
                      function()
                          next_focused_client_gets_mouse = true
                      end
)

client.connect_signal("focus",
                      function(c)
                          if next_focused_client_gets_mouse then
                              ask_move_mouse_to_client(c, true)
                              next_focused_client_gets_mouse = false
                          end
                      end
)
