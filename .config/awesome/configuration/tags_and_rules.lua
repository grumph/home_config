local gears = require "gears"
local awful = require "awful"
local sharedtags = require "external.awesome-sharedtags"
local beautiful = require "beautiful"
local ruled = require "ruled"


-- beautiful.taglist_font must be a NerdFont
local tags = sharedtags({
        { name = "Home",
          icon = awful.util.get_configuration_dir() .. "icons/home.svg",
          layout = awful.layout.layouts[2] },
        { name = "Dev",
          icon = awful.util.get_configuration_dir() .. "icons/code-braces.svg",
          layout = awful.layout.layouts[1] },
        { name = "Email",
          icon = awful.util.get_configuration_dir() .. "icons/at.svg",
          layout = awful.layout.layouts[6] },
        { name = "Notes",
          icon = awful.util.get_configuration_dir() .. "icons/file-document-outline.svg",
          layout = awful.layout.layouts[3] },
        { name = "Music",
          icon = awful.util.get_configuration_dir() .. "icons/speaker-wireless.svg",
          layout = awful.layout.layouts[4] }
})

local output_tag = sharedtags({
        { name = "Output",
          icon = awful.util.get_configuration_dir() .. "icons/projector-screen-outline.svg",
          screen = 2,
          layout = awful.layout.suit.max }
})


screen.connect_signal("request::desktop_decoration",
                      function()
                          -- move all tags to corresponding screens
                          for _, t in ipairs(tags) do
                              sharedtags.movetag(t, screen.primary)
                          end
                          for _, t in ipairs(output_tag) do
                              io.stderr:write("screen.count() is " .. tostring(screen.count()) .. "\n")
                              if screen.count() == 1 then
                                  -- sharedtags.viewonly(t, screen.primary)
                                  local c = t:clients()
                                  if #c > 0 then
                                      sharedtags.movetag(t, screen.primary)
                                  end
                              else
                                  for s in screen do
                                      -- FIXME: only works with 2 screens, for more use tyranical
                                      sharedtags.viewonly(t, s)
                                      -- sharedtags.movetag(t, s)
                                  end
                              end
                          end
                      end
)


clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 2, function (c) c.minimized = true; end),
    awful.button({ modkey }, 3, awful.mouse.client.resize
))

-- Rules to apply to new clients (through the "manage" signal).
ruled.client.append_rule {
    rule = { },  -- All clients will match this rule.
    properties = { border_width = beautiful.border_width,
                   border_color = beautiful.border_normal,
                   focus = awful.client.focus.filter,
                   raise = true,
                   -- keys = clientkeys,
                   buttons = clientbuttons,
                   screen = awful.screen.preferred,
                   placement = awful.placement.no_offscreen,
                   titlebars_enabled = false,
                   titlebars_enabled_hack = false
    }
}

ruled.client.append_rule {
    rule = { type = "utility" },
    properties = { border_width = 0 }
}


-- Floating clients.
ruled.client.append_rule {
    rule_any = {
        instance = {
            "DTA",  -- Firefox addon DownThemAll.
            "copyq",  -- Includes session name in class.
        },
        class = {
            "Arandr",
            "Gpick",
            "Kruler",
            "krunner",
            "MessageWin",  -- kalarm.
            "Sxiv",
            "Wpa_gui",
            "pinentry",
            "veromix",
            "xtightvncviewer",
            "mojosetup",  -- GOG installer
        },
        name = {
            "Event Tester",  -- xev.
            "GOGPostinstall",
        },
        role = {
            "AlarmWindow",  -- Thunderbird's calendar.
            "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
    },
    properties = { floating = true }
}

-- Add titlebars to normal clients and dialogs
ruled.client.append_rule {
    rule_any = {type = { "normal", "dialog" }},
    except_any = { class = {"Oblogout", "Clearine"} },
    properties = {
        titlebars_enabled = true,
        titlebars_enabled_hack = true}
}

-- Logout windows
ruled.client.append_rule {
    rule_any = { class = {"Oblogout", "Clearine"} },
    properties = { fullscreen = true,
                   floating = true,
                   ontop = true,
                   maximized = true,
                   sticky = true,
                   border_width = 0}
}

-- Set specific tags for apps
ruled.client.append_rule {
    rule_any = { class = {"Firefox", "firefox", "Navigator"} },
    properties = { tag = tags["Home"] }
}


ruled.client.append_rule {
    rule = { class = "firefox", name = "Picture-in-Picture" },
    properties = { floating = false,
                   sticky = true}
}

ruled.client.append_rule {
    rule = { class = "firefox", name = "^[Ww]eb.*" },
    properties = { tag = tags["Dev"] }
}

ruled.client.append_rule {
    rule = { class = "konsole", name = "^ET :.*" },
    properties = { tag = tags["Dev"] }
}

ruled.client.append_rule {
    rule = { class = "Zim" },
    properties = { tag = tags["Notes"] }
}

ruled.client.append_rule {
    rule = { class = "Thunderbird" },
    properties = { tag = tags["Email"] }
}

ruled.client.append_rule {
    rule_any = { class = {"keepassxc", "KeePassXC", "Telegram", "TelegramDesktop" }},
    properties = {sticky = true}
}


ruled.client.append_rule {
    rule_any = { class = {"gravity-pi", "Gravity"}},
    properties = {floating = true}
}

ruled.client.append_rule {
    rule_any = { class = {"arandr", "Arandr"} },
    properties = {
        floating = true,
        ontop = true,
        sticky = true,
        callback = function(c)
            -- grow the window then center it (it's the other way when using properties directly in the rule)
            c.width = screen.primary.workarea.width / 2
            c.height = screen.primary.workarea.height / 2
            awful.placement.centered(c)
        end,
    }
}


-- local no_fullscreen = false
-- local rule = { class = "Firefox" }
-- client.disconnect_signal("request::geometry", awful.ewmh.geometry)
-- client.connect_signal("request::geometry",
--                       function(c, context, ...)
--                          if (not no_fullscreen) or (context ~= "fullscreen") or (not awful.rules.match(c, rule)) then
--                             awful.ewmh.geometry(c, context, ...)
--                          end
--                       end
-- )


-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
awful.keyboard.append_global_keybindings({
        awful.key {
            modifiers = { modkey },
            keygroup = "numrow",
            description = "only view tag",
            group       = "tag",
            on_press    = function (i)
                local s = awful.screen.focused()
                if (s == screen.primary) then
                    local t = tags[i]
                    if t then
                        sharedtags.viewonly(t, s)
                    end
                end
            end,
        },
        awful.key {
            modifiers = { modkey, "Control" },
            keygroup = "numrow",
            description = "toggle tag",
            group       = "tag",
            on_press    = function (i)
                local s = awful.screen.focused()
                if (s == screen.primary) then
                    local t = tags[i]
                    if t then
                        sharedtags.viewtoggle(t, s)
                    end
                end
            end,
        },
        awful.key {
            modifiers = { modkey, "Shift" },
            keygroup = "numrow",
            description = "move focused client to tag",
            group       = "tag",
            on_press    = function (i)
                if client.focus then
                    local t = tags[i]
                    if t then
                        client.focus:move_to_tag(t)
                    end
                end
            end,
        },
        awful.key {
            modifiers = { modkey, "Control", "Shift" },
            keygroup = "numrow",
            description = "toggle focused client on tag",
            group       = "tag",
            on_press    = function (i)
                if client.focus then
                    local t = tags[i]
                    if t then
                        client.focus:toggle_tag(t)
                    end
                end
            end,
        },
        awful.key(
            { modkey, "Control", "Shift" }, "o",
            function ()
                local next_screen_index = (awful.screen.focused().index % screen.count()) + 1
                sharedtags.viewonly(awful.screen.focused().selected_tag, next_screen_index)
            end,
            {description = "move current tag to other screen", group = "client"}),

})

require "configuration.spectacle_center"
