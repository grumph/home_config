local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi


-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
                          -- buttons for the titlebar
                          local buttons = gears.table.join(
                              awful.button({ }, 1, function()
                                      client.focus = c
                                      c:raise()
                                      awful.mouse.client.move(c)
                              end),
                              awful.button({ }, 3, function()
                                      client.focus = c
                                      c:raise()
                                      awful.mouse.client.resize(c)
                              end)
                          )

                          awful.titlebar(c) : setup
                          {
                              {
                                  { -- Left
                                      awful.titlebar.widget.closebutton    (c),
                                      awful.titlebar.widget.maximizedbutton(c),
                                      awful.titlebar.widget.minimizebutton(c),
                                      awful.titlebar.widget.stickybutton   (c),
                                      awful.titlebar.widget.ontopbutton    (c),
                                      awful.titlebar.widget.floatingbutton (c),
                                      spacing = dpi(1),
                                      layout  = wibox.layout.fixed.horizontal
                                  },
                                  {
                                      { -- Middle
                                          awful.titlebar.widget.iconwidget(c),
                                          awful.titlebar.widget.titlewidget(c),
                                          layout  = wibox.layout.fixed.horizontal
                                      },
                                      buttons = buttons,
                                      halign = "center",
                                      widget = wibox.container.place,
                                  },
                                  layout = wibox.layout.align.horizontal
                              },
                              bottom = dpi(2),
                              top = dpi(1),
                              widget = wibox.container.margin,
                          }
end)

--- Set titlebar for a client
local function set_titlebar(c, visible)
    if (not c.requests_no_titlebar) and c.titlebars_enabled_hack and visible then
        awful.titlebar.show(c)
    else
        awful.titlebar.hide(c)
    end
end



client.connect_signal("request::manage", function (c)
                          set_titlebar(c, global_titlebars_visible)
end)


local visible = true
--- Toggle visibility for all clients
--
-- @bool[opt=nil] force_visible Enforce visibility. If `nil`, just toggle.
local function toggle(force_visible)
    visible = (force_visible ~= nil) and force_visible or (not visible)
    for _, c in ipairs(client.get()) do
        set_titlebar(c, visible)
    end
end



return {toggle = toggle}
