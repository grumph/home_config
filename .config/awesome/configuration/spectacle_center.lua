-- Hack to center spectacle on start
-- spectacle resize itself after its window is managed by awesome
-- so if we want to center it, we have to do it after this

local spectacle_just_started = nil

client.connect_signal("request::manage",
                      function(c)
                          if c.class == "spectacle" then
                              spectacle_just_started = c
                          end
                      end
)


client.connect_signal("request::geometry",
                      function(c)
                          if c.class == "spectacle" and spectacle_just_started == c then
                              spectacle_just_started = nil
                              c:geometry {
                                  x = c.screen.workarea.width / 2 - c.width / 2,
                                  y = c.screen.workarea.height / 2 - c.height / 2,
                                  width = c.width,
                                  height = c.height,
                              }
                          end
                      end
)
