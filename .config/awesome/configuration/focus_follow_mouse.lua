
-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
                          -- io.stderr:write("mouse enter :" .. tostring(c) .."\n")
                          c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

