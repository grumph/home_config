local ruled = require "ruled"
local awful = require "awful"


local function insert_client_after_current_focused(c)
    -- TODO
end

local function insert_client_before_current_focused(c)
    -- https://old.reddit.com/r/awesomewm/comments/g41lti/how_to_create_new_windows_at_the_current_position/fosc8ty/
    if not awesome.startup then
        local my_unfocus = awful.client.focus.history.get(c.screen, 1, nil)
        if my_unfocus then
            awful.client.setslave(c)
            local c_step_init = nil
            while true do
                local c_step = awful.client.next(-1, c)
                if c_step_init == nil then
                    c_step_init = c_step
                else
                    if c_step_init == c_step then
                        -- Prevent eternal loop.
                        break
                    end
                end
                c:swap(c_step)
                if c_step == my_unfocus then
                    break
                end
            end
        end
    end
end


-- -- dirty fix for FIXME in rule below
-- client.connect_signal("request::manage", function (c)
--                           insert_client_after_current_focused(c)
-- end)


ruled.client.append_rule {
    rule = { },
    -- callback = awful.client.setmaster
    --FIXME --callback = insert_client_after_current_focused
    callback = awful.client.setslave
}
