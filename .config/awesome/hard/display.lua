local awful = require 'awful'

local brightness = {
    current_value = 0,
    step = 7,
    minimum = 5,
    maximum = 100,
}

function brightness:_update_current_value ()
    awful.spawn.easy_async(
        "xbacklight -get",
        function(stdout)-- , stderr, reason, exit_code
            self.current_value = tonumber(stdout);
            -- TODO: emit signal here
        end
    )
end

brightness._toggle_value = 0

function brightness:toggle (force_to)
    if force_to == true then
        self.set(self._toggle_value)
    elseif force_to == false then
        self._toggle_value = self.current_value
        self:set(self.minimum)
    else
        local is_off = self.current_value <= self.minimum
        self:toggle(is_off)
    end
end

function brightness:lower (percent)
    local value = tonumber(percent) or self.step
    awful.spawn.easy_async(
        "xbacklight -dec " .. tostring(value),
        function()
            self:_update_current_value()
        end
    )
end

function brightness:raise (percent)
    local value = tonumber(percent) or self.step
    awful.spawn.easy_async(
        "xbacklight -inc " .. tostring(value),
        function()
            self:_update_current_value()
        end
    )
end

function brightness:set (percent)
    local value = tonumber(percent) or (self.minimum + (self.maximum - self.minimum) / 2)
    awful.spawn.easy_async(
        "xbacklight -set " .. tostring(value),
        function()
            self:_update_current_value()
        end
    )
end



local function print_screen()
    awful.spawn("spectacle");
end


return {
    print_screen = print_screen,
    brightness = brightness,
}
