local awful = require 'awful'
local beautiful = require 'beautiful'
local gears = require 'gears'

local gsound = require 'widgets.gsound'

local sounds = beautiful.sounds or {
    all_mute = false,
    volume = "message",
    -- display = nil,
                                   }

local play = function(sound)
    if not sounds.all_mute and sound ~= nil then
        gsound.play({sound = sound})
        -- awful.spawn("paplay /home/grumph/.config/awesome/mytheme/sound.wav")
    end
end

local volume = {
    current_value = 0,
    step = 5,
    minimum = 5,
    maximum = 100,
}

local update_timer = gears.timer {
    timeout = 0.2,
    single_shot = true,
    callback = function()
        awful.spawn.easy_async_with_shell(
            "pactl list short sinks | sed -e 's,^\\([0-9][0-9]*\\)[^0-9].*,\\1,' | head -n 1",
            function(sink)
                awful.spawn.easy_async_with_shell(
                    "pactl list sinks | grep '^[[:space:]]Volume:' | head -n $(( ".. sink .. " + 1 )) | tail -n 1 | sed -e 's,.* \\([0-9][0-9]*\\)%.*,\\1,' ",
                    function(stdout)
                        volume.current_value = tonumber(stdout)
                        awesome.emit_signal("hard::audio::volume", volume.current_value)
                    end
                )
            end
        )
    end
}

local default_callback = function()
    play(sounds.volume)
    update_timer:again()
end

function volume:toggle (force_to, callback)
    local action = "toggle"
    if force_to ==  true or force_to == false then
        action = tostring(force_to)
    end
    awful.spawn.easy_async(
        "pactl set-sink-mute @DEFAULT_SINK@ " .. action,
        callback or default_callback
    )
end

function volume:lower (percent, callback)
    local value = tonumber(percent) or self.step
    self:toggle(false,
                function()
                    awful.spawn.easy_async(
                        "pactl set-sink-volume @DEFAULT_SINK@ -" .. tostring(value) .. "%",
                        callback or default_callback
                    )
                end
    )
end

function volume:raise (percent, callback)
    local value = tonumber(percent) or self.step
    self:toggle(false,
                function()
                    awful.spawn.easy_async(
                        "pactl set-sink-volume @DEFAULT_SINK@ +" .. tostring(value) .. "%",
                        callback or default_callback
                    )
                end
    )
end

function volume:set (percent, callback)
    local value = tonumber(percent) or (self.minimum + (self.maximum - self.minimum) / 2)
    self:toggle(false,
                function()
                    awful.spawn.easy_async(
                        "pactl set-sink-volume @DEFAULT_SINK@ ".. tostring(value) .. "%",
                        callback or default_callback
                    )
                end
    )
end

update_timer:again()

return {
    play = play,
    volume = volume,
}
