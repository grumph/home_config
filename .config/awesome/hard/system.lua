local awful = require "awful"



-- Emit sleep or resume signals
awful.spawn.with_line_callback([[dbus-monitor --system "type='signal',interface='org.freedesktop.login1.Manager',member=PrepareForSleep"]], {
        stdout = function(line)
            if line == "   boolean false" then
                awesome.emit_signal("hard::system::resume")
            elseif line == "   boolean true" then
                awesome.emit_signal("hard::system::sleep")
            end
        end,
})
