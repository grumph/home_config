-- volume:
--    - manage: pulse, alsa, jack
--    - play: gsound, paplay, aplay
-- brightness: xbacklight
-- lockscreen: xscreensaver, builtin
-- system (power, sleep, reboot, etc...): systemd, openrc, etc...
-- wifi: iwctl, networkmanager
-- bluetooth: bluetoothctl

return {
    system = require 'hard.system',
    audio = require 'hard.audio',
    display = require 'hard.display',
}
