local beautiful = require 'beautiful'
local gears = require 'gears'
local lgi = require 'lgi'
local wibox = require 'wibox'

-- because lgi.Gdk is broken with Gtk4, see https://github.com/pavouk/lgi/issues/226
local gdk = lgi.require('Gdk', '3.0')
-- local gdk = lgi.Gdk
gdk.init({})

function get_pixel_rgb(x, y)
    local w = gdk.get_default_root_window()
    local pb = gdk.pixbuf_get_from_window(w, x, y, 1, 1)
    local bytes = pb:get_pixels()
    return {
        r = string.byte(bytes, 1),
        g = string.byte(bytes, 2),
        b = string.byte(bytes, 3),
    }
end



local colorbox_text = wibox.widget.textbox()
colorbox_text.valign = 'center'

local colorbox_bg = wibox.container.background {
    bg     = '#ffffff',
}
colorbox_bg.forced_width = 20
colorbox_bg.forced_height = 20


colorbox = wibox.widget {
    colorbox_bg,
    colorbox_text,
    layout  = wibox.layout.fixed.horizontal
}



gears.timer {
    timeout = 3,
    call_now = true,
    autostart = true,
    callback = function()
        local mc = mouse.coords()
        local pixel = get_pixel_rgb(mc.x, mc.y)

        local max = 0
        for _, value in pairs(pixel) do
            if value > max then
                max = value
            end
        end

        local function mk(value)
            -- Markupify a color value
            if value > (max - 15) then
                return string.format('<b><big>%02X</big></b>', value)
            else
                return string.format('%02x', value)
            end
        end

        local textcolor = string.format('#%s%s%s', mk(pixel.r), mk(pixel.g), mk(pixel.b))
        local hexcolor = string.format('#%02x%02x%02x', pixel.r, pixel.g, pixel.b)

        colorbox_text.markup = textcolor
        colorbox_bg.bg = hexcolor
    end
}

return colorbox
