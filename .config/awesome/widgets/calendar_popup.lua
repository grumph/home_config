local awful = require("awful")
local beautiful = require("beautiful")
local helpers = require("helpers")

local popup = awful.popup {
    ontop = true,
    visible = false,
    -- shape = function(cr, width, height)
    --     gears.shape.rounded_rect(cr, width, height, 4)
    -- end,
    border_width = 0,
    border_color = beautiful.bg_focus,
    maximum_width = 400,
    offset = {},
    widget = {}
}

helpers.click_to_hide.widget(popup)


calendar_popup:buttons(
    awful.button({}, 1, function()
            if popup and popup.visible then
                popup.visible = false
            else
                popup:setup(build_rows())
                popup:move_next_to(mouse.current_widget_geometry)
                popup.visible = true
            end
    end)
)


return popup
