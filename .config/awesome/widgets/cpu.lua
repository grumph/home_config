local beautiful = require("beautiful")
local vicious = require("vicious")
local wibox = require("wibox")
local awful = require("awful")


vicious.cache(vicious.widgets.cpu)

local cpugraph = wibox.widget.graph()
cpugraph:set_width(100)
cpugraph:set_color{type = "linear", from = {0, 0}, to = {0, 20},
                   stops = {
                       {0.2, beautiful.bg_primary},
                       {0.5, beautiful.bg_secondary},
                       {0.8, beautiful.bg_minimize .. "55"}}}
vicious.register(cpugraph, vicious.widgets.cpu, "$1", 1)


local cpuwidget = wibox.container.margin(wibox.container.mirror(cpugraph, { horizontal = true }), 0, 0, 0, 2)


    local tooltip
    local tooltip_text = "..."
    local function refresh_tooltip()
        awful.spawn.easy_async_with_shell('ps -eo pcpu,pid,user,args --sort=-pcpu | head -n 15 | cut -c -120',
                               function(stdout, _, _, _, _)
                                   tooltip_text = stdout
                                   tooltip.markup = stdout
                                   tooltip.width = 300
                               end
        )
    end
    tooltip = awful.tooltip {
        timer_function = function()
            refresh_tooltip()
            return tooltip_text
        end,
        timeout = 2,
        objects = { cpuwidget },
        mode = "outside",
        preferred_alignments = { "middle", "front", "back" },
        preferred_positions = { "top" },
        margin_topbottom = 14,
        margin_leftright = 20,
    }




return cpuwidget
