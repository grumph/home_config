-------------------------------------------------
-- Battery Arc Widget for Awesome Window Manager
-- Shows the battery level of the laptop
-- Can manage multiple batteries with their own colors
--
-- @author Grumph
--
-- Blithely inspired from:
-- https://github.com/streetturtle/awesome-wm-widgets/tree/master/batteryarc-widget
-------------------------------------------------

local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local naughty = require("naughty")
local wibox = require("wibox")
local helpers = require("helpers")

pcall(require, "luarocks.loader")
local config_path = require("gears.filesystem").get_configuration_dir()
local old_package_path = package.path
package.path = (package.path .. ';'
                .. config_path .. 'external/lua-dbus_proxy/src/?/init.lua;'
                .. config_path .. 'external/lua-dbus_proxy/src/?.lua;')
local upower = require("external.lua-upower_dbus.src.upower_dbus")
package.path = old_package_path

local widget = {}

local function worker(args)

    args = args or {}

    local font = args.font or beautiful.font or 'Sans 6'
    local arc_thickness = args.arc_thickness or 3
    local show_current_level = args.show_current_level or false
    local critical_level = args.critical_level or 15

    local fg_color = args.fg_color or beautiful.fg_normal or "#00aa00"
    local bg_color = args.bg_color or beautiful.bg_normal or "#000000"

    local warning_msg_title = args.warning_msg_title or 'Battery is dying!'
    local warning_msg_text = args.warning_msg_text or 'Please plug in your computer.'
    local warning_msg_position = args.warning_msg_position or 'bottom_right'
    local warning_msg_icon = args.warning_msg_icon
        or helpers.get_script_path() .. 'battery_dying.png'


    local batteries = {}

    -- Widget is in 3 parts: the center text, the background and the arcchart
    local text = wibox.widget {
        font = font,
        align = 'center', -- align the text
        valign = 'center',
        widget = wibox.widget.textbox
    }
    local text_with_background = wibox.container.background(text)
    widget = wibox.widget {
        {
            {
                text_with_background,
                -- rounded_edge = true,
                thickness = arc_thickness,
                paddings = 0,
                start_angle = 4.71238898, -- 2pi*3/4
                bg = gears.color.ensure_pango_color(fg_color .. "22", fg_color), -- try to add transparency
                border_width = 0,
                border_color = beautiful.fg_normal,
                min_value = 0,
                max_value = 100 * #batteries,
                widget = wibox.container.arcchart,
                id = "arcchart"
            },
            margins = 1,
            widget = wibox.container.margin
        },
        bg = beautiful.fg_normal,
        shape = gears.shape.circle,
        widget = wibox.container.background
    }

    -- Variable used for low battery check
    local last_alert_check = os.time()

    -- Update the whole widget
    function update_widget()
        local total_bat = 0
        local available_colors = args.colors or { beautiful.bg_focus }
        local colors = {}
        local values = {}
        local charging = true
        for i, bat in ipairs(batteries) do
            if bat.IsPresent then
                bat:update_mappings()
                total_bat = total_bat + bat.Percentage
                values[i] = bat.Percentage
                colors[i] = available_colors[(i % #available_colors) + 1]
                charging = charging and (bat.state == upower.enums.BatteryState.Charging
                                         or bat.state == upower.enums.BatteryState[5])
            end
        end

        total_bat = math.floor(total_bat / #values)

        -- Update the text
        if show_current_level then
            if total_bat ~= 100 then
                --- if battery is fully charged (100) there is not enough place for
                --- three digits, so we don't show any text
                text.text = string.format('%d', total_bat)
            else
                text.text = '✔'
            end
        end

        -- Update the background, invert colors when charging
        if charging then
            text_with_background.bg = fg_color
            text_with_background.fg = bg_color
        else
            text_with_background.bg = bg_color
            text_with_background.fg = fg_color
        end

        -- Update the arcchart
        local arcchart = widget:get_children_by_id("arcchart")[1]
        arcchart.colors = colors
        arcchart.values = values
        arcchart.max_value = 100 * #values

        -- In case of low battery, show the warning
        if total_bat < critical_level then
            if not charging and os.difftime(os.time(), last_alert_check) > 300 then
                -- if 5 minutes have elapsed since the last warning
                last_alert_check = os.time()
                show_battery_warning()
            end
        end
    end

    local tooltip
    local tooltip_text = "..."
    local function refresh_tooltip()
        awful.spawn.easy_async('acpi',
                               function(stdout, _, _, _, _)
                                   tooltip_text = stdout
                                   tooltip.markup = stdout
                               end
        )
    end
    tooltip = awful.tooltip {
        timer_function = function()
            refresh_tooltip()
            return tooltip_text
        end,
        timeout = 2,
        objects = { widget },
        mode = "outside",
        preferred_alignments = { "middle", "front", "back" },
        preferred_positions = { "top" },
        margin_topbottom = 14,
        margin_leftright = 20,
    }

    -- Show warning notification
    function show_battery_warning()
        naughty.notification {
            icon = warning_msg_icon,
            icon_size = 200,
            title = warning_msg_title,
            message = warning_msg_text,
            timeout = 25, -- show the warning for a longer time
            hover_timeout = 2,
            position = warning_msg_position,
            width = 400,
            urgency = "critical",
            ontop = true
        }
    end

    for _, device in ipairs(upower.Manager.devices) do
        if device.type == upower.enums.DeviceType.Battery then
            table.insert(batteries, upower.create_device(device.object_path))
            -- batteries[#batteries]:on_properties_changed(update_widget)
        end
    end

    local device = upower.create_device("/org/freedesktop/UPower/devices/DisplayDevice")
    device:on_properties_changed(update_widget)
    update_widget()

    return widget

end

return setmetatable(widget, { __call = function(_, ...)
                                  return worker(...)
                   end })
