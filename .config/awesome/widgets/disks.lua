local beautiful = require("beautiful")
local gears = require("gears")
local vicious = require("vicious")
local wibox = require("wibox")


-- Temporary dirty fix to avoid these log lines:
-- df: /run/user/1000/doc: Operation not permitted
-- see https://github.com/flatpak/xdg-desktop-portal/issues/553
local awful = require("awful")
awful.spawn.once("systemctl --user stop xdg-document-portal.service")


vicious.cache(vicious.widgets.fs)


function vicious_wrapper_fs(drive)
    local text = wibox.widget.textbox()
    local progress = wibox.widget.progressbar()
    local bar = wibox.widget {
        {
            color            = beautiful.bg_primary,
            background_color = beautiful.bg_primary .. "75",
            margins          = {
                top = 15,
            },
            forced_width     = 150,
            paddings         = 0,
            shape            = gears.shape.rounded_bar,
            widget           = progress,
        },
        {
            {
                text,
                fg     = beautiful.fg_normal,
                widget = wibox.container.background,
            },
            left   = 5,
            widget = wibox.container.margin,
        },
        layout = wibox.layout.stack
    }

    local tstring = drive .. " ${" .. drive .. " avail_gb}GiB ${" .. drive .. " used_p}%"
    local pstring = "${" .. drive .. " used_p}"

    vicious.register(text, vicious.widgets.fs, tstring, 31)
    vicious.register(progress, vicious.widgets.fs, pstring, 31)

    return bar
end


diskbar = wibox.widget {
    vicious_wrapper_fs("/"),
    vicious_wrapper_fs("/data"),
    layout = wibox.layout.fixed.horizontal,
}


return diskbar
