local awful = require "awful"
local gears = require "gears"

require "helpers.client_functions"


local clientinfo = require "widgets.clientinfo"

local _module_data = {...}
local _module_path = _module_data[1]
-- local _file_path = _module_data[2]

local hk_pop = require (_module_path .. ".widget")
local hkpop = hk_pop.new {
    group_margin = 50,
    width = 500,
    height = 600,
    title_function = function()
        if client.focus then
            return client.focus.name
        else
            return "?????"
        end
    end,
}

local function show_clientbox(c)
    hkpop:show_help(c, -- c.screen or
                    nil, {show_awesome_keys = false})
    client_grabber:start()
end

local function hide_clientbox()
    client_grabber:stop()
    -- Dirty hack?: simulate a keypress to hide the hotkey_popup
    -- first key combination is for the grabber,
    -- second (this one) is for the hotkey_popup
    root.fake_input("key_press", "Super_L")
    root.fake_input("key_release", "Super_L")
end





client_grabber = awful.keygrabber {
    stop_event = "release",
    mask_event_callback = false,
    mask_modkeys = true,
    keypressed_callback = hide_clientbox,
    keybindings = {
        awful.key { modifiers = {}, key = 'n',
                    on_press = function () clientinfo.toggle() end,
                    description = "show clientiNfo", group = "client S-c"},
        awful.key { modifiers = {}, key = 't',
                    on_press = function () toggle_client_prop("ontop") end,
                    description = "on Top", group = "client S-c"},
        awful.key { modifiers = {}, key = 's',
                    on_press = function () toggle_client_prop("sticky") end,
                    description = "Sticky", group = "client S-c"},
        awful.key { modifiers = {}, key = "d",
                    on_press = function () toggle_client_prop("floating") end,
                    description = "floating (Detach)", group = "client S-c"},
        awful.key { modifiers = {}, key = "v",
                    on_press = function () toggle_client_prop("minimized") end,
                    description = "minimize ⌄", group = "client S-c"},
        awful.key { modifiers = {}, key = "f",
                    on_press = function () toggle_client_prop_then_raise("maximized") end,
                    description = "Fillscreen, (maximize)", group = "client S-c"},
        awful.key { modifiers = {}, key = "g",
                    on_press = function () toggle_client_prop_then_raise("fullscreen") end,
                    description = "fullscreen", group = "client S-c"},
        awful.key { modifiers = {}, key = "m",
                    on_press = function () toggle_client_prop_then_raise("maximized_vertical") end,
                    description = "maximize vertically", group = "client S-c"},
        awful.key { modifiers = {}, key = "l",
                    on_press = function () toggle_client_prop_then_raise("maximized_horizontal") end,
                    description = "maximize horizontally", group = "client S-c"},
        awful.key { modifiers = {}, key = 'i',
                    on_press = function () toggle_client_xprop("_PICOM_TAG_INVERT") end,
                    description = "Invert colors", group = "client S-c"},
        awful.key { modifiers = {}, key = "q",
                    on_press = function () client_focus_or_not():kill() end,
                    description = "close, Quit", group = "client S-c" },
        awful.key { modifiers = {}, key = "Return",
                    on_press = function () client_focus_or_not():swap(awful.client.getmaster()) end,
                    description = "move to master", group = "client S-c"},
        awful.key { modifiers = {}, key = "u",
                    on_press = minimize_all_but_focused_on_current_tag,
                    description = "make current client Unique (minimize all others)", group = "client S-c"},
        awful.key { modifiers = { "Shift" }, key = "Left",
                    on_press = function () move_focused_to_tag_relative(-1) end,
                    description = "move to previous tag", group = "client S-c"},
        awful.key { modifiers = { "Shift" }, key = "Right",
                    on_press = function () move_focused_to_tag_relative(1) end,
                    description = "move to next tag", group = "client S-c"},

    }
}


for _, key in ipairs(client_grabber.keybindings) do
    hkpop:add_hotkeys({
            [key.group] = {{
                    modifiers = key.modifiers,
                    keys = {
                        [key.key] = key.description
    }}}})
end


return {
    client = show_clientbox,
    grabber = client_grabber
}
