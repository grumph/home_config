local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")


local clientinfo = wibox({
                          ontop = true, type = "dock",
                          bg = "#4444cc", fg = "#cccccc"})

local ci_text = wibox.widget {
    halign = "center",
    valign = "center",
    widget = wibox.widget.textbox
}

local function update_clientinfo (c)
    c = c or client.focus
    if c.active then
        -- clientinfo.visible = false
        ci_text.markup = "\n\n<big><b>" .. c.name .. "</b></big>" ..
            "\n\n<b>Window ID:</b> " .. tostring(c.window) ..
            "\n<b>Type:</b> " .. (c.type or "") ..
            "\n<b>Class:</b> " .. (c.class or "") ..
            "\n<b>Instance:</b> " .. (c.instance or "") ..
            "\n<b>PID:</b> " .. tostring(c.pid) ..
            "\n<b>Role:</b> " .. (c.role or "") ..
            "\n<b>machine:</b> " .. (c.machine or "") ..
            "\n<b>Icon name:</b> " .. (c.icon_name or "") ..
            "\n<b>Transient for:</b> " .. (gears.debug.dump_return(c.transient_for) or "") ..
            "\n<b>Group window:</b> " .. tostring(c.group_window) ..
            "\n<b>Leader window:</b> " .. tostring(c.leader_window) ..
            "\n\n<b>x</b> = " .. c.x .. ", <b>y</b> = " .. c.y ..
            "\n<b>width</b> = " .. c.width .. ", <b>height</b> = " .. c.height ..
            "\n"
        clientinfo.width = c.width * 0.75
        clientinfo.height = ci_text:get_height_for_width(clientinfo.width) + 60
        local to_top = function(w) return awful.placement.top(w, {parent = c, margins = 30}) end
        to_top(clientinfo)
        clientinfo.visible = true
    end
end


local function show_clientinfo ()
    local c = client.focus
    if c then
        update_clientinfo(c)
    end
    client.connect_signal("property::active", update_clientinfo)
    client.connect_signal("property::x", update_clientinfo)
    client.connect_signal("property::y", update_clientinfo)
    client.connect_signal("property::width", update_clientinfo)
    client.connect_signal("property::height", update_clientinfo)
end


local function hide_clientinfo ()
    client.disconnect_signal("property::active", update_clientinfo)
    client.disconnect_signal("property::x", update_clientinfo)
    client.disconnect_signal("property::y", update_clientinfo)
    client.disconnect_signal("property::width", update_clientinfo)
    client.disconnect_signal("property::height", update_clientinfo)
    clientinfo.visible = false
end

local function toggle_clientinfo ()
    if clientinfo.visible then
        hide_clientinfo()
    else
        show_clientinfo()
    end
end


clientinfo:setup {
    nil,
    {
        nil,
        ci_text,
        nil,
        expand = "none",
        layout = wibox.layout.align.horizontal
    },
    nil,
    expand = "none",
    layout = wibox.layout.align.vertical
}


return {
    show = show_clientinfo,
    hide = hide_clientinfo,
    toggle = toggle_clientinfo
}
