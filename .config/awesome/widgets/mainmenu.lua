require "configuration.defaults"
local freedesktop = require "external.awesome-freedesktop"
local exit_screen = require "widgets.exit_screen"
local beautiful = require "beautiful"
local helpers = require("helpers")

local awesomemenu = {
    { "hotkeys", function() return false, hotkeys_popup.show_help end},
    { "manual", terminal .. " -e man awesome" },
    { "edit config", editor_cmd .. " " .. awesome.conffile },
    { "restart", awesome.restart },
    { "quit", exit_screen}
}

local mainmenu = freedesktop.menu.build({
        before = { { "awesome", awesomemenu, beautiful.awesome_icon } },
        after = { {"open terminal", terminal} },
})

helpers.click_to_hide.menu(mainmenu)

return mainmenu
