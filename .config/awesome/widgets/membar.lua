local beautiful = require("beautiful")
local gears = require("gears")
local vicious = require("vicious")
local wibox = require("wibox")
local awful = require("awful")


vicious.cache(vicious.widgets.mem)

local memtext = wibox.widget.textbox()
local memprogress = wibox.widget.progressbar()
local membar = wibox.widget {
    {
        color            = beautiful.bg_secondary,
        background_color = beautiful.bg_secondary .. "75",
        margins          = {
            top = 15,
        },
        forced_width     = 150,
        paddings         = 0,
        shape            = gears.shape.rounded_bar,
        widget           = memprogress,
    },
    {
        {
            memtext,
            fg     = beautiful.fg_normal,
            widget = wibox.container.background,
        },
        left   = 5,
        widget = wibox.container.margin,
    },
    layout = wibox.layout.stack
}

local swaptext = wibox.widget.textbox()
local swapprogress = wibox.widget.progressbar()
local swapbar = wibox.widget {
    {
        {
            color            = beautiful.bg_primary,
            background_color = beautiful.bg_primary .. "75",
            margins          = {
                top = 15,
            },
            forced_width     = 100,
            paddings         = 0,
            shape            = gears.shape.rounded_bar,
            widget           = swapprogress,
        },
        reflection = {
            horizontal = true,
            vertical = false,
        },
        widget = wibox.container.mirror,
    },
    {
        {
            {
                align = "right",
                widget = swaptext,
            },
            fg     = beautiful.fg_normal,
            widget = wibox.container.background,
        },
        right  = 5,
        widget = wibox.container.margin,
    },
    layout = wibox.layout.stack
}


vicious.register(memtext, vicious.widgets.mem, "$2 MiB", 5)
vicious.register(memprogress, vicious.widgets.mem, "$1", 5)

vicious.register(swaptext, vicious.widgets.mem, "$6 MiB", 5)
vicious.register(swapprogress, vicious.widgets.mem, "$5", 5)



memswap = wibox.widget {
    swapbar,
    membar,
    layout = wibox.layout.fixed.horizontal,
}




    local tooltip
    local tooltip_text = "..."
    local function refresh_tooltip()
        awful.spawn.easy_async_with_shell('ps -eo pmem,pid,user,args --sort=-pmem | head -n 15 | cut -c -120',
                               function(stdout, _, _, _, _)
                                   tooltip_text = stdout
                                   tooltip.markup = stdout
                                   tooltip.width = 300
                               end
        )
    end
    tooltip = awful.tooltip {
        timer_function = function()
            refresh_tooltip()
            return tooltip_text
        end,
        timeout = 2,
        objects = { memswap },
        mode = "outside",
        preferred_alignments = { "middle", "front", "back" },
        preferred_positions = { "top" },
        margin_topbottom = 14,
        margin_leftright = 20,
    }




return memswap
