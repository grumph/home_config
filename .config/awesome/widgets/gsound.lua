



local lgi = require 'lgi'
-- local GLib = lgi.require('GLib')
local GSound = lgi.require('GSound')

local sound_context = GSound.Context()

local function play(args)
    if args.sound then
        sound_context:play_simple({
                [GSound.ATTR_EVENT_ID] = args.sound,
                [GSound.ATTR_EVENT_DESCRIPTION] = args.description or "awesomewm"
        })
    end
end


return {play = play}
