-------------------------------------------------
-- Battery Arc Widget for Awesome Window Manager
-- Shows the battery level of the laptop
-- Can manage multiple batteries with their own colors
--
-- @author Grumph
--
-- Blithely inspired from:
-- https://github.com/streetturtle/awesome-wm-widgets/tree/master/batteryarc-widget
-------------------------------------------------

local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local naughty = require("naughty")
local vicious = require("vicious")
local wibox = require("wibox")


local widget = {}

local function worker(args)

    args = args or {}

    local font = args.font or beautiful.font or 'Sans 6'
    local arc_thickness = args.arc_thickness or 3
    local show_current_level = args.show_current_level or false
    local refresh_rate = args.refresh_rate or 59
    local critical_level = args.critical_level or 15

    local fg_color = args.fg_color or beautiful.fg_normal or "#00aa00"
    local bg_color = args.bg_color or beautiful.bg_normal or "#000000"

    local popup_msg_title = args.popup_msg_title or 'Battery status'
    local popup_msg_position = args.popup_msg_position or 'bottom_right'

    local warning_msg_title = args.warning_msg_title or 'Battery is dying!'
    local warning_msg_text = args.warning_msg_text or 'Please plug in your computer.'
    local warning_msg_position = args.warning_msg_position or popup_msg_position
    local warning_msg_icon = args.warning_msg_icon
        or gears.filesystem.get_configuration_dir() .. '/widgets/batteryarc/battery_dying.png'

    local batteries = args.batteries or {{name = "BAT0", color = beautiful.fg_color}}

    -- Add default parameters to batteries object
    for i, bat in ipairs(batteries) do
        bat["value"] = 0
        bat["widget"] = wibox.widget.textbox()
        bat["present"] = true
    end

    local charging = false


    -- Widget is in 3 parts: the center text, the background and the arcchart
    local text = wibox.widget {
        font = font,
        align = 'center', -- align the text
        valign = 'center',
        widget = wibox.widget.textbox
    }
    local text_with_background = wibox.container.background(text)
    widget = wibox.widget {
        text_with_background,
        -- rounded_edge = true,
        thickness = arc_thickness,
        paddings = 1,
        start_angle = 4.71238898, -- 2pi*3/4
        bg = gears.color.ensure_pango_color(fg_color .. "22", fg_color), -- try to add transparency
        border_width = 0,
        min_value = 0,
        max_value = 100 * #batteries,
        widget = wibox.container.arcchart
    }

    -- Variable used for low battery check
    local last_battery_check = os.time()

    -- Update the whole widget
    function update_widget()
        local total_bat = 0
        local colors = {}
        local values = {}
        for i, bat in ipairs(batteries) do
            if bat.present then
                total_bat = total_bat + bat.value
                colors[i] = bat.color
                values[i] = bat.value
            end
        end
        total_bat = math.floor(total_bat / #values)

        -- Update the text
        if show_current_level and total_bat ~= 100 then
            --- if battery is fully charged (100) there is not enough place for three digits, so we don't show any text
            text.text = string.format('%d', total_bat)
        else
            text.text = '✅'
        end

        -- Update the background, invert colors when charging
        if charging then
            text_with_background.bg = fg_color
            text_with_background.fg = bg_color
        else
            text_with_background.bg = bg_color
            text_with_background.fg = fg_color
        end

        -- Update the arcchart
        widget.colors = colors
        widget.values = values
        widget.max_value = 100 * #values

        -- In case of low battery, show the warning
        if total_bat < critical_level then
            if not charging and os.difftime(os.time(), last_battery_check) > 300 then
                -- if 5 minutes have elapsed since the last warning
                last_battery_check = os.time()
                show_battery_warning()
            end
        end
    end

    -- Update the values
    for i, bat in ipairs(batteries) do
        vicious.register(bat.widget,
                         vicious.widgets.bat,
                         function(W, A)
                             -- io.stderr:write("battery " .. bat.name .. " " .. gears.debug.dump_return(A) .. "\n")
                             batteries[i].value = A[2]
                             batteries[i].present = A[1] ~= "⌁" -- Linux only
                             charging = A[1] == "+"
                             update_widget()
                             return ("%d"):format(A[2])
                         end,
                         refresh_rate,
                         bat.name)
    end

    -- Show battery status popup
    local notification
    function show_battery_status()
        awful.spawn.easy_async(
            [[bash -c 'acpi']],
            function(stdout, _, _, _)
                -- if notification then
                --    notification:destroy("expired")
                -- end
                notification = naughty.notification {
                    title = popup_msg_title,
                    message = stdout,
                    timeout = 5,
                    hover_timeout = 0.5,
                    width = auto,
                    margin = 7,
                    position = popup_msg_position,
                    ontop = true
                }
        end)
    end

    widget:connect_signal("mouse::enter", function()
                              show_battery_status()
    end)
    widget:connect_signal("mouse::leave", function()
                              if notification then
                                  notification:destroy("dismissed_by_user")
                              end
    end)

    -- Show warning notification
    function show_battery_warning()
        naughty.notification {
            icon = warning_msg_icon,
            icon_size = 200,
            title = warning_msg_title,
            message = warning_msg_text,
            timeout = 25, -- show the warning for a longer time
            hover_timeout = 2,
            position = warning_msg_position,
            width = 400,
            urgency = "critical",
            ontop = true
        }
    end

    return widget

end

return setmetatable(widget, { __call = function(_, ...)
                                  return worker(...)
                   end })
