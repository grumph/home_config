local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local beautiful = require("beautiful")
local helpers = require("helpers")

local function client_row(c)
    local row = wibox.widget {
        {
            {
                {
                    {
                        image = c.icon,
                        forced_width = 16,
                        forced_height = 16,
                        widget = wibox.widget.imagebox
                    },
                    right = 6,
                    widget = wibox.container.margin
                },
                {
                    text = c.name,
                    widget = wibox.widget.textbox
                },
                layout = wibox.layout.fixed.horizontal
            },
            margins = 6,
            widget = wibox.container.margin
        },
        bg = beautiful.bg_normal,
        fg = beautiful.fg_normal,
        widget = wibox.container.background
    }

    row:connect_signal("mouse::leave", function(bg_container)
                           bg_container:set_bg(beautiful.bg_normal)
                           bg_container.fg = beautiful.fg_normal
    end)
    row:connect_signal("mouse::enter", function(bg_container)
                           bg_container:set_bg(beautiful.bg_focus)
                           bg_container.fg = beautiful.fg_focus
    end)

    row:buttons(
        awful.button({}, 1, function()
                if not c.valid then return end
                if not c:isvisible() then
                    awful.tag.viewmore(c:tags(), c.screen)
                end
                c:emit_signal("request::activate", "menu.clients", {raise=true})
        end)
    )

    return row
end

local function tag_row(t)
    local label = wibox.widget {
        {
            {
                {
                    image = t.icon,
                    forced_height = 20,
                    widget = wibox.widget.imagebox
                },
                {
                    text = t.name,
                    align = "center",
                    widget = wibox.widget.textbox
                },

                layout = wibox.layout.align.horizontal,
            },
            halign = "center",
            widget = wibox.container.place,
        },
        bg = beautiful.bg_minimize,
        fg = beautiful.fg_minimize,
        shape = gears.shape.hexagon,
        widget = wibox.container.background
    }
    local row = wibox.widget {
        {
            label,
            left = beautiful.menu_height,
            right = beautiful.menu_height,
            widget = wibox.container.margin
        },
        bg = beautiful.bg_normal,
        widget = wibox.container.background
    }

    row:connect_signal("mouse::leave", function()
                           label:set_bg(beautiful.bg_minimize)
                           label.fg = beautiful.fg_minimize
                           row:set_bg(beautiful.bg_normal)
    end)
    row:connect_signal("mouse::enter", function()
                           label:set_bg(beautiful.bg_focus)
                           label.fg = beautiful.fg_focus
                           row:set_bg(beautiful.bg_minimize)
    end)


    row:buttons(awful.button({}, 1, function() t:view_only() end))
    return row
end

local function screen_row(s)
    local row = wibox.widget {
        {
            text = "[ SCREEN " .. tostring(s.index) .. " ]",
            align = "center",
            widget = wibox.widget.textbox
        },
        layout = wibox.layout.flex.horizontal,
    }
    return row
end

local function empty_row()
    local row = wibox.widget {
        text = "",
        forced_height = 10,
        widget = wibox.widget.textbox
    }
    return row
end

local function build_rows()
    local rows = { layout = wibox.layout.fixed.vertical }
    for s in screen do
        if screen.count() > 1 then
            table.insert(rows, screen_row(s))
        end
        for _, t in ipairs(s.tags) do
            table.insert(rows, tag_row(t))
            if #(t:clients()) > 0 then
                for _, c in ipairs(helpers.sort.clients(t:clients())) do
                    table.insert(rows, client_row(c))
                end
            else
                table.insert(rows, empty_row())
            end
        end
    end
    return rows
end

local clientsmenu_widget = wibox.widget {
    {
        image = awful.util.get_configuration_dir() .. "icons/application-cog-outline.svg",
        widget = wibox.widget.imagebox
    },
    margins = 3,
    layout = wibox.container.margin
}

local popup = helpers.widget_popup {
    content_function = build_rows,
    maximum_width = 400,
    hide_on_left_click = true,
}

clientsmenu_widget:add_button(awful.button({}, 1, function() popup:toggle() end))


return clientsmenu_widget
