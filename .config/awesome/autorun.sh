#!/usr/bin/env bash

function run {
    echo -n "$1 "
    if ! pgrep -f $1 ; then
        echo "starting"
        $@&
    else
        echo "is already running"
    fi
}

run cmst -w5
run lxqt-policykit-agent
run lxqt-powermanagement
run udiskie --tray --no-automount
# run picom -b   # "May cause a display freeze" ???
run picom
run copyq
run redshift-gtk
# run mplugd
# run pasystray
# run volctl
run /usr/lib/kdeconnectd
run kdeconnect-indicator
run xscreensaver -nosplash
