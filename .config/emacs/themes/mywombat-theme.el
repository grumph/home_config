;;; Package --- My wombat Theme

;;; Commentary:

;;; Code:

(deftheme mywombat
  "Created 2017-05-30.")

(custom-theme-set-variables
 'mywombat
 '(ansi-color-names-vector ["black" "#fd3355" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"]))

(custom-theme-set-faces
 'mywombat
 '(mode-line ((t (:background "#2121aa" :foreground "#f6f3e8"))))
 '(mode-line-inactive ((t (:background "#00005f" :foreground "#857b6f"))))
 '(mode-line-highlight ((t (:box nil))))
 '(cursor ((t (:background "#a9a9a9"))))
 '(fringe ((((class color) (min-colors 89)) (:background "#303030"))))
 '(highlight ((t (:background "#004000"))))
 '(hl-line ((t (:background "#1c1c1c"))))
 '(region ((t (:background "#00af5f" :foreground "#f6f3e8"))))
 '(secondary-selection ((((class color) (min-colors 89)) (:background "#333366" :foreground "#f6f3e8"))))
 '(isearch ((((class color) (min-colors 89)) (:background "#343434" :foreground "#857b6f"))))
 '(lazy-highlight ((((class color) (min-colors 89)) (:background "#384048" :foreground "#a0a8b0"))))
 '(minibuffer-prompt ((((class color) (min-colors 89)) (:foreground "#e5786d"))))
 '(escape-glyph ((((class color) (min-colors 89)) (:foreground "#ddaa6f" :weight bold))))
 '(font-lock-builtin-face ((((class color) (min-colors 89)) (:foreground "#ee5757"))))
 '(font-lock-comment-face ((((class color) (min-colors 89)) (:foreground "#99968b"))))
 '(font-lock-constant-face ((((class color) (min-colors 89)) (:foreground "#d5d57b"))))
 '(font-lock-function-name-face ((((class color) (min-colors 89)) (:foreground "#7373ff"))))
 '(font-lock-keyword-face ((((class color) (min-colors 89)) (:foreground "#0f7290" :weight bold))))
 '(font-lock-string-face ((((class color) (min-colors 89)) (:foreground "#1ab31a"))))
 '(font-lock-type-face ((((class color) (min-colors 89)) (:foreground "#92a65e" :weight bold))))
 '(font-lock-variable-name-face ((((class color) (min-colors 89)) (:foreground "#edff67"))))
 '(font-lock-warning-face ((((class color) (min-colors 89)) (:foreground "brightred"))))
 '(link ((((class color) (min-colors 89)) (:foreground "#8ac6f2" :underline t))))
 '(link-visited ((((class color) (min-colors 89)) (:foreground "#e5786d" :underline t))))
 '(button ((((class color) (min-colors 89)) (:background "#333333" :foreground "#f6f3e8"))))
 '(custom-button ((t (:background "lightgrey" :foreground "black" :box (2 . 2)))))
 '(header-line ((((class color) (min-colors 89)) (:background "#303030" :foreground "#e7f6da"))))
 '(show-paren-match ((t (:background "#002f00"))))
 '(whitespace-tab ((t (:background "#262626"))))

 '(ediff-current-diff-A ((t (:background "#442222"))))
 '(ediff-current-diff-B ((t (:background "#224422"))))
 '(ediff-current-diff-C ((t (:background "#222244"))))
 '(ediff-even-diff-A ((t (:background "#262626"))))
 '(ediff-even-diff-B ((t (:background "#3a3a3a"))))
 '(ediff-even-diff-C ((t (:background "#262626"))))
 '(ediff-even-diff-Ancestor ((t (:background "#3a3a3a"))))
 '(ediff-fine-diff-A ((t (:background "#aa2222" :foreground "#000000"))))
 '(ediff-fine-diff-B ((t (:background "#22aa22" :foreground "#000000"))))
 '(ediff-fine-diff-C ((t (:background "#aaaa22" :foreground "#000000"))))
 '(ediff-odd-diff-A ((t (:background "#3a3a3a"))))
 '(ediff-odd-diff-B ((t (:background "#262626"))))
 '(ediff-odd-diff-C ((t (:background "#3a3a3a"))))
 '(ediff-odd-diff-Ancestor ((t (:background "#444444"))))

 '(diff-added ((t (:foreground unspecified :background unspecified :inherit ediff-current-diff-B))))
 '(diff-removed ((t (:foreground unspecified :background unspecified :inherit ediff-current-diff-A))))

 '(mmm-default-submode-face ((t nil)))

 '(default ((t (:background "black" :foreground "#f6f3e8"))))

 '(line-number ((t (:inherit (shadow default) :foreground "#444444"))))
 '(line-number-current-line ((t (:inherit line-number :foreground "#bcbcbc"))))

 '(company-scrollbar-bg ((t (:background "#262650"))))
 '(company-scrollbar-fg ((t (:background "#857b6f"))))
 '(company-template-field ((t (:background "#111187"))))
 '(company-tooltip ((t (:background "#3333bb"))))
 '(company-tooltip-selection ((t (:background "#11115f"))))

 '(magit-section-heading ((t (:foreground unspecified :background unspecified :inherit font-lock-variable-name-face))))
 '(magit-section-highlight ((t (:background "#272727"))))
 '(magit-diff-context-highlight ((t (:background "#303030"))))
 '(magit-branch-local ((t (:foreground "#7373ff"))))
 '(magit-branch-remote ((t (:foreground "#ee5757"))))
 '(magit-branch-current ((t (:background "#7373ff" :foreground "#000000"))))
 '(magit-branch-remote-head ((t (:background "#ee5757" :foreground "#000000"))))

 )


;; Change the modeline's colour to red when the kmacro function
;; is engaged, like an old-school "record" button and back off again when it's
;; stopped.
;; (defface kmacro-modeline-face '((t :background "Firebrick"
;;                               :box `(:line-width -1 :color "salmon" :style released-button)))
;;   "Face for modeline when recording a macro.")
;; (defvar kmacro-modeline-cookie)
;; (defun adv-kmacro-change-modeline (arg)
;;   "Remap the mode-line face with our custom face."
;;   ;; (add-to-list 'face-remapping-alist '(mode-line . kmacro-modeline))
;;   (setq kmacro-modeline-cookie (face-remap-add-relative 'mode-line 'kmacro-modeline-face)))
;; (defun adv-kmacro-restore-modeline (&optional arg)
;;   "Restore the mode-line face."
;;   ;; (setf face-remapping-alist
;;   ;;       (assoc-delete-all 'mode-line face-remapping-alist)))
;;   (face-remap-remove-relative kmacro-modeline-cookie))
;; ;; (defadvice kmacro-start-macro (before kmacro-hl-modeline activate)
;; ;;   "Change modeline color when macro start."
;; ;;   (ad-kmacro-change-modebar))
;; ;; (defadvice kmacro-keyboard-quit (before kmacro-rem-hl-modeline activate)
;; ;;   "Restore modeline color when macro quit."
;; ;;   (ad-kmacro-restore-modebar))
;; ;; (defadvice kmacro-end-macro (before kmacro-rem-hl-modeline activate)
;; ;;   "Restore modeline color when macro end."
;; ;;   (ad-kmacro-restore-modebar))
;; (advice-add 'kmacro-start-macro :before #'adv-kmacro-change-modeline)
;; (advice-add 'kmacro-keyboard-quit :after #'adv-kmacro-restore-modeline)
;; (advice-add 'kmacro-end-macro :after #'adv-kmacro-restore-modeline)


(defface kmacro-modeline '((t :background "#b22222"
                              :box (:line-width (-1 . -1) :color "#fa8072" :style released-button)))
  "Face when kmacro is active")
(defun ad-kmacro-change-modebar ()
  "Remap the mode-line face with our custom face"
  (add-to-list 'face-remapping-alist '(mode-line . kmacro-modeline)))

(defun ad-kmacro-restore-modebar ()
  "Restore the mode-line face"
  (setf face-remapping-alist
        (assoc-delete-all 'mode-line face-remapping-alist)))

(defadvice kmacro-start-macro (before kmacro-hl-modeline activate)
  "Alters `kmacro-start-macro' so it highlights the modeline when
  recording begins."
  (ad-kmacro-change-modebar))

(defadvice kmacro-keyboard-quit (before kmacro-rem-hl-modeline activate)
  "Alters `kmacro-keyboard-quit' so it highlights the modeline when
  recording begins."
  (ad-kmacro-restore-modebar))

(defadvice kmacro-end-macro (before kmacro-rem-hl-modeline activate)
  "Alters `kmacro-end-macro' so it highlights the modeline when
  recording begins."
  (ad-kmacro-restore-modebar))


(provide-theme 'mywombat)
;;; mywombat-theme.el ends here
