;;; .emacs --- My emacs configuration

;; ----------------------------------------------------------------------------
;; "THE BEER-WARE LICENSE" (Revision 42):
;; <grumph@capcat.net>  wrote this file.  As long as you retain this notice you
;; can do whatever you want with this stuff. If we meet some day, and you think
;; this stuff is worth it, you can buy me a beer in return.              Grumph
;; ----------------------------------------------------------------------------


;;; Commentary:
;; No comment.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Make Emacs start faster
;;  - https://github.com/farlado/dotemacs/tree/aesthetic#file-name-handling-setup
;;  - https://tony-zorman.com/posts/2022-10-22-emacs-potpourri.html
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq load-prefer-newer t)

;; Garbage collection shouldn’t happen during startup
(setq gc-cons-threshold  most-positive-fixnum
      gc-cons-percentage 1)

;; Don't resize the frame to preserve the number of columns or lines
;; being displayed when setting font, menu bar, tool bar, tab bar,
;; internal borders, fringes, or scroll bars.  On tiling managers, this
;; option is i) useless and ii) _terribly_ expensive.
(setq frame-inhibit-implied-resize t)

;; For whatever reason, setting file-name-handler-alist to nil during startup helps Emacs load faster.
(defvar startup/file-name-handler-alist file-name-handler-alist
  "Temporary storage for `file-name-handler-alist' during startup.")

(defun startup/revert-file-name-handler-alist ()
  "Revert `file-name-handler-alist' to its default value after startup."
  (setq file-name-handler-alist startup/file-name-handler-alist))

(setq file-name-handler-alist nil)

(add-hook 'after-init-hook 'startup/revert-file-name-handler-alist)


(defun startup/reset-gc ()
  "Return garbage collection to normal parameters after startup."
  (setq gc-cons-threshold (* 2 1024 1024)
        gc-cons-percentage 0.1))

(add-hook 'after-init-hook 'startup/reset-gc)


;; Increase data read from processes
(setq read-process-output-max (* 1024 1024)) ;; 1mb

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Init package manager
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq package-enable-at-startup nil)
(require 'package)
(package-initialize)
(add-to-list 'package-archives '("melpa"        . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("gnu"          . "https://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("nongnu"       . "https://elpa.nongnu.org/nongnu/"))

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; Benchmark tools
;; ;; https://blog.d46.us/advanced-emacs-startup/

;; (require 'benchmark-init)
;; ;; ;; To disable collection of benchmark data after init is done.
;; (add-hook 'after-init-hook 'benchmark-init/deactivate)

;; (add-hook 'emacs-startup-hook
;;           (lambda ()
;; (message "Emacs ready in %s with %d garbage collections."
;;          (format "%.2f seconds"
;;                  (float-time
;;                   (time-subtract after-init-time before-init-time)))
;;          gcs-done)
;; ))

;; ;; Must be after package init
;; (require 'esup)


;; Init 'use-package' (MUST BE 2ND)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile (require 'use-package))

(defvar use-package-always-ensure)
(setq use-package-always-ensure t)
(defvar use-package-always-pin)
(setq use-package-always-pin "melpa")

;;; Code:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Display & ergonomics options
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Use a nice font
(set-frame-font "MonacoB2 Nerd Font Mono" nil t)

;; Set the proper title
(setq frame-title-format '(buffer-file-name "Emacs: %b (%f)" "Emacs: %b"))

;; Pixel-wise stuff
(setq frame-resize-pixelwise t)
(setq window-resize-pixelwise t)
(when (>= emacs-major-version 26)
  (pixel-scroll-mode))

;; One line is one line, do not cut long lines to show all
(setq-default truncate-lines t)
(setq-default word-wrap nil)

;; Remove startup message
(setq inhibit-startup-screen t)

;; Smooth scroll
(setq scroll-step 1) ;; keyboard scroll one line at a time
(setq scroll-margin 2) ;; keep x lines of border
(setq scroll-conservatively 15) ;; if cursor goes out of the screen less than x lines, do not recenter on it, just continue scrolling
(setq hscroll-step 1)
(setq hscroll-margin 15)
(setq scroll-preserve-screen-position t)

;; replace yes-or-no with y-or-n
(setq use-short-answers t)  ;; From emacs 28.1
(defalias 'yes-or-no-p 'y-or-n-p )



;; defer fontification while input pending
(setq jit-lock-defer-time 0.1)

;; Better performance with files with long lines
(global-so-long-mode 1)



;; Update modeline with leading icons

(defvar-local my-mode-line-info ""
  "Buffer-local variable to store mode line information.")

(defun my-update-mode-line-info ()
  "Update the mode line information for the current buffer."
  (let ((my-string (concat
                    ;; Read only buffer
                    (if buffer-read-only "🔒" "")
                    ;; Modified buffer
                    (if (and (buffer-modified-p) (not (string-prefix-p "*" (buffer-name)))) "💾" "") ;; "🗸"
                    ;; Remote buffer
                    (if (file-remote-p default-directory) "@" "")
                    )))
    (setq my-mode-line-info my-string)))

(defun my-mode-line-format ()
  "Return the formatted mode line string."
  (concat
   (if (display-graphic-p) " " "")
   my-mode-line-info))

(setq inhibit-modification-hooks nil)
(add-hook 'post-command-hook #'my-update-mode-line-info)

(setq-default mode-line-front-space " ")
(setq-default mode-line-mule-info "")
(setq-default mode-line-client "")
(setq-default mode-line-modified "")
(setq-default mode-line-remote "")
(setq-default mode-line-frame-identification "")

(setq-default mode-line-format
              (cons
               '(:eval (my-mode-line-format))
               (cdr mode-line-format)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Editor options
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Do not show buffer list at startup
(setq inhibit-startup-buffer-menu t)
;; Use an empty generic scratch
(setq initial-major-mode (quote fundamental-mode))
(setq initial-scratch-message nil)

(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)
(define-coding-system-alias 'UTF-8 'utf-8)

;; no lockfiles
(setq create-lockfiles nil)


;; Indenting
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)

;; TODO useful ?
(setq-default c-basic-offset 4)
(defvar c-default-style) ;; avoid warning
(setq c-default-style "bsd")

;; Do not allow UTF that hide BOM
(setq auto-coding-regexp-alist
      (delete (rassoc 'utf-16be-with-signature auto-coding-regexp-alist)
              (delete (rassoc 'utf-16le-with-signature auto-coding-regexp-alist)
                      (delete (rassoc 'utf-8-with-signature auto-coding-regexp-alist)
                              auto-coding-regexp-alist))))

;; Try to open with sudo if the file is not writable
(defadvice save-buffer (around save-buffer-as-root-around activate)
  "Use sudo to save the current buffer."
  (interactive "p")
  (if (and (buffer-file-name) (not (file-writable-p (buffer-file-name))))
      (let ((buffer-file-name (format "/sudo::%s" buffer-file-name)))
        ad-do-it)
    ad-do-it))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Custom functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Better C-k handeling
(defun kill-and-join-forward (&optional arg)
  "If at end of line, remove newline and join with the following line.
Otherwise kill current line.  Whitespaces are ignored.
Argument ARG: see \"kill-line\" doc."
  (interactive "P")
  (if (and (eolp) (not (bolp)))
      (progn (forward-char 1)
             (just-one-space 0)
             (backward-char 1)
             (kill-line arg))
    (kill-line arg)))


(defun my/forward-word (&optional arg)
  "Go to closest point: \"end-of-line\", \"forward-word\" or \"forward-to-word\".
Check these functions for ARG value."
  (interactive)
  (setq arg (or arg 1))
  (let ((p-to (save-excursion
                (forward-to-word arg)
                (point)))
        (p (save-excursion
             (forward-word arg)
             (point)))
        (p-end (save-excursion
                 (end-of-line)
                 (point)))
        )
    (if (and (< p-end p) (< p-end p-to) (/= p-end (point)))
        (goto-char p-end)               ;; Move to end of line after last word
      (if (or
           (and (< p-to p)  (< (+ (point) 1) p-to))
           (>= (+ (point) 2) p))
          (goto-char p-to)
        (goto-char p)))))

(defun my/backward-word (&optional arg)
  "Go to closest point following different methods.
Functions are\"back-to-indentation\", \"backward-word\" or \"backward-to-word\".
Check these functions for ARG value."
  (interactive)
  (setq arg (or arg 1))
  (let ((p-to (save-excursion
                (backward-to-word arg)
                (point)))
        (p (save-excursion
             (backward-word arg)
             (point)))
        (p-start (save-excursion
             (back-to-indentation)
             (point)))
        )
    (if (and (> p-start p) (> p-start p-to) (/= p-start (point)))
        (goto-char p-start)             ;; Move to indentation before first word
      (if (or (and (> p-to p)  (> (- (point) 1) p-to))
              (<= (- (point) 2) p))
          (goto-char p-to)
        (goto-char p)))))

(defun my/forward-kill-word (&optional arg)
  "Kill the next word with \"my/forward-word\".
Check this function for ARG value."
  (interactive)
  (setq arg (or arg 1))
  (let ((p (save-excursion
            (my/forward-word arg)
            (point))))
    (delete-region (point) (goto-char p))))

(defun my/backward-kill-word (&optional arg)
  "Kill the previous word with \"my/backward-word\".
Check this function for ARG value."
  (interactive)
  (setq arg (or arg 1))
  (let ((p (save-excursion
            (my/backward-word arg)
            (point))))
    (delete-region (point) (goto-char p))))



(defun forward-char-same-line (&optional arg)
  "Move forward a max of ARG chars on the same line, or backward if ARG < 0.
Return the signed number of chars moved if /= ARG, else return nil."
  (interactive "p")
  (let* ((start                      (point))
         (fwd-p                      (natnump arg))
         (inhibit-field-text-motion  t) ; Just to be sure, for end-of-line.
         (max                        (save-excursion
                                       (if fwd-p (end-of-line) (beginning-of-line))
                                       (- (point) start))))
    (forward-char (if fwd-p (min max arg) (max max arg)))
    (and (< (abs max) (abs arg))
         max)))

(defun backward-char-same-line (&optional arg)
  "Move backward a max of ARG chars on the same line, or forward if ARG < 0.
Return the signed number of chars moved if /= ARG, else return nil."
  (interactive "p")
  (forward-char-same-line (- 0 arg)))


(defun clean-exit ()
  "Exit Emacs cleanly.
If there are unsaved buffer, pop up a list for them to be saved
before exiting.  Replaces ‘save-buffers-kill-terminal’."
  (interactive)
  (if (frame-parameter nil 'client)
      (server-save-buffers-kill-terminal)
    (if-let ((buf-list (seq-filter (lambda (buf)
                                     (and (buffer-modified-p buf)
                                          (buffer-file-name buf)))
                                   (buffer-list))))
        (progn
          (pop-to-buffer (list-buffers-noselect t buf-list))
          (message "s to save, C-k to kill, x to execute"))
      (save-buffers-kill-emacs))))


;; Select the window you create
(defun split-follow (&rest _arg)
  "Advice to follow a function which spawn a window."
  (other-window 1))

(advice-add 'split-window-below :after #'split-follow)
(advice-add 'split-window-right :after #'split-follow)

;; Discard all themes before loading new.
(define-advice load-theme (:before (&rest _args) theme-dont-propagate)
  "Discard all themes before loading new."
  (mapc #'disable-theme custom-enabled-themes))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Packages
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;
;; Use package related

(use-package use-package-chords
  :config (key-chord-mode 1))

;; (use-package use-package-ensure-system-package)

(use-package diminish)

(use-package package
  :ensure nil
  :custom
  (package-install-upgrade-built-in t))

;;;;;;;;;;;;;;;;;;;;;;
;; Emacs behaviour packages

(use-package auth-source
  :ensure nil
  :custom
  ;; Never ask to save credentials
  (auth-source-save-behavior nil))

;; Change default directories
(use-package no-littering
  :config
  (setq custom-file (no-littering-expand-etc-file-name "custom.el")))

;; Re-read file when changed on disk
(use-package autorevert
  :ensure nil
  :diminish auto-revert-mode
  :hook (after-init . global-auto-revert-mode))

(use-package files
  :ensure nil
  :custom
  (backup-by-copying t)
  (delete-old-versions t)
  (kept-new-versions 6)
  (kept-old-versions 5)
  (version-control t)
  (auto-save-file-name-transforms '((".*" "~/.config/emacs/var/autosaves/\\1" t)))
  (backup-directory-alist '((".*" . "~/.config/emacs/var/autosaves/\\1")))
  (view-read-only t)  ;; enable view-mode for read-only files
  :init
  (make-directory "~/.config/emacs/var/autosaves/" t)
  )

;; CONFLICTS WITH AUTO SUDO SAVES, and from time to time left a process running at quit
;; (use-package async-backup
;;   :hook (after-save . async-backup))

(use-package compile
  :ensure nil
  :defer t
  :custom
  (compilation-scroll-output 'first-error))

;; Manage clipboard with xclip or wl-clipboard
(if (string= (getenv "XDG_SESSION_TYPE") "x11")
    (if (file-exists-p "/usr/bin/xclip")
        (use-package xclip
          :defer 1
          :pin "gnu"
          :config (xclip-mode 1))
      (message "xclip binary not found, skipping")
      )
  (if (string= (getenv "XDG_SESSION_TYPE") "wayland")
      (if (file-exists-p "/usr/bin/wl-copy")
          (progn
            ;; https://www.emacswiki.org/emacs/CopyAndPaste#h5o-4
            ;; credit: yorickvP on Github
            (setq wl-copy-process nil)
            (defun wl-copy (text)
              (setq wl-copy-process (make-process :name "wl-copy"
                                                  :buffer nil
                                                  :command '("wl-copy" "-f" "-n")
                                                  :connection-type 'pipe
                                                  :noquery t))
              (process-send-string wl-copy-process text)
              (process-send-eof wl-copy-process))
            (defun wl-paste ()
              (if (and wl-copy-process (process-live-p wl-copy-process))
                  nil ; should return nil if we're the current paste owner
                (shell-command-to-string "wl-paste -n | tr -d \r")))
            (setq interprogram-cut-function 'wl-copy)
            (setq interprogram-paste-function 'wl-paste)
            )
        (message "wl-copy binary not found, skipping")
        )
    (message "Not in x11 or Wayland, no clipboard syncing")
  )
)

(use-package recentf
  :ensure nil
  :hook (after-init . recentf-mode)
  :config
  (add-to-list 'recentf-exclude "\\.gpg\\")
  (add-to-list 'recentf-exclude "\\.credentials\\")
  (recentf-mode 1))

;;;;;;;;;;;;;;;;;;;;;;;;
;; GUI/TUI packages

(use-package scroll-bar
  :ensure nil
  :custom (scroll-bar-mode nil))

(setq tool-bar-mode nil)

(use-package simple
  :ensure nil
  :bind (("M-SPC" . my-cycle-spacing)
         ([remap set-selective-display] . toggle-selective-display)
         ("C-c w" . count-words)
         ("M-§" . shell-command))

  :config
  ;; Column number at the bottom
  (column-number-mode 1)
  ;; (line-number-mode 1)
  ;; highlight selection
  (transient-mark-mode 1)
  :init
  (defun my-cycle-spacing ()
    "Cycle spacing, one space, also remove newlines after point."
    (interactive)
    (cycle-spacing -1))
  (defun toggle-selective-display (column)
    (interactive "P")
    (set-selective-display
     (or column
         (unless selective-display
           (1+ (current-column))))))
  )

(use-package delsel
  :defer 1
  :ensure nil
  :config
  ;; delete seleted text when typing
  (delete-selection-mode 1))

(use-package window
  :ensure nil
  :custom
  ;; If there is not enough lines to scroll, just go to the first/last line.
  (scroll-error-top-bottom t)
  )

(use-package menu-bar
  :ensure nil
  :config (menu-bar-mode -1)
  :bind ([S-f10] . menu-bar-mode))

(use-package import-popwin
  :bind ("C-c i" . import-popwin)
  :chords ("ii" . import-popwin)
  :config (popwin-mode 1))


(use-package mlscroll
  :ensure t
  :config
  ;; (setq mlscroll-shortfun-min-width 11) ;truncate which-func, for default mode-line-format's
  (mlscroll-mode 1))


;; undo-tree mode
(use-package undo-tree
  :pin "gnu"
  :diminish
  :bind (("C-x u" . undo-tree-visualize)
         ("M-_" . undo-tree-redo)
         ("M-n" . go-back-to-last-edit))
  :custom
  (undo-tree-visualizer-diff t)
  (undo-tree-enable-undo-in-region t)
  ;; (undo-tree-auto-save-history nil)
  (undo-tree-history-directory-alist '((".*" . "~/.config/emacs/var/autosaves/\\1")))
  ;; (setq undo-tree-visualizer-relative-timestamps nil)
  ;; (setq undo-tree-visualizer-timestamps t)
  :config
  (declare-function undo-tree-undo "undo")
  (defun go-back-to-last-edit ()
    "Jump back to the last change in the current buffer."
    (interactive)
    (ignore-errors
      (let ((inhibit-message t))
        (undo-tree-undo)
        (undo-tree-redo))))
  :init
  (global-undo-tree-mode))


;; zoom window like C-b z in tmux
(use-package zoom-window
  :bind ("C-x C-z" . zoom-window-zoom))

(use-package corfu
  :custom
  ;; (corfu-cycle t)                ;; Enable cycling for `corfu-next/previous'
  (corfu-auto t)                 ;; Enable auto completion
  ;; (corfu-separator ?\s)          ;; Orderless field separator
  ;; (corfu-quit-at-boundary nil)   ;; Never quit at completion boundary
  ;; (corfu-quit-no-match nil)      ;; Never quit, even if there is no match
  ;; (corfu-preview-current nil)    ;; Disable current candidate preview
  ;; (corfu-preselect 'prompt)      ;; Preselect the prompt
  ;; (corfu-on-exact-match nil)     ;; Configure handling of exact matches
  (corfu-scroll-margin 1)        ;; Use scroll margin
  :hook ((prog-mode . corfu-mode)
         (shell-mode . corfu-mode)
         (eshell-mode . corfu-mode))
  :init
  (global-corfu-mode)
  )

(use-package corfu-terminal
  :pin "nongnu"
  :after corfu
  :init
  (unless (display-graphic-p)
    (corfu-terminal-mode +1)))

(use-package emacs
  :defines text-mode-ispell-word-completion
  :init
  ;; Enable indentation+completion using the TAB key.
  ;; `completion-at-point' is often bound to M-TAB.
  (setq tab-always-indent 'complete)

  ;; Emacs 30 and newer: Disable Ispell completion function. As an alternative,
  ;; try `cape-dict'.
  (setq text-mode-ispell-word-completion nil)

  ;; Emacs 28 and newer: Hide commands in M-x which do not apply to the current
  ;; mode.  Corfu commands are hidden, since they are not used via M-x. This
  ;; setting is useful beyond Corfu.
  (setq read-extended-command-predicate #'command-completion-default-include-p))

(use-package cape
  :init
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-keyword)
  (advice-add 'eglot-completion-at-point :around #'cape-wrap-buster))

(use-package nerd-icons-corfu
  :after corfu
  :config
  (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))

(use-package nerd-icons-completion
  :after marginalia
  :config
  (nerd-icons-completion-mode)
  (add-hook 'marginalia-mode-hook #'nerd-icons-completion-marginalia-setup))


(use-package magit-file-icons
  :after magit
  :init
  (magit-file-icons-mode 1)
  :custom
  ;; These are the default values:
  (magit-file-icons-enable-diff-file-section-icons t)
  (magit-file-icons-enable-untracked-icons t)
  (magit-file-icons-enable-diffstat-icons t))


(use-package display-line-numbers
  :ensure nil
  :bind ([f3] . display-line-numbers-mode))

;; add line and columns markers
(defun set-markers ()
  "Add lines and column markers."
  (interactive)
  (if (null ruler-mode)
      (ruler-mode 1)
    (ruler-mode 0)
    )
  (if (null display-line-numbers-mode)
      (display-line-numbers-mode 1)
    (display-line-numbers-mode 0)
    )
  )


(use-package which-key
  :diminish
  :hook (after-init . which-key-mode)
  :custom
  (which-key-paging-prefixes nil)
  (which-key-show-docstrings nil)
  (which-key-side-window-location 'bottom)
  (which-key-sort-order 'which-key-description-order)
  :config (which-key-mode 1))

(use-package helpful
  :defer t
  :config
  (advice-add 'describe-function :override #'helpful-function)
  (advice-add 'describe-variable :override #'helpful-variable)
  (advice-add 'describe-command  :override #'helpful-callable)
  (advice-add 'describe-key      :override #'helpful-key)
  (advice-add 'describe-symbol   :override #'helpful-symbol))

;; visible indicator for current line
(use-package pulse
  :ensure nil
  :commands (pulse-momentary-highlight-one-line)
  :functions pulse-line
  :init
  (defun pulse-line (&rest _)
    "Pulse the current line."
    (pulse-momentary-highlight-one-line (point)))
  (dolist (command '(scroll-up-command
                     scroll-down-command
                     recenter-top-bottom
                     other-window
                     windmove-down
                     windmove-up
                     windmove-left
                     windmove-right))
    (advice-add command :after #'pulse-line)))

(defun find-awesome-rc-file ()
  "Edit the `user-init-file', in another window."
  (interactive)
  (find-file "~/.config/awesome/rc.lua"))


(use-package crux
  :defer 1
  :bind (([f7]      . delete-trailing-whitespace)
         ("M-:"     . comment-or-uncomment-region)
         ("C-c o"   . crux-open-with)
         ("C-x 4 t" . crux-transpose-windows)
         ;; ("C-c d"  . crux-duplicate-current-line-or-region)
         ("C-c d"  . crux-duplicate-and-comment-current-line-or-region)
         ("C-c k"   . crux-kill-other-buffers)
         ("C-c t"   . indent-region)
         ("C-c e i" . crux-find-user-init-file)
         ("C-c e s" . crux-find-shell-init-file)
         ("C-c e c" . crux-find-user-custom-file)
         ("C-c e a" . find-awesome-rc-file) ;; open awesome config
         )
  :config
  (crux-with-region-or-line comment-or-uncomment-region)
  (crux-with-region-or-line kill-region)
  (crux-with-region-or-line kill-ring-save)
  (crux-with-region-or-buffer indent-region)
  (crux-with-region-or-buffer delete-trailing-whitespace)
  )

(use-package mosey
  :bind (
         ([remap move-beginning-of-line] . mosey-begin-backward-cycle) ; Cycle between begning of line or indentation
         ([remap move-end-of-line] . mosey-end-forward-cycle)          ; Cycle between end of line or begining of indentation
         )
  :config
  (defmosey '(beginning-of-line
              back-to-indentation)
    :prefix "begin")
  (defmosey '(end-of-line
              mosey-goto-end-of-code
              ;; mosey-goto-beginning-of-comment-text
              )
    :prefix "end")
  )


(use-package saveplace
  :unless noninteractive
  :hook (after-init . save-place-mode))

;; Save history
(use-package savehist
  :config
  (setq savehist-additional-variables '(ivy-views
                                        search-ring
                                        regexp-search-ring
                                        file-name-history))
  (savehist-mode 1))

;; Better buffer names
(use-package uniquify
  :ensure nil
  :custom (uniquify-buffer-name-style 'forward))

(use-package tramp
  :defer t
  :custom
  (tramp-default-method "ssh")
  ;; Tips to speed up connections
  (tramp-verbose 0)
  (tramp-chunksize 2000)
  (tramp-use-ssh-controlmaster-options nil)
  ;; from jwiegley config: Without this change, tramp ends up sending hundreds of
  ;; shell commands to the remote side to ask what the temporary directory is.
  :config
  (put 'temporary-file-directory 'standard-value '("/tmp"))
  (add-to-list 'tramp-connection-properties
               (list (regexp-quote "/ssh:YOUR_HOSTNAME:")
                     "direct-async-process" t))
  )

(use-package frame
  :ensure nil
  :bind ("C-z z" . suspend-frame)
  :init (unbind-key "C-z")
  )

(use-package fullframe
  :defer t
  :config
  (fullframe magit-status magit-mode-quit-window)
  (fullframe list-packages quit-window))
;; TODO: (fullframe ztree-diff ))

(use-package rotate
  :defer t
  :bind (([f6] . rotate-layout)))

(use-package windmove
  :defer 1
  :ensure nil
  :config
  ;; Move between window panes with Alt + arrow
  (windmove-default-keybindings 'meta)
  (windmove-swap-states-default-keybindings '(shift meta)))

(use-package mouse
  :defer 1
  :ensure nil
  :custom
  ;; GUI only
  ;; middle click paste at point like in console, not at mouse point
  (mouse-yank-at-point t)
  :config
  (global-set-key (kbd "<mouse-4>") 'previous-line)
  (global-set-key (kbd "<mouse-5>") 'next-line)
  (global-set-key (kbd "<mouse-6>") 'backward-char-same-line)
  (global-set-key (kbd "<mouse-7>") 'forward-char-same-line)
  )

;; A more interactive isearch and query-replace
(use-package anzu
  :defer 3
  :diminish
  :custom (anzu-search-threshold 333)
  :bind (([remap query-replace]        . anzu-query-replace)
         ([remap query-replace-regexp] . anzu-query-replace-regexp)
         ([remap isearch-query-replace]        . anzu-isearch-query-replace)
         ([remap isearch-query-replace-regexp] . anzu-isearch-query-replace-regexp))
  :config
  (global-anzu-mode +1)
  )


;;;;;;;;;;;;;;;;;
;; Editor packages

;; Hilight current line (you should also customize 'highlight face)
(use-package hl-line
  :defer 1
  :ensure nil
  :config
  (global-hl-line-mode 1))

(use-package paren
  :defer 3
  :ensure nil
  :custom (show-paren-style 'expression)
  :config
  ;; highlight parenthesis
  (show-paren-mode 1))



;; Move through CamelCase words
(use-package subword
  :defer 3
  :ensure nil
  :diminish
  :config (global-subword-mode 1))

;; show useless whitespaces
(use-package whitespace
  :defer 1
  :custom
  (whitespace-style '(face trailing tabs tab-mark)) ;; show tabs and trailing spaces
  (whitespace-display-mappings nil) ;; do not show other things
  :config (global-whitespace-mode 1)
  :diminish global-whitespace-mode)

;; (un)fold code with F5
;; See also toggle-selective-display in simple package configuration
(use-package hideshow
  :hook (prog-mode . hs-minor-mode)
  :diminish hs-minor-mode
  :init
  (add-to-list 'hs-special-modes-alist
               '(nxml-mode
                 "<!--\\|<[^/>]*[^/]>"
                 "-->\\|</[^/>]*[^/]>"
                 "<!--"
                 sgml-skip-tag-forward
                 nil))  ;; Add code folding in xml
  :bind (:map prog-mode-map
              ([f5] . hs-toggle-hiding)
              ("C-<f5>" . hs-hide-all)
              ("M-<f5>" . hs-show-all)))

(use-package expand-region
  :bind (("C-<next>" . er/expand-region)
         ("C-<prior>" . er/contract-region)))

(use-package smartparens
  :diminish
  :hook (after-init . smartparens-global-mode)
  :bind (("M-RET" . sp-slurp-hybrid-sexp)
         ("M-a" . sp-beginning-of-sexp)
         ("M-e" . sp-end-of-sexp))
  :custom
  (sp-highlight-pair-overlay nil)
  (sp-highlight-wrap-overlay nil)
  (sp-highlight-wrap-tag-overlay nil)
  :config
  (declare-function sp-pair "sp-pair")
  (declare-function sp-local-pair "sp-local-pair")
  (require 'smartparens-config)
  ;; From https://github.com/hlissner/doom-emacs/blob/develop/modules/config/default/config.el
  ;;
  ;; Autopair quotes more conservatively; if I'm next to a word/before another
  ;; quote, I don't want to open a new pair or it would unbalance them.
  (let ((unless-list '(sp-point-before-word-p
                       sp-point-after-word-p
                       sp-point-before-same-p)))
    (sp-pair "'"  nil :unless unless-list)
    (sp-pair "\"" nil :unless unless-list))
  ;; Expand {|} => { | }
  ;; Expand {|} => {
  ;;   |
  ;; }
  (defun my/smartparens-pair-newline-and-indent (id action context)
    (when (memq this-command
                '(newline newline-and-indent reindent-then-newline-and-indent))
      (save-excursion
        (newline)
        (indent-according-to-mode))
      (indent-according-to-mode)))
  (dolist (brace '("(" "{" "["))
    (sp-pair brace nil
             :post-handlers '(("| " "SPC")
                              ;; ("||\n[i]" "RET"))  ;; BUG: also inserts newline when slurping. Replaced with next line
                              (my/smartparens-pair-newline-and-indent "RET"))
             ;; I likely don't want a new pair if adjacent to a word or opening brace
             :unless '(sp-point-before-word-p sp-point-before-same-p)))
  ;; Don't do square-bracket space-expansion where it doesn't make sense to
  (sp-local-pair '(emacs-lisp-mode org-mode markdown-mode gfm-mode)
                 "[" nil :post-handlers '(:rem ("| " "SPC")))
  ;; Reasonable default pairs for HTML-style comments
  (sp-local-pair (append sp--html-modes '(markdown-mode gfm-mode))
                 "<!--" "-->"
                 :unless '(sp-point-before-word-p sp-point-before-same-p)
                 :actions '(insert) :post-handlers '(("| " "SPC")))
  (sp-with-modes '(markdown-mode gfm-mode)
    (sp-local-pair "```" "```" :post-handlers '(:add ("||\n[i]" "RET")))
    ;; The original rules for smartparens had an odd quirk: inserting two
    ;; asterixex would replace nearby quotes with asterixes. These two rules
    ;; set out to fix this.
    (sp-local-pair "**" nil :actions :rem)
    (sp-local-pair "*" "*"
                   :actions '(insert skip)
                   :unless '(:rem sp-point-at-bol-p)
                   ;; * then SPC will delete the second asterix and assume
                   ;; you wanted a bullet point. * followed by another *
                   ;; will produce an extra, assuming you wanted **|**.
                   :post-handlers '(("[d1]" "SPC") ("|*" "*")))))

(use-package jump-char
  :chords (("jk" . jump-char-forward)
           ("jh" . jump-char-backward)
           ("jl" . isearch-forward)
           ("jg" . isearch-backward))
  :bind (("C-c j" . jump-char-forward)
         ("C-c h" . jump-char-backward)))

(use-package move-text
  :bind (("<C-M-up>" . move-text-up)
         ("<C-M-down>" . move-text-down)))

(use-package multiple-cursors
  :defines my-multiple-cursors-map
  :bind-keymap ("C-c m" . my-multiple-cursors-map)
  :init
  (defvar my-multiple-cursors-map
    (let ((map (make-sparse-keymap)))
      (define-key map (kbd "l") #'mc/edit-lines)
      (define-key map (kbd "C-a") #'mc/edit-beginnings-of-lines)
      (define-key map (kbd "C-e") #'mc/edit-ends-of-lines)
      (define-key map (kbd "C-s") #'mc/mark-all-in-region)
      (define-key map (kbd "n") #'mc/mark-next-like-this)
      (define-key map (kbd "p") #'mc/mark-previous-like-this)
      (define-key map (kbd "e") #'mc/mark-more-like-this-extended)
      (define-key map (kbd "h") #'mc/mark-all-like-this-dwim)
      (define-key map (kbd "r") #'mc/mark-all-in-region-regexp)
      map))
  (key-chord-define-global "mù" my-multiple-cursors-map)
  )

(use-package rainbow-mode
  :pin "gnu"
  :diminish
  :hook (prog-mode . rainbow-mode)
  :custom (rainbow-x-colors nil))

;; Jump to definition of thing at point
(use-package dumb-jump
  :defer t
  :hook (xref-backend-functions . dumb-jump-xref-activate)
  :bind (("M-g d" . xref-find-definitions)
         ;; ("M-p" . dumb-jump-back)
         ;; ("M-q" . dumb-jump-quick-look)
         ))

(use-package font-core
  :ensure nil
  :config
  ;; Enable syntax highlighting
  (global-font-lock-mode 1))

(use-package custom
  :ensure nil
  :defer t
  :custom
  ;; themes directory
  (custom-theme-directory (concat user-emacs-directory "themes"))
  :init
  (load-theme 'mywombat t))

(use-package electric
  :ensure nil
  :hook (prog-mode . (lambda () (electric-indent-local-mode 1)))
  :custom
  ;; disable "magic" indentation for non prog modes
  (electric-indent-mode nil))


;; Open images
(use-package image-file
  :ensure nil
  :config (auto-image-file-mode 1))

;; Open compressed files
(use-package jka-cmpr-hook
  :ensure nil
  :config (auto-compression-mode 1))

;; Remove trailing whitespaces on save
(use-package ws-butler
  :diminish
  :hook (prog-mode . ws-butler-mode))

(use-package eldoc
  :diminish)

;;;;;;;;;;;;;;
;; Dired Stuff

(use-package dirvish
  :init
  (dirvish-override-dired-mode)
  :custom
  (dirvish-subtree-state-style 'arrow)
  (dirvish-path-separators '("~" "/" "/"))
  (dirvish-quick-access-entries ; It's a custom option, `setq' won't work
   '(("H" "~/"                          "Home")
     ("h" "~/.local/git/home_config"    "home_config")
     ("d" "~/Downloads/"                "Downloads")
     ("t" "~/.local/share/Trash/files/" "Trash")))
  :config
  ;; (dirvish-peek-mode) ; Preview files in minibuffer
  (dirvish-side-follow-mode) ; similar to `treemacs-follow-mode'
  (setq dirvish-mode-line-format
        '(:left (sort symlink) :right (omit yank index)))
  (setq dirvish-attributes
        ;; '(nerd-icons file-time file-size collapse subtree-state vc-state git-msg))
        ;; Order doesn't matter, keep this sorting for better comprehension
        '(vc-state subtree-state nerd-icons collapse file-time file-size))
  ;; (setq dirvish-subtree-state-style 'nerd)
  (setq delete-by-moving-to-trash t)
  (setq dired-listing-switches
        ;; "-l -c --almost-all --human-readable --group-directories-first --no-group")
        "-l -c --almost-all --human-readable --group-directories-first")
  :bind ; Bind `dirvish | dirvish-side | dirvish-dwim' as you see fit
  (
   ("C-x f" . dirvish-fd-ask)
   ([f8] . dirvish-side)
   :map dirvish-mode-map ; Dirvish inherits `dired-mode-map'
        ("a"   . dirvish-quick-access)
        ("f"   . dirvish-file-info-menu)
        ("y"   . dirvish-yank-menu)
        ("/"   . dirvish-narrow)
        ;; ("^"   . dirvish-history-last)
        ("$"   . dirvish-history-jump) ; remapped `describe-mode'
        ("s"   . dirvish-quicksort)    ; remapped `dired-sort-toggle-or-edit'
        ;; ("v"   . dirvish-vc-menu)      ; remapped `dired-view-file'
        ("TAB" . dirvish-subtree-toggle)
        ("<backtab>" . dirvish-subtree-remove)
        ;; ("M-f" . dirvish-history-go-forward)
        ;; ("M-b" . dirvish-history-go-backward)
        ;; ("M-l" . dirvish-ls-switches-menu)
        ("M-m" . dirvish-mark-menu)
        ;; ("M-t" . dirvish-layout-toggle)
        ;; ("M-s" . dirvish-setup-menu)
        ;; ("M-e" . dirvish-emerge-menu)
        ;; ("M-j" . dirvish-fd-jump)
  ))


(use-package dired
  :ensure nil
  :defer t
  :bind (:map dired-mode-map
              ("RET" . dired-find-file-follow-symlinks))
  :hook
  (dired-mode . dired-hide-details-mode)
  :custom
  (dired-listing-switches "-AlhcF --group-directories-first")
  (insert-directory-program "~/.local/bin/human_ls")
  (dired-dwim-target t)  ;; when copy/move/... marked files, dired will automatically fill path with other window dired path
  (dired-auto-revert-buffer t)
  :functions dired-get-file-for-visit
  :config
  (defun dired-find-file-follow-symlinks ()
    "Open file or directory and show its canonical path."
    (interactive)
    (let ((original (dired-get-file-for-visit)))
      (if (file-directory-p original)
          (find-alternate-file (file-truename original))
        (find-file original))))
  )




;; (use-package launch
;;   :config (global-launch-mode +1))

(defun buffer-binary-p (&optional buffer)
  "Return whether BUFFER or the current buffer is binary.
A binary buffer is defined as containing at least on null byte.
Returns either nil, or the position of the first null byte."
  (with-current-buffer (or buffer (current-buffer))
    (save-excursion
      (goto-char (point-min))
      (search-forward (string ?\x00) nil t 1))))

(defun hexl-if-binary ()
  "Use \"hexl-mode\" for binary buffers.
It will do nothing in \"hexl-mode\" buffers"
  (interactive)
  (unless (eq major-mode 'hexl-mode)
    (when (buffer-binary-p)
      (hexl-mode)
      (read-only-mode))))

(add-hook 'find-file-hook 'hexl-if-binary)

(use-package dired-open
  :after dired
  :bind (:map dired-mode-map
              ("o" . dired-open-xdg)))



;;;;;;;;;;
;; Isearch

(use-package isearch
  :ensure nil
  :custom
  (isearch-lazy-count t)
  ;; (isearch-yank-on-move 'shift)
  ;; (isearch-allow-scroll 'unlimited)
  :bind (:map isearch-mode-map
              ("M-<home>" . isearch-beginning-of-buffer)
              ("M-<end>" . isearch-end-of-buffer)))


;;;;;;;;;;;;;;;;;;;;;;;;;
;; minibuffer : vertico + consult + orderless + marginalia

(setq read-buffer-completion-ignore-case t
      completion-ignore-case t)

(use-package minibuffer
  :ensure nil
  :custom
  (read-file-name-completion-ignore-case t))

(use-package vertico
  :pin "gnu"
  :hook (after-init . vertico-mode)
  :bind (("C-c r" . vertico-repeat)
         :map vertico-map
         ("<prior>" . nil)
         ("<next>" . nil))
  :custom
  (vertico-cycle t)
  (vertico-group-format "==  %s  ")
  (vertico-resize t)
  :functions vertico-bottom--display-candidates vertico-directory--completing-file-p vertico-previous vertico-next
  :defines vertico-map
  :init
   ;; Input at bottom
   (defun vertico-bottom--display-candidates (lines)
     "Display LINES in bottom."
     (move-overlay vertico--candidates-ov (point-min) (point-min))
     (unless (eq vertico-resize t)
       (setq lines (nconc (make-list (max 0 (- vertico-count (length lines))) "\n") lines)))
     (let ((string (apply #'concat lines)))
       (add-face-text-property 0 (length string) 'default 'append string)
       (overlay-put vertico--candidates-ov 'before-string string)
       (overlay-put vertico--candidates-ov 'after-string nil))
     (vertico--resize))

   (advice-add #'vertico--display-candidates :override #'vertico-bottom--display-candidates)

   ;; https://github.com/minad/vertico/wiki#additions-for-moving-up-and-down-directories-in-find-file
   (defun vertico-directory-delete-entry ()
     "Delete directory or entire entry before point."
     (interactive)
     (when (and (> (point) (minibuffer-prompt-end))
                (vertico-directory--completing-file-p))
       (save-excursion
         (goto-char (1- (point)))
         (when (search-backward "/" (minibuffer-prompt-end) t)
           (delete-region (1+ (point)) (point-max))
           t))))

   ;; (add-to-list 'vertico-multiform-categories '(embark-keybinding grid))
   ;; (vertico-multiform-mode)
   )

(use-package orderless
  :custom
  (completion-styles '(orderless substring))
  (completion-category-defaults nil)
  (completion-category-overrides '((file (styles partial-completion))))
  :config
  (defun without-if-bang (pattern _index _total)
    (cond
     ((equal "!" pattern)
      '(orderless-literal . ""))
     ((string-prefix-p "!" pattern)
      `(orderless-without-literal . ,(substring pattern 1)))))

  (setq orderless-matching-styles '(orderless-literal)
        orderless-style-dispatchers '(without-if-bang))
  )


(use-package consult
  ;; Replace bindings. Lazily loaded due by `use-package'.
  :bind (;; C-c bindings (mode-specific-map)
         ("C-c h" . consult-history)
         ("C-c m" . consult-mode-command)
         ("C-c b" . consult-bookmark)
         ("C-c k" . consult-kmacro)
         ;; C-x bindings (ctl-x-map)
         ("C-x M-:" . consult-complex-command)     ;; orig. repeat-complex-command
         ;; ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
         ("C-x 4 b" . consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
         ("C-x 5 b" . consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame
         ;; Custom M-# bindings for fast register access
         ("M-#" . consult-register-load)
         ("M-'" . consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
         ("C-M-#" . consult-register)
         ;; Other custom bindings
         ("M-y" . consult-yank-pop)                ;; orig. yank-pop
         ("<help> a" . consult-apropos)            ;; orig. apropos-command
         ;; M-g bindings (goto-map)
         ("M-g e" . consult-compile-error)
         ;; ("M-g f" . consult-flymake)               ;; Alternative: consult-flycheck
         ("M-g g" . consult-goto-line)             ;; orig. goto-line
         ("M-g M-g" . consult-goto-line)           ;; orig. goto-line
         ("M-g o" . consult-outline)               ;; Alternative: consult-org-heading
         ("M-g m" . consult-mark)
         ("M-g k" . consult-global-mark)
         ("M-g i" . consult-imenu)
         ("M-g I" . consult-imenu-multi)
         ;; M-s bindings (search-map)
         ("M-s f" . consult-find)
         ("M-s F" . consult-locate)
         ("M-s g" . consult-grep)
         ("C-c g" . consult-git-grep)
         ("M-s r" . consult-ripgrep)
         ("M-s l" . consult-line)
         ("M-s L" . consult-line-multi)
         ("M-s m" . consult-multi-occur)
         ("M-s k" . consult-keep-lines)
         ("M-s u" . consult-focus-lines)
         ;; Isearch integration
         ("M-s e" . consult-isearch)
         ("C-c C-f" . consult-find)
         ("C-c g" . consult-git-grep)
;;          ("C-c d" . counsel-semantic-or-imenu)
         ("M-g f" . consult-semantic-or-imenu)
         ("M-g b" . consult-mark)
         ("M-y"   . consult-yank-pop)
         ("C-s" . consult-line)
         ([remap switch-to-buffer] . consult-buffer)
         :map isearch-mode-map
         ("M-e" . consult-isearch)                 ;; orig. isearch-edit-string
         ("M-s e" . consult-isearch)               ;; orig. isearch-edit-string
         ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
         ("M-s L" . consult-line-multi)           ;; needed by consult-line to detect isearch
         )
  :functions my/history-or-next-element my/history-or-previous-element vertico-previous vertico-next
  :config
  (defun my/history-or-previous-element ()
    "If minibuffer prompt empty, previous-history-element, else vertico-next"
    (interactive)
    (if (= (length (minibuffer-contents-no-properties)) 0)
        (previous-history-element 1)
        (vertico-previous)
      )
    )

  (defun my/history-or-next-element ()
    "If minibuffer prompt empty, previous-history-element, else vertico-next"
    (interactive)
    (if (= (length (minibuffer-contents-no-properties)) 0)
        (previous-history-element 1)
        (vertico-next)
      )
    )

  (defvar my-consult-line-map
    (let ((map (make-sparse-keymap)))
      (define-key map (kbd "C-s") #'my/history-or-next-element)
      (define-key map (kbd "C-r") #'my/history-or-previous-element)
      (define-key map (kbd "<next>") nil)
      (define-key map (kbd "<prior>") nil)

      map))

  (consult-customize consult-line :keymap my-consult-line-map)
  )

(use-package consult-dir
  :after (vertico consult)
  :defines vertico-map
  :bind (("C-x C-d" . consult-dir)
         :map vertico-map
         ("C-x C-d" . consult-dir)
         ("C-x C-j" . consult-dir-jump-file)))

(use-package marginalia
  :after vertico
  :bind
  (:map minibuffer-local-map
            ("M-a" . marginalia-cycle))
  :init
  (marginalia-mode))

(use-package embark

  :bind
  (("M-ù" . embark-act)         ;; pick some comfortable binding
   ("M-*" . embark-dwim)        ;; good alternative: M-.
   ;; ("C-h B" . embark-bindings) ;; alternative for `describe-bindings'
   )
  :init

  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)

  ;; Show the Embark target at point via Eldoc. You may adjust the
  ;; Eldoc strategy, if you want to see the documentation from
  ;; multiple providers. Beware that using this can be a little
  ;; jarring since the message shown in the minibuffer can be more
  ;; than one line, causing the modeline to move up and down:

  ;; (add-hook 'eldoc-documentation-functions #'embark-eldoc-first-target)
  ;; (setq eldoc-documentation-strategy #'eldoc-documentation-compose-eagerly)

  :config

  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none))))
)

(use-package embark-consult
  ; only need to install it, embark loads it after consult if found
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

;; (use-package xcscope
;;   :disabled
;;   :init (setq-default cscope-keymap-prefix "C-c C-s"))

;; (use-package helm-cscope
;;   :after (helm xcscope)
;;   :bind (("C-c s g" . helm-cscope-find-global-definition)
;;          ([remap cscope-find-global-definition] . helm-cscope-find-global-definition)
;;          ("C-c s u" . helm-cscope-pop-mark)
;;          ([remap cscope-pop-mark] . helm-cscope-pop-mark)
;;          ("C-c s s" . helm-cscope-find-this-symbol)
;;          ([remap cscope-find-this-symbol] . helm-cscope-find-this-symbol)
;;          ("C-c s c" . helm-cscope-find-calling-this-function)
;;          ([remap cscope-find-calling-this-function] . helm-cscope-find-calling-this-function))
;;   :config '(disable-command cscope-minor-mode)
;;   '(disable-command cscope-setup))

;; (use-package clang-format
;;   :init
;;   (setq clang-format-style "{BasedOnStyle: chromium, \
;;                              AccessModifierOffset: -4, \
;;                              AllowShortBlocksOnASingleLine: true, \
;;                              AllowShortIfStatementsOnASingleLine: false, \
;;                              AlwaysBreakTemplateDeclarations: true, \
;;                              IndentWidth: 4, \
;;                              ColumnLimit: 100, \
;;                              BreakBeforeBraces : Stroustrup, \
;;                              Standard: Cpp11}"))

(use-package flycheck
  :commands flycheck-mode
  :hook (prog-mode . flycheck-mode))

(use-package flycheck-indicator
  :hook (flycheck-mode . flycheck-indicator-mode))

(use-package flymake
  :bind (("C-c n" . flymake-goto-next-error)
         ("C-c p" . flymake-goto-previous-error))
  )


(use-package elisp-mode
  :ensure nil
  :bind (:map emacs-lisp-mode-map
              ("C-c C-f" . nil)
              ("C-c C-c" . eval-region))
  )

;; (use-package elpy
;;   :hook (python-mode . elpy-mode)
;;   :bind (:map elpy-mode-map   ;; Disable elpy-nav-* actions
;;               (("M-<up>" . nil)
;;                ("M-<down>" . nil)
;;                ("M-<left>" . nil)
;;                ("M-<right>" . nil)))
;;   :config
;;   (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
;;   (setq gud-pdb-command-name "python -m pdb")
;;   (elpy-enable))

(use-package yasnippet
  :defer t
  :diminish yas-minor-mode
  :commands yas-minor-mode
  :hook (after-init . yas-minor-mode)
  :custom (yas-snippet-dirs (append yas-snippet-dirs '("~/.config/emacs/snippets/")))
  :init (setq yas-verbosity 0)
  :config (yas-global-mode 1))

(use-package autoinsert
  :hook (after-init . auto-insert-mode)
  :custom (auto-insert-query nil))

(use-package yatemplate
  :hook (auto-insert-mode . yatemplate-fill-alist))

;; Org mode
(use-package org
  :bind (("C-c a" . org-agenda)
         ;; ("<f6>" . org-capture)
         (:map org-mode-map
               ("M-<up>" . nil)
               ("M-<down>" . nil)
               ("M-<left>" . nil)
               ("M-<right>" . nil)
               ("M-S-<up>" . nil)
               ("M-S-<down>" . nil)
               ("M-S-<left>" . nil)
               ("M-S-<right>" . nil)
               ("C-M-<down>" . nil)
               ("C-M-<up>" . nil)
               ("C-M-<right>" . org-metaright)  ; Demote
               ("C-M-<left>" . org-metaleft)  ; Promote
               ("C-S-M-<down>" . org-metadown)  ; Switch with item below
               ("C-S-M-<up>" . org-metaup)  ; Switch with item above
               ("C-S-M-<right>" . org-shiftmetaright)  ; Demote recursivly
               ("C-S-M-<left>" . org-shiftmetaleft)  ; Promote recursively
               ("M-*" . my/org-emphasize-dwim)  ; Surround by [*/_=~+]
               ))
  :custom
  ;; (org-modules (delq 'org-gnus org-modules));; '(org-crypt ol-docview org-id ol-info org-protocol))
  (org-capture-templates
   '(("t" "Todo" entry (file+headline "/data/zim/home.org" "Tasks")
      "* TODO %?" "%i" "%a")
     ("j" "Journal" entry (file+datetree "/data/zim/journal.org")
      "* %?\nEntered on %U\n  %i\n  %a")))
  (org-todo-keywords '("TODO" "STARTED" "WAITING" "DONE"))
  (org-confirm-babel-evaluate nil)
  (org-agenda-include-diary t)
  (org-agenda-include-all-todo t)
  ;; :preface (setq org-modules nil)
  :config
  (declare-function org-emphasize "org-emphasize")
  (setq org-modules '(org-id ol-info org-protocol))
  (defun my/org-emphasize-dwim (&optional char)
    "Emphasize word or selection."
    (interactive)
    (unless (region-active-p)
      (backward-word)
      (mark-word))
    (org-emphasize char))
  )

(use-package org-bullets
  :after org
  :init
  (setq org-bullets-face-name "Inconsolata-12")
  (setq org-bullets-bullet-list
        '("•" ":" "⁖" "⁘" "⁙" "○"))
  :hook (org-mode . (lambda () (org-bullets-mode 1))))

(use-package persistent-scratch
  :config
  (persistent-scratch-setup-default))

(use-package hippie-exp
  :ensure nil
  :bind ("M-/" . hippie-expand))

(use-package dabbrev
  :ensure nil
  :bind ("M-!" . dabbrev-expand))

;; Only on archlinux
(if (file-exists-p "/usr/bin/pacman")
    (use-package pacfiles-mode
      :defer t)
  (message "Not an Archlinux system, skipping...")
  )

(use-package magit
  :defer t
  :bind ([f2] . magit-file-dispatch)
  :custom
  ;; (magit-diff-refine-hunk t)
  (magit-section-initial-visibility-alist '((stashes . hide)
                                            (recent . show)))
  (magit-status-sections-hook '(magit-insert-status-headers
                                magit-insert-merge-log
                                magit-insert-rebase-sequence
                                magit-insert-am-sequence
                                magit-insert-sequencer-sequence
                                magit-insert-bisect-output
                                magit-insert-bisect-rest
                                magit-insert-bisect-log
                                magit-insert-untracked-files
                                magit-insert-unstaged-changes
                                magit-insert-staged-changes
                                magit-insert-unpushed-to-pushremote
                                magit-insert-unpushed-to-upstream-or-recent
                                magit-insert-unpulled-from-pushremote
                                magit-insert-unpulled-from-upstream
                                magit-insert-recent-commits
                                magit-insert-stashes))

  )

(use-package git-gutter
  :hook (after-init . global-git-gutter-mode)
  :diminish
  )

(use-package git-rebase
  :defer t
  :ensure nil
  :config
  ;; I could not do it with ":bind (:map ...)", so do it the good old fashion way
  (define-key git-rebase-mode-map [C-M-up] 'git-rebase-move-line-up)
  (define-key git-rebase-mode-map [C-M-down] 'git-rebase-move-line-down)
  (define-key git-rebase-mode-map [M-up] nil)
  (define-key git-rebase-mode-map [M-down] nil))

;; (use-package magit-popup)

(use-package find-file
  :ensure nil
  :bind (([f4] . ff-find-other-file)))

(use-package project
  :defer t
  :ensure nil
  :bind-keymap (("C-x p" . project-prefix-map))
  :bind (("C-c C-f" . project-find-file)
         :map project-prefix-map
         ("m" . magit-project-status)))



(use-package editorconfig
  :defer 1
  :diminish
  :config (editorconfig-mode 1))

;; ;; By default Emacs doesn't read from the same environment variables set in your terminal. This package fixes that.
;; ;; See https://github.com/emacsmirror/exec-path-from-shell
;; (use-package exec-path-from-shell
;;     :ensure t
;;     :config
;;     (exec-path-from-shell-initialize))

;; Web Development ;;;;;;;;;;;;;;;;;;;;;

(use-package js
  :ensure nil
  :defer t
  :custom
  (js-indent-level 2))

(use-package js2-mode
  :mode  (("\\.js\\'" . js2-mode))
  :bind ((:map js2-mode-map
               ("C-c C-f" . nil)))
  )

(use-package js-auto-format-mode
  :hook ((js2-mode web-mode vue-mode) .  js-auto-format-mode))


(use-package add-node-modules-path
  :hook ((js2-mode web-mode vue-mode) . add-node-modules-path))


(use-package web-mode
  :defer t
  :custom
  (web-mode-markup-indent-offset 2)
  (web-mode-code-indent-offset 2)
  (web-mode-css-indent-offset 2)
  (tab-width 2)
  (js-indent-level 2)
  (typescript-indent-level 2)
  (web-mode-enable-current-column-highlight t)
  (web-mode-enable-current-element-highlight t)
  (web-mode-enable-comment-interpolation t)
  (web-mode-enable-part-face t)
  (web-mode-enable-block-face t)
  (web-mode-enable-css-colorization t)
  (web-mode-enable-auto-pairing nil)  ;; avoid conflict with smartparens
  (web-mode-enable-auto-closing t)
  (web-mode-tag-auto-close-style 1)
  :mode (("\\.php$" .  web-mode)
         ("\\.html$" .  web-mode)
         ("\\.html\\.twig$" . web-mode)
         )
  :bind ((:map web-mode-map
               ("<f5>" . web-mode-fold-or-unfold)
               ("C-c C-f" . nil))))

;; (use-package emmet-mode
;;   :after web-mode
;;   :hook
;;   (web-mode . emmet-mode)
;;   (css-mode . emmet-mode)
;;   (web-mode-before-auto-complete . (lambda ()
;;                                      (let ((web-mode-cur-language
;;                                             (web-mode-language-at-pos)))
;;                                        (if (string= web-mode-cur-language "php")
;;                                            (yas-activate-extra-mode 'php-mode)
;;                                          (yas-deactivate-extra-mode 'php-mode))
;;                                        (if (string= web-mode-cur-language "css")
;;                                            (setq emmet-use-css-transform t)
;;                                          (setq emmet-use-css-transform nil))))))


(define-derived-mode my-vue-mode web-mode "myVue"
  "A major mode derived from web-mode, for editing .vue files with LSP support.")
(add-to-list 'auto-mode-alist '("\\.vue\\'" . my-vue-mode))


(use-package c++-mode
  :ensure nil
  :mode (("\\.h\\'" . c++-mode)))

(use-package sh-mode
  :ensure nil
  :mode (("PKGBUILD" . sh-mode)))


;;;;;; LSP ;;;;;;

(use-package eglot
  :hook ((js-mode     . eglot-ensure)
         (lua-mode    . eglot-ensure)
         (js2-mode    . eglot-ensure)
         (css-mode    . eglot-ensure)
         (c++-mode    . eglot-ensure)
         (sh-mode     . eglot-ensure)
         (bash-ts-mode . eglot-ensure)
         (rust-mode   . eglot-ensure)
         (rust-ts-mode . eglot-ensure)
         (python-mode . eglot-ensure)
         (python-ts-mode . eglot-ensure))
  :config
  (add-to-list 'eglot-server-programs
               '(qml-mode . ("qml-lsp"))))

(use-package jsonrpc
  :config
  (declare-function jsonrpc--log-event "jsonrpc--log-event")
  (fset #'jsonrpc--log-event #'ignore))

  ;; :hook (prog-mode . eglot-ensure))

(use-package flycheck-eglot
  :after (flycheck eglot)
  :config
  (global-flycheck-eglot-mode 1))


;; Display messages when idle, without prompting
(setq help-at-pt-display-when-idle t)


(use-package clang-format
  :defer t)
(use-package modern-cpp-font-lock
  :hook (c++-mode . modern-c++-font-lock-mode))
(use-package flycheck-clang-tidy
  :after flycheck
  :hook
  (flycheck-mode . flycheck-clang-tidy-setup)
  :custom
  (flycheck-clang-language-standard "c++17"))



(setq-default major-mode
              (lambda () ; guess major mode from file name
                (unless buffer-file-name
                  (let ((buffer-file-name (buffer-name)))
                    (set-auto-mode)))))

(setq major-mode-remap-alist
 '((bash-mode . bash-ts-mode)
   (js2-mode . js-ts-mode)
   (typescript-mode . typescript-ts-mode)
   (json-mode . json-ts-mode)
   (css-mode . css-ts-mode)
   (python-mode . python-ts-mode)
   (rust-mode . rust-ts-mode)))


;; LUA Development ;;;;;;;;;;;;;;;;;;;;;

(use-package lua-mode
  ;; :bind-keymap
  ;; ("C-c C-l" . lua-prefix-mode-map)
  :custom
  (lua-indent-level 4)
  (lua-prefix-key nil)
  ;; :mode "\\.lua$"
  :bind ((:map lua-mode-map
               ("<f5>" . hs-toggle-hiding))))


;; QT Development
(use-package qml-mode
  :after js
  :custom
  (qml-indent-width 4 "set indentation")
  (js-jsx-indent-level 4 "set indentation (qml is derived from js)")
  (js-indent-level 4 "set indentation (qml is derived from js)"))

(use-package qt-pro-mode
  :mode (("\\.pr[io]$" . qt-pro-mode)))

(use-package nxml-mode
  :ensure nil
  :mode(("\\.qrc$" . nxml-mode))
  :mode(("\\.svg$" . nxml-mode))
  :bind (:map nxml-mode-map
              (("C-c C-f" . nil)
               ("C-c C-e" . nxml-finish-element)))
  )

(use-package prog-mode
  :ensure nil
  :hook ((emacs-lisp-mode . prettify-symbols-mode)
         (emacs-lisp-mode . (lambda ()
                              (mapc (lambda (pair) (push pair prettify-symbols-alist))
                                    '(;; Syntax
                                      ("use-package" . "⁋")
                                      )))))
  :custom
  (prettify-symbols-unprettify-at-point 'right-edge)
  )


(use-package rust-mode)


;; https://github.com/emacsorphanage/quickrun
(use-package quickrun)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Emails
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package mu4e
  :ensure nil
  :load-path "/usr/share/emacs/site-lisp/mu4e"
  :commands mu4e
  :config
  (add-to-list 'mu4e-marks
               '(spam
                 :char       "s"
                 :prompt     "Spam"
                 :show-target (lambda (target) mu4e-spam-folder)
                 :action      (lambda (docid msg target)
                                (mu4e~proc-move docid mu4e-spam-folder "+S-u-N"))))
  (mu4e~headers-defun-mark-for spam)
  (setq
   mu4e-headers-draft-mark     '("D" . "🚧")
   mu4e-headers-flagged-mark   '("F" . "❤")
   mu4e-headers-new-mark       '("N" . "✨")
   mu4e-headers-passed-mark    '("P" . "↪")
   mu4e-headers-replied-mark   '("R" . "↩")
   mu4e-headers-seen-mark      '("S" . "✓")
   mu4e-headers-trashed-mark   '("T" . "❌")
   mu4e-headers-attach-mark    '("a" . "📎")
   mu4e-headers-encrypted-mark '("x" . "🔒")
   mu4e-headers-signed-mark    '("s" . "🔑")
   mu4e-headers-unread-mark    '("u" . "📩")
   mu4e-headers-list-mark      '("l" . "Ⓛ")
   mu4e-headers-personal-mark  '("p" . "👨")
   mu4e-headers-calendar-mark  '("c" . "📅"))
  :bind
  (:map mu4e-headers-mode-map
        ("s" . mu4e-headers-mark-for-spam))
  :custom
  (mail-user-agent 'mu4e-user-agent)
  (mu4e-maildir (expand-file-name "~/emails"))
  (mu4e-get-mail-command "offlineimap")
  (mu4e-update-interval nil)
  (message-kill-buffer-on-exit t) ;; don't keep message buffers around
  (mu4e-use-fancy-chars t)
  (mu4e-attachment-dir "~/Downloads")
  (mu4e-completing-read-function 'completing-read)
  (mu4e-maildir-shortcuts
   '(("/capcat/INBOX" . ?i)
     ("/capcat/pro" . ?p)
     ("/capcat/Drafts" . ?d)
     ("/capcat/Sent" . ?s)))
  (mu4e-bookmarks
   '(
     ( :name "PRO"
       :query "to:pro@capcat.net or cc:pro@capcat.net or bcc:pro@capcat.net or from:pro@capcat.net"
       :key   ?p )
     ( :name  "All Inbox"
       :query "maildir:/capcat/INBOX or maildir:/ak42/INBOX"
       :key   ?a )
     ( :name  "Unread messages"
       :query "flag:unread AND NOT flag:trashed"
       :key ?u)
     ( :name "Today's messages"
       :query "date:today..now AND NOT flag:trashed"
       :key ?t)
     ( :name "Last 7 days"
       :query "date:7d..now"
       :hide-unread t
       :key ?w)
     ( :name "Messages with images"
       :query "mime:image/*"
       :key ?i)
     ( :name  "Stared flagged"
       :query "flag:flagged"
       :key   ?f )
     ( :name  "Big messages"
       :query "size:5M..500M"
       :key   ?b )
     ))
  )

;; (use-package mu4e-maildirs-extension
;;   :after mu4e)

(use-package mu4e-context
  :after mu4e
  :load-path "/usr/share/emacs/site-lisp/mu4e"
  :custom
  (mu4e-context-policy 'pick-first)
  (mu4e-contexts
   (list
    ;; capcat account
    (make-mu4e-context
     :name "capcat"
     :match-func
     (lambda (msg)
       (when msg
         (string-prefix-p "/capcat" (mu4e-message-field msg :maildir))))
     :vars '((user-mail-address                . "grumph@capcat.net")
             (user-full-name                   . "Grumph")
             (mu4e-drafts-folder               . "/capcat/Drafts")
             (mu4e-sent-folder                 . "/capcat/Sent")
             (mu4e-refile-folder               . "/capcat/INBOX")
             (mu4e-trash-folder                . "/capcat/Trash")
             (mu4e-spam-folder                 . "/capcat/Junk")))
    (make-mu4e-context
     :name "ak42"
     :match-func
     (lambda (msg)
       (when msg
         (string-prefix-p "/ak42" (mu4e-message-field msg :maildir))))
     :vars '((user-mail-address                . "ak42@free.fr")
             (user-full-name                   . "Grumph")
             (mu4e-drafts-folder               . "/ak42/Drafts")
             (mu4e-sent-folder                 . "/ak42/Sent")
             (mu4e-refile-folder               . "/ak42/INBOX")
             (mu4e-trash-folder                . "/ak42/Trash")
             (mu4e-spam-folder                 . "/ak42/Junk")))))

  )

(use-package mu4e-column-faces
  :after mu4e
  :config (mu4e-column-faces-mode))

(use-package mu4e-jump-to-list
  :after mu4e)

(use-package mu4e-query-fragments
  :after mu4e
  :custom
  (mu4e-query-fragments-list '(("%junk" . "maildir:/Junk OR subject:SPAM")
                               ("%hidden" . "flag:trashed OR %junk")))
  (mu4e-query-fragments-append "NOT %hidden"))

(use-package smtpmail
  :custom
  (smtpmail-stream-type 'ssl)
  (message-send-mail-function 'smtpmail-send-it)
  (smtpmail-smtp-server         "mail.infomaniak.com")
  (smtpmail-smtp-service 465)
  (smtpmail-auth-credentials
     '(("mail.infomaniak.com" 465 "grumph@capcat.net" nil)))
  )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Key Bindings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Don't ask confirmation for killing a buffer
(global-set-key [remap kill-buffer] #'kill-this-buffer)
;; Don't consider spaces at end of line for kill-line
(global-set-key [remap kill-line] 'kill-and-join-forward)

(global-set-key (kbd "C-<up>")     'backward-paragraph)
(global-set-key (kbd "C-<down>")   'forward-paragraph)
(global-set-key (kbd "C-<right>")  'my/forward-word)
(global-set-key (kbd "C-<left>")   'my/backward-word)
(global-set-key (kbd "C-<delete>")    'my/forward-kill-word)
;; C-h is the same as C-backspace, so move help to F1
(global-set-key (kbd "C-<backspace>") 'my/backward-kill-word)
(global-set-key (kbd "C-h") 'my/backward-kill-word)
(global-set-key [f1] 'help-command)
(define-key help-map [f1] nil)  ;; normally  bound to help-for-help, needed for which-key pager
(setq help-char (string-to-char "<f1>"))

(global-set-key (kbd "C-x C-f") 'find-file-at-point)


;; (defvar real-keyboard-keys
;;   '(("M-<up>"        . "\M-[1;3A")
;;     ("M-<down>"      . "\M-[1;3B")
;;     ("M-<right>"     . "\M-[1;3C")
;;     ("M-<left>"      . "\M-[1;3D")
;;     ("C-<return>"    . "\C-j")
;;     ("C-<delete>"    . "\M-[3;5~")
;;     ("C-<up>"        . "\M-[1;5A")
;;     ("C-<down>"      . "\M-[1;5B")
;;     ("C-<right>"     . "\M-[1;5C")
;;     ("C-<left>"      . "\M-[1;5D"))
;;   "An assoc list of pretty key strings
;; and their terminal equivalents.")

;; ;;putty Arrow Keys
;; ;;
;; (defvar real-keyboard-keys
;;     ‘((“M-<up>”        . ”\C-[\C-[OA”)
;;       (“M-<down>”      . ”\C-[\C-[OB”)
;;       (“M-<right>”     . ”\C-[\C-[OC”)
;;       (“M-<left>”      . ”\C-[\C-[OD”)
;;       (“C-<up>”        . ”\C-[[A”)
;;       (“C-<down>”      . ”\C-[[B”)
;;       (“C-<right>”     . ”\C-[[C”)
;;       (“C-<left>”      . ”\C-[[D”))
;;       “An assoc list of pretty key strings
;; and their terminal equivalents.”)

;; handle tmux's xterm-keys
;; put the following line in your ~/.tmux.conf:
;;   setw -g xterm-keys on
(if (getenv "TMUX")
    (progn
      (let ((x 2) (tkey ""))
        (while (<= x 8)
          ;; shift
          (if (= x 2)
              (setq tkey "S-"))
          ;; alt
          (if (= x 3)
              (setq tkey "M-"))
          ;; alt + shift
          (if (= x 4)
              (setq tkey "M-S-"))
          ;; ctrl
          (if (= x 5)
              (setq tkey "C-"))
          ;; ctrl + shift
          (if (= x 6)
              (setq tkey "C-S-"))
          ;; ctrl + alt
          (if (= x 7)
              (setq tkey "C-M-"))
          ;; ctrl + alt + shift
          (if (= x 8)
              (setq tkey "C-M-S-"))

          ;; arrows
          (define-key key-translation-map (kbd (format "M-[ 1 ; %d A" x)) (kbd (format "%s<up>" tkey)))
          (define-key key-translation-map (kbd (format "M-[ 1 ; %d B" x)) (kbd (format "%s<down>" tkey)))
          (define-key key-translation-map (kbd (format "M-[ 1 ; %d C" x)) (kbd (format "%s<right>" tkey)))
          (define-key key-translation-map (kbd (format "M-[ 1 ; %d D" x)) (kbd (format "%s<left>" tkey)))
          ;; home
          (define-key key-translation-map (kbd (format "M-[ 1 ; %d H" x)) (kbd (format "%s<home>" tkey)))
          ;; end
          (define-key key-translation-map (kbd (format "M-[ 1 ; %d F" x)) (kbd (format "%s<end>" tkey)))
          ;; page up
          (define-key key-translation-map (kbd (format "M-[ 5 ; %d ~" x)) (kbd (format "%s<prior>" tkey)))
          ;; page down
          (define-key key-translation-map (kbd (format "M-[ 6 ; %d ~" x)) (kbd (format "%s<next>" tkey)))
          ;; insert
          (define-key key-translation-map (kbd (format "M-[ 2 ; %d ~" x)) (kbd (format "%s<delete>" tkey)))
          ;; delete
          (define-key key-translation-map (kbd (format "M-[ 3 ; %d ~" x)) (kbd (format "%s<delete>" tkey)))
          ;; f1
          (define-key key-translation-map (kbd (format "M-[ 1 ; %d P" x)) (kbd (format "%s<f1>" tkey)))
          ;; f2
          (define-key key-translation-map (kbd (format "M-[ 1 ; %d Q" x)) (kbd (format "%s<f2>" tkey)))
          ;; f3
          (define-key key-translation-map (kbd (format "M-[ 1 ; %d R" x)) (kbd (format "%s<f3>" tkey)))
          ;; f4
          (define-key key-translation-map (kbd (format "M-[ 1 ; %d S" x)) (kbd (format "%s<f4>" tkey)))
          ;; f5
          (define-key key-translation-map (kbd (format "M-[ 15 ; %d ~" x)) (kbd (format "%s<f5>" tkey)))
          ;; f6
          (define-key key-translation-map (kbd (format "M-[ 17 ; %d ~" x)) (kbd (format "%s<f6>" tkey)))
          ;; f7
          (define-key key-translation-map (kbd (format "M-[ 18 ; %d ~" x)) (kbd (format "%s<f7>" tkey)))
          ;; f8
          (define-key key-translation-map (kbd (format "M-[ 19 ; %d ~" x)) (kbd (format "%s<f8>" tkey)))
          ;; f9
          (define-key key-translation-map (kbd (format "M-[ 20 ; %d ~" x)) (kbd (format "%s<f9>" tkey)))
          ;; f10
          (define-key key-translation-map (kbd (format "M-[ 21 ; %d ~" x)) (kbd (format "%s<f10>" tkey)))
          ;; f11
          (define-key key-translation-map (kbd (format "M-[ 23 ; %d ~" x)) (kbd (format "%s<f11>" tkey)))
          ;; f12
          (define-key key-translation-map (kbd (format "M-[ 24 ; %d ~" x)) (kbd (format "%s<f12>" tkey)))
          ;; f13
          (define-key key-translation-map (kbd (format "M-[ 25 ; %d ~" x)) (kbd (format "%s<f13>" tkey)))
          ;; f14
          (define-key key-translation-map (kbd (format "M-[ 26 ; %d ~" x)) (kbd (format "%s<f14>" tkey)))
          ;; f15
          (define-key key-translation-map (kbd (format "M-[ 28 ; %d ~" x)) (kbd (format "%s<f15>" tkey)))
          ;; f16
          (define-key key-translation-map (kbd (format "M-[ 29 ; %d ~" x)) (kbd (format "%s<f16>" tkey)))
          ;; f17
          (define-key key-translation-map (kbd (format "M-[ 31 ; %d ~" x)) (kbd (format "%s<f17>" tkey)))
          ;; f18
          (define-key key-translation-map (kbd (format "M-[ 32 ; %d ~" x)) (kbd (format "%s<f18>" tkey)))
          ;; f19
          (define-key key-translation-map (kbd (format "M-[ 33 ; %d ~" x)) (kbd (format "%s<f19>" tkey)))
          ;; f20
          (define-key key-translation-map (kbd (format "M-[ 34 ; %d ~" x)) (kbd (format "%s<f20>" tkey)))

          (setq x (+ x 1))
          ))
      )
  )

(load-file (no-littering-expand-etc-file-name "custom.el"))

(provide 'init)
;;; init.el ends here
