local wezterm = require 'wezterm';

require "open_editor_with_scrollback"


wezterm.on("window-config-reloaded", function(window, pane)
  window:toast_notification("wezterm", "configuration reloaded!", nil, 2000)
end)



return {
    font = wezterm.font_with_fallback({
            {family="MonacoB2 Nerd Font Mono", stretch="Condensed"},
            {family="MonacoB Nerd Font Mono", stretch="Condensed"},
            {family="ProggyVector NF"},
            {family="TerminessTTF NF"},
    }),
    freetype_load_target = "HorizontalLcd",
    dpi = 176.0,
    font_size = 9.0,
    line_height = 0.95,

    bold_brightens_ansi_colors = true,
    window_background_opacity = 0.85,
    -- text_background_opacity = 0.7,

    tab_bar_at_bottom = true,
    tab_max_width = 32,

    enable_scroll_bar = true,

    default_cursor_style = "SteadyBar",

    pane_focus_follows_mouse = true,

    quick_select_alphabet = "qsdfazerwxcvjklmuiopghtybn",

    selection_word_boundary = " \t\n{}[]()\"'`=:",

    -- No api.github.com calls
    check_for_updates = false,

    -- Adjust scroll speed for emacs and others
    alternate_buffer_wheel_scroll_speed = 1,

    -- Better for tiling window managers
    adjust_window_size_when_changing_font_size = false,

    window_close_confirmation = "AlwaysPrompt",


    keys = {
        {mods="CTRL|SHIFT", key="E",
         action=wezterm.action{EmitEvent="open-editor-with-scrollback"}},

        -- Switch tabs
        {mods="CTRL", key="Tab",
         action={ActivateTabRelative=1}},
        {mods="CTRL|SHIFT", key="Tab",
         action={ActivateTabRelative=-1}},
        {mods="CTRL", key="²",
         action="ActivateLastTab"},
        -- Move tabs
        {mods="CTRL|ALT", key="Tab",
         action={MoveTabRelative=1}},
        {mods="CTRL|SHIFT|ALT", key="Tab",
         action={MoveTabRelative=-1}},

        {mods="CTRL|SHIFT|ALT", key="LeftArrow",
         action="DisableDefaultAssignment"},
        {mods="CTRL|SHIFT|ALT", key="RightArrow",
         action="DisableDefaultAssignment"},
        {mods="CTRL|SHIFT|ALT", key="UpArrow",
         action="DisableDefaultAssignment"},
        {mods="CTRL|SHIFT|ALT", key="DownArrow",
         action="DisableDefaultAssignment"},
        {mods="CTRL|SHIFT", key="LeftArrow",
         action="DisableDefaultAssignment"},
        {mods="CTRL|SHIFT", key="RightArrow",
         action="DisableDefaultAssignment"},
        {mods="CTRL|SHIFT", key="UpArrow",
         action="DisableDefaultAssignment"},
        {mods="CTRL|SHIFT", key="DownArrow",
         action="DisableDefaultAssignment"},

        -- {mods="CTRL", key="Backspace",
        --  action="DisableDefaultAssignment"},
        {mods="CTRL", key="Backspace",
         action={SendString="\x08"}},

        -- ALT is Meta, not ESC !
        {mods="ALT", key="LeftArrow",
         action={SendString="\x1b[1;3D"}},
        {mods="ALT", key="RightArrow",
         action={SendString="\x1b[1;3C"}},
        {mods="ALT", key="UpArrow",
         action={SendString="\x1b[1;3A"}},
        {mods="ALT", key="DownArrow",
         action={SendString="\x1b[1;3B"}},
    },

    colors = {
        tab_bar = {
            background = "#000000",
            active_tab = {
                bg_color = "#323234",
                fg_color = "#ebebec",
                intensity = "Bold",
            },
            inactive_tab = {
                bg_color = "#000000",
                fg_color = "#ebebec",
                intensity = "Half",
            },
            inactive_tab_hover = {
                bg_color = "#202021",
                fg_color = "#ffffff",
                intensity = "Normal",
            },
        },

        background = "#000000",
    },

}
