local wezterm = require 'wezterm'
local io = require 'io'
local os = require 'os'

wezterm.on("open-editor-with-scrollback", function(window, pane)
  -- Retrieve the current viewport's text.
  -- Pass an optional number of lines (eg: 2000) to retrieve
  -- that number of lines starting from the bottom of the viewport.
  local scrollback = pane:get_lines_as_text()

  -- Create a temporary file to pass to your editor
  local name = os.tmpname()
  local f = io.open(name, "w+")
  f:write(scrollback)
  f:flush()
  f:close()

  -- FIXME
  -- local editor = os.getenv("EDITOR") or "emacs -nw"

  -- Open a new window running your editor and tell it to open the file
  window:perform_action(wezterm.action{SpawnCommandInNewWindow={
    args={"emacs", "-nw", name}}
  }, pane)

  -- wait "enough" time for your editor to read the file before we remove it.
  -- The window creation and process spawn are asynchronous
  -- wrt. running this script and are not awaitable, so we just pick
  -- a number.  We don't strictly need to remove this file, but it
  -- is nice to avoid cluttering up the temporary file directory
  -- location.
  wezterm.sleep_ms(1000)
  os.remove(name)
end)

