#!/bin/bash

set -e

## GET SELF DIRECTORY ##########################################################
# one liner version
#SELF_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# symbolic links version
SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do
  SELF_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"
  SOURCE="$(readlink "${SOURCE}")"
  [[ "${SOURCE}" != "/*" ]] && SOURCE="${SELF_DIR}/${SOURCE}"
done
SELF_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"
################################################################################

# format for each line: type:file_in_home:[file_in_this_repo]
# type is L (file is linked) or F (file is copied)
# if file_in_this_repo is ommited, name of file_in_home is used
FILES="L:.bashrc:
       L:.bashrc.d:
       F:.bashrc_config:
       L:.config/awesome:
       L:.config/copyq/copyq-commands.ini:
       L:.config/copyq/copyq.conf:
       L:.config/environment.d:
       L:.config/emacs:
       L:.config/fontconfig:
       L:.config/git/config:
       L:.config/kactivitymanagerdrc:
       L:.config/kglobalshortcutsrc:
       L:.config/kwinrc:
       L:.config/locale.conf:
       L:.config/locales:
       L:.config/mimeapps.list:
       L:.config/nano/nanorc:
       L:.config/nano/nanorc.d:
       L:.config/picom.conf:
       L:.config/pikaur.conf:
       L:.config/procps:
       L:.config/redshift.conf:
       L:.config/rofi:
       L:.config/systemd/user/ssh-agent.service:
       L:.config/systemd/user/scream_windows_sound.service:
       L:.config/tmux/tmux.conf:
       L:.config/wezterm:
       L:.inputrc:
       L:.librewolf/librewolf.overrides.cfg:
       L:.local/bin:
       L:.local/share/applications/mimeapps.list:.config/mimeapps.list
       L:.local/share/konsole:
       L:.local/share/applications/net.local.hide-private-windows.desktop:
       L:.local/share/applications/net.local.unhide-private-windows.desktop:
       L:.local/share/applications/net.local.private-falkon.desktop:
       L:.local/share/dbus-1/services/org.freedesktop.secrets.service:
       L:.logout:
       L:.mplugd:
       L:.Xresources:
       L:.xprofile:
"

TESTMODE=0
if [ "${1}" == "--check" ]; then
    echo "checking mode activated"
    TESTMODE=1
fi
FORCEMODE=0
if [ "${1}" == "--force" ]; then
    echo "force mode activated"
    FORCEMODE=1
fi
HOMEDIR="$(cd && pwd)"
SAVE_DIR="${HOMEDIR}/SAVE_CONFIG_$(date +%F_%H-%M-%S)"

# Handle firefox and librewolf profiles links
for profiles_path in ".mozilla/firefox" ".librewolf"; do
    # Ensure the profiles.ini exists before trying to do anything
    if [[ -a "${HOMEDIR}/${profiles_path}/profiles.ini"  ]]; then
        # Add links in all profiles
        while IFS= read -r profile; do
            mkdir -p "${HOMEDIR}/${profiles_path}/${profile}/chrome" && \
                FILES="${FILES}
               L:${profiles_path}/${profile}/chrome/firefox-csshacks:.mozilla/firefox/chrome/firefox-csshacks
               L:${profiles_path}/${profile}/chrome/userChrome.css:.mozilla/firefox/chrome/userChrome.css
               L:${profiles_path}/${profile}/chrome/userContent.css:.mozilla/firefox/chrome/userContent.css"
        done < <(grep 'Path=' "${HOMEDIR}/${profiles_path}/profiles.ini" | cut -d '=' -f 2)
    fi
done

save_current_file ()
{
    file="${1}"
    savedir="${SAVE_DIR}/"$(dirname "${file:$(( ${#HOMEDIR} + 1 ))}")
    if [ -e "${file}" ] || [ -L "${file}" ]; then
        mkdir -p "${savedir}"
        echo -n "  saving old file to ${savedir}"
        mv -t "${savedir}" "${file}"
        res=$?
        if [ ${res} == 0 ]; then
            echo " OK"
        else
            echo " Error"
        fi
    fi
    # shellcheck disable=SC2086
    return $res
}

copy_or_link_file ()
{
    file="${1}"
    original="${2}"
    type="${3}"
    directory=$(dirname "${file}")
    if [ ! -d "${directory}" ]; then
        if [ -e "${directory}" ]; then
            echo "ERROR: ${directory} already exists and is not a directory."
            exit 1
        fi
        mkdir -p "${directory}"
    fi
    if save_current_file "${file}"; then
        case "${type}" in
            F) cp "${original}" "${file}" ;;
            L) ln -s "${original}" "${file}" ;;
            *)
                echo "ERROR"
                exit 1
                ;;
        esac
    else
        false
    fi
    return $?
}

is_valid_link ()
{
    src="${1}"
    dest="${2}"
    file=""
    if [ -L "${src}" ]; then
        # get symbolic link destination
        #shellcheck disable=SC2012
        file=$(ls -l "${src}" | awk -F '-> ' '{ print $2 }')
    fi
    [ "${file}" == "${dest}" ]
    return $?
}

is_valid ()
{
    src="${1}"
    dest="${2}"
    type="${3}"
    case "${type}" in
        F) [ -e "${src}" ] ;;
        L) is_valid_link "${src}" "${dest}" ;;
        *) false ;;
    esac
    return $?
}



################################# Main #########################################

errors_count=0
for f in $FILES; do
    count=1
    for x in $(echo "${f}" | tr ":" " "); do
        case ${count} in
            1) type="${x}" ;;
            2) new_file="${HOMEDIR}/${x}";&
            3) original_file="${SELF_DIR}/${x}" ;;
            *) exit 1 ;;
        esac
        count=$(( count + 1 ))
    done;
    echo -n "[    ] ${new_file}"
    if ( [ ${FORCEMODE} != 1 ] && is_valid "${new_file}" "${original_file}" "${type}" ); then
        echo -e "\r[ OK"
    else
        echo -e "\r[****"
        if [ ${TESTMODE} == 1 ] \
           || ! copy_or_link_file "${new_file}" "${original_file}" "${type}"
        then
            errors_count=$(( errors_count + 1 ))
        fi
    fi
done

# shellcheck source=./.bashrc.d/goto
source "${SELF_DIR}/.bashrc.d/goto"
set +e
goto -r homeconf "${SELF_DIR}" 2>/dev/null
set -e

if [ "$(systemctl --user is-enabled ssh-agent.service)" != "enabled" ]; then
    echo "For SSH agent at session start, enable the ssh-agent user service:"
    echo "systemctl --user enable ssh-agent.service"
fi


exit ${errors_count}
