# My home configuration.


## Install or update

    ./install.sh

This will save the current files if present and create appropriate links.

For updating, just run

    git pull
    ./install.sh



## 24 bits in terminals applications


    tic -x -o ~/.terminfo terminfo-24bit.src
    TERM=xterm-24bit emacs
    TERM=xterm-24bits emacs    # depending on terminal

* https://www.gnu.org/software/emacs/manual/html_node/efaq/Colors-on-a-TTY.html
* https://stackoverflow.com/questions/14672875/true-color-24-bit-in-terminal-emacs/50577683#50577683

## SSH agent

Start and enable the systemd user service

    systemctl --user start ssh-agent
    systemctl --user enable ssh-agent


If you want to copy this service to your own config, you have to set the `SSH_AUTH_LOCK` environment variable. Check my `.pam_environment` file.
